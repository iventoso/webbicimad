package com.bonopark.service;
import java.io.Serializable;
import java.util.List;

import com.bonopark.domain.*;

public interface NoticiaManager extends Serializable{

	List<Noticia> getNoticias();
	List<Noticia> getUltimasNoticias(int numeroNoticias);
	Noticia getNoticiaById(int id);

}
