package com.bonopark.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bonopark.domain.Estacion;

@Repository(value = "estacionDao")
public class JPAEstacionDao implements EstacionDao{

	private EntityManager em = null;
	/*
	 * Sets the entity manager.
	 */
	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Estacion> getEstacionList() {
			em.clear();
			List<Estacion> estaciones = em.createQuery("select e from Estacion e where e.activo=1").getResultList();
			return estaciones;
	}

}
