package com.bonopark.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bonopark.domain.Noticia;
import com.bonopark.repository.NoticiaDao;

@Component
public class SimpleNoticiaManager implements NoticiaManager {
	private static final long serialVersionUID = 1L;

	
	@Autowired
	private NoticiaDao noticiaDao;


	@Override
	public List<Noticia> getNoticias() {
		return noticiaDao.getNoticias();

	}


	@Override
	public List<Noticia> getUltimasNoticias(int numeroNoticias) {
		// TODO Auto-generated method stub
		return noticiaDao.getUltimasNoticias(numeroNoticias);
	}


	@Override
	public Noticia getNoticiaById(int id) {
		// TODO Auto-generated method stub
		return noticiaDao.getNoticiaById(id);
	}


}
