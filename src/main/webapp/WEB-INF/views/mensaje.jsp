<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD :: Mensaje</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>
	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="mensaje" value="active" />
		</jsp:include>

		<div class="content-15b v-center bg-bici">

			<div class="container">
				<div class="row">
                	<h1></h1>
					<h3>
						${model.titulo}
					</h3>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h6>
							<c:set var="mensaje">
								<c:out value="${model.mensaje}" />
							</c:set>
							<c:set var="mensaje2">
								<c:out value="${model.mensaje2}" />
							</c:set>
							<fmt:message key="${mensaje}" />
							<br/>
							<c:if test="${model.mensaje2 != null}">
								<fmt:message key="${mensaje2}" />
							</c:if>
						</h6>
					</div>
				</div>
			</div>

		</div>

        <!-- footer-6 -->
        <jsp:include page="/WEB-INF/common/footer-6.jsp" />
    
        <!-- footer-2 -->
        <jsp:include page="/WEB-INF/common/footer-2.jsp" />
    
	</div>

	<script
		src="<%=request.getContextPath()%>/resources/js/language_slide.js"></script>

<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>
