<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD ::</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="comofunciona" value="active" />
		</jsp:include>

		<!-- blog-3 -->
		<div class="blog-3 bg-clouds">
			<div class="container">
				<div class="row">
					<div class="col-xs-8 col-xs-offset-2">
						<div>
							<h3>Preguntas Frecuentes</h3>
						</div>
						<div class="promo">
							<h6>¿Qué ventajas tiene la bicicleta eléctrica?</h6>
							<p>
								La bicicleta eléctrica mantiene todas las ventajas de la
								bicicleta convencional pero además:</p>
								<ul>
									<li>Ha permitido poner estaciones en zonas de alto
										desnivel y poder equilibrarlas territorialmente</li>
									<li>Es más ecológica: ayuda a la redistribución del
										sistema, evitando su transporte en vehículo motorizado y
										disminuyendo las emisiones a la atmósfera</li>
									<li>Se recarga en la misma estación y tiene una autonomía
										de 70 Km</li>
									<li>Facilita el uso a todas las personas sin necesidad de
										una condición física determinada</li>
									<li>Te permite ir con ropa adecuada al trabajo ya que el
										ejercicio que desarrolla la persona es menos intenso</li>
								</ul>
							
							<h6>¿Cómo funciona una bicicleta eléctrica?</h6>
							<p>La bicicleta eléctrica es una bicicleta convencional a la
								que se añade tecnología eléctrica que proporciona al usuario
								asistencia en el pedaleo. Al pedalear, el motor eléctrico se
								activa asumiendo gran parte del esfuerzo que, de manera
								convencional, asumiría el usuario.</p>

							<h6>¿Son peligrosas las bicicletas eléctricas?</h6>
							<p>No, su manejo es igual que el de las bicicletas
								convencionales y su velocidad está limitada a 25 km/h. No
								significa que no pueda circular a mayor velocidad, pero el motor
								deja de ayudar cuando la alcanzamos.</p>


							<h6>¿Son pesadas las bicicletas eléctricas?</h6>
							<p>Son algo más pesadas que la bicicleta convencional
								anterior ya que incluyen el motor y la batería. El peso de la
								bicicleta es de unos 22 kg, sin embargo, las prestaciones
								propias de su sistema eléctrico le otorgan mayor ligereza en el
								uso.</p>


							<h6>¿Es más caro un sistema de bicicletas eléctricas para el
								Ayuntamiento de Madrid?</h6>
							<p>Para el Ayuntamiento no ha sido más caro que un sistema de
								bicis convencional. La licitación que sacó el Ayuntamiento era
								con un presupuesto para bicis convencionales y el adjudicatario
								ofertó el servicio mejorando con bicicletas eléctricas por el
								mismo presupuesto. </br>Además tiene una doble ventaja económica y
								medioambiental en la distribución de las bicicletas al poder
								subir las cuestas el propio usuario, con lo que se reducen
								costes y emisiones a la atmósfera, al reducirse la necesidad de
								transporte entre estaciones en vehículos motorizados.</p>
								
								
							<h6>¿Por qué se cobra desde la primera media hora?</h6>
							<p>
								La experiencia contrastada con otras ciudades recomienda cobrar
								la primera fracción de 30 minutos. Así se evita que la bici
								pública recoja los movimientos cortos que realizan andando
								muchos ciudadanos, ya que el objetivo del Ayuntamiento es
								reducir los viajes que se efectúan en otros medios de transporte
								más contaminantes.</br>El coste de la primera fracción de 30 min se
								compensa al ser el abono anual en Madrid más barato que en otras
								ciudades y con el sistema de bonificaciones que es una novedad
								de este sistema.
							</p>

							<h6>¿Por qué es más caro después de la primera hora?</h6>
							<p>
								BiciMAD es un medio de transporte público, por lo que está
								ideado para que las personas utilicen las bicicletas como medio
								de transporte. </br>Con esta tarifa evitamos que un usuario utilice
								la bicicleta durante todo el día para recorrer la ciudad, para
								lo que ya existen empresas que ofrecen excelentes servicios de
								alquiler.
							</p>

							<h6>¿El sistema quita negocio y perjudica a las empresas de
								alquiler de bicicletas?</h6>
							<p>El sistema tarifario implantado pretende evitar que este
								servicio sea utilizado por turistas y evitamos que un usuario
								utilice la bicicleta durante todo el día para recorrer la
								ciudad, ambos tienen otras alternativas en el mercado como las
								empresas de alquiler de bicicletas. A partir de la segunda hora
								el coste es de 4 euros por hora, por lo que no compite por
								precio con las empresas de alquiler.</p>

							<h6>¿Qué zona de Madrid abarca Bicimad?</h6>
							<p>BiciMAD abarca 5 distritos de la capital. El Distrito
								Centro completo y parte de los Distritos de Arganzuela, Retiro,
								Moncloa-Aravaca y Chamberí. Se puede consultar en el mapa
								bicimad. Son aproximadamente unas 30 h que se corresponden con
								algo más de lo que se conoce con el segundo cinturón de la
								ciudad.</p>

							<h6>¿Cuándo se tiene previsto ampliar BiciMad a otras zonas
								de Madrid?</h6>
							<p>BiciMAD tiene prevista su ampliación que se irá definiendo
								en función de la planificación de la movilidad de la ciudad,
								basada en el Plan de Movilidad Sostenible de la ciudad de
								Madrid.</p>

							<h6>¿Las estaciones han quitado plazas de aparcamiento a los
								vecinos y  espacio a los peatones?</h6>
							<p>Las estaciones se han situado mayoritariamente en zonas de
								aparcamiento azules de rotación, solo en aquellos barrios que no
								poseen plazas de zona azul se ha optado por el aparcamiento
								verde de residentes.</p>

							<h6>¿Puedo pagar en efectivo?</h6>
							<p>No. El pago se realiza siempre mediante tarjeta de crédito
								o débito.</p>

							<h6>¿Cuál es la edad mínima para acceder al servicio?</h6>
							<p>Se puede utilizar a partir de los 14 años. Los menores de
								16 deberán ser dados de alta por un adulto, en calidad de tutor
								o representante legal, y asumirá completamente la
								responsabilidad tanto de los actos de los menores durante la
								utilización del servicio como de su idoneidad física para usar
								el servicio.</p>

							<h6>¿BiciMAD  es sólo para los vecinos de Centro?</h6>
							<p>No. El sistema es para todos los que quieren moverse por
								el centro, complementando en su caso al coche o al transporte
								público.</p>

							<h6>   ¿Puedo salir fuera de la zona de BiciMAD?</h6>
							<p>Sí, se puede utilizar en cualquier vía urbana de Madrid
								pero deberás calcular el tiempo de regreso y su incidencia en la
								tarifa resultante.</p>

							<h6>¿Qué horario de funcionamiento tiene?</h6>
							<p>Las estaciones las bicicletas estarán accesibles las 24h,
								de lunes a domingo, todos los días del año.</p>

							<h6>¿Hay tiempo limitado de uso?</h6>
							<p>El usuario puede hacer un uso continuado de la bicicleta
								por el tiempo que desee a lo largo del día, pero el uso superior
								a las dos horas paga una tarifa más alta.</p>

							<h6>¿Tengo que esperar entre un uso y otro?</h6>
							<p>Una vez devuelta la bicicleta, deberán transcurrir un
								mínimo de quince minutos para poder retirar esa misma u otra
								bicicleta y comenzar un nuevo trayecto. De no transcurrir ese
								plazo entre la devolución y la retirada, el sistema
								contabilizará que el uso del servicio ha sido continuado.</p>

							<h6>¿Puedo reservar aparcamiento en la estación de destino?</h6>
							<p>Sí, la reserva de estación de destino se realiza a través
								del tótem de las
								estaciones. La reserva de la estación tiene una bonificación de
								diez céntimos, no acumulable a la bonificación por devolver la
								bicicleta en estación deficitaria (con menos de un 30% de
								ocupación).</p>

							<h6>¿Qué hago si no puedo devolver una bici porque no
								funciona el anclaje?</h6>
							<p>Al igual que en el caso anterior, deberá seleccionar la
								opción de estación ocupada para disponer de diez minutos más
								para llegar hasta la estación más cercana y deberá dar parte de
								la avería.</p>


							<h6>¿Tienen candado las bicicletas?</h6>
							<p>No, el usuario tiene la responsabilidad de custodiar la
								bicicleta durante todo el tiempo que disponga de ella.</p>

							<h6>¿Puede pinchar una rueda?</h6>
							<p> Son antipinchazos, pero ante una avería se debe notificar
								la incidencia en el 900 50 54 63.</p>

							<h6>¿Tiene marchas la bicicleta?</h6>
							<p> Sí, tiene tres marchas, un cambio de desarrollo que va en
								el interior del buje trasero. Además, tiene tres niveles de
								asistencia eléctrica.</p>

							<h6>¿Estoy asegurado? ¿Y contra terceros?</h6>
							<p>
								Sí, en ambos casos. Puede consultar <a
									href="/resources/docs/Seguro de Accidentes Personales.pdf" />aquí
								</a> las condiciones del Seguro.
							</p>

							<h6>¿Qué hago si tengo un accidente?</h6>
							<p>Avisar de la incidencia por los canales establecidos (ver
								pregunta siguiente).</p>

							<h6>  ¿Qué hago si tengo una avería u otra incidencia?</h6>
							<p>
								En caso de avería o cualquier otra incidencia del servicio, el
								usuario tiene que realizar un aviso de la incidencia a través de
								cualquiera de estos canales: tótem de la estación, teléfono 900
								50 54 63, sin cita previa en las Oficinas de Atención al
								Ciudadano Línea Madrid, 010 y aplicación para
								móvil BiciMAD.</br> En caso de avería de la bicicleta, el usuario la
								anclará en la estación más próxima y comunicará esta incidencia
								a través del tótem de la estación o de cualquiera de los canales
								habilitados para el aviso de incidencias. Si fuera imposible el
								traslado de la bicicleta a la estación más cercana, el usuario
								la custodiará hasta que la empresa encargada de la gestión del
								servicio proceda a recogerla tras recibir el aviso de la
								incidencia.
							</p>

							<h6>¿Qué hago si me la roban?</h6>
							<p>En el supuesto de pérdida, robo o hurto de la bicicleta,
								el usuario denunciará los hechos, con la mayor brevedad posible,
								a las Fuerzas y Cuerpos de Seguridad, y comunicará la incidencia
								en un plazo máximo de 2 horas. Asimismo, entregará una copia de
								la denuncia a la empresa Bonopark, con domicilio en la calle
								Serrano 85, 7º Izda, 28006 Madrid.</p>

							<h6>¿Tengo que usar casco?</h6>
							<p>El casco es obligatorio para los menores de 16 años.</p>

							<h6>¿Por dónde debe circular la bici?</h6>
							<p>Por la calzada, nunca por la acera. En las vías con más de
								un carril circularán siempre por el carril de la derecha pero
								pueden circular por el carril de la izquierda si han de realizar
								un giro a la izquierda.</p>

							<h6>¿Por qué no podemos usar los carriles bus?</h6>
							<p>La prioridad en Madrid es mantener la excelente calidad
								del transporte público. Generalizar el uso de la bici en los
								carriles exclusivos puede ralentizar el buen servicio de  los
								autobuses.</p>

							<h6>¿Tengo preferencia en el ciclocarril?</h6>
							<p>No, pero tengo más facilidad al estar limitada la
								velocidad del tráfico a 30 km/h.</p>

							<h6>¿Es el ciclocarril de uso exclusivo para los ciclistas?</h6>
							<p>No, pero el resto de vehículos deberá adaptar su velocidad
								a la de la bicicleta</p>

							<h6>¿Pueden las bicis utilizar otros carriles de la
								calzada? </h6>
							<p>Sí, en las vías con más de un carril circularán siempre
								por el carril de la derecha pero pueden circular por el carril
								de la izquierda si han de realizar un giro a la izquierda.</p>

							<h6>¿Qué obligaciones tienen los conductores cuando circulan
								junto a ciclistas?</h6>
							<p>La Ordenanza Municipal de Movilidad establece que el
								ciclista debe circular por el centro del carril, respetar las
								normas de circulación relativas a las preferencias de paso, así
								como los semáforos y las señales de circulación. Los vehículos
								que circulen detrás de una bici deben guardar una distancia
								mínima de 5 metros. Para adelantar o rebasar a una bici el
								vehículo debe cambiar de carril o respetar una distancia de
								seguridad lateral, fijada en 1,50 metros.</p>

						</div>
					</div>
				</div>

			</div>
		</div>
		<h1></h1>
		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>