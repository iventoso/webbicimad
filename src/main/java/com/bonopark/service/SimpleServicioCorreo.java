package com.bonopark.service;

import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.bonopark.domain.Residente;
import com.bonopark.domain.Usuario;

/**
 * Servicio de envÃío de emails
 */
public class SimpleServicioCorreo implements ServicioCorreo {

	private static String correoNoReply = "noreply@bonopark.es";
	private static String passwordNoReply = "2r2AP4W1";

	private static String cabeceraCorreo = 
			"<p align=\"center\" style=\"margin:10px;font-family:Arial;font-size:14px\">"
			+ "Este correo es informativo, por favor no responder a esta "
			+ "cuenta de correo, ya que no se encuentra habilitada para recibir mensajes."
			+ "</p>";
		
	private static String pieCorreo = 
			"<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
			+ "<p style=\"padding-top:10px;font-style:italic;font-size:small\">Estamos a su disposici&#243n" +
			" en www.madrid.es/contactar, en nuestras Oficinas de Atenci&#243n a la Ciudadan&#237a " +
			"(L&#237nea Madrid), en el Servicio de Atenci&#243n Telef&#243nica 010-L&#237nea Madrid " +
			"(accesible marcando 010 desde nuestra ciudad o bien 91 529 82 10) y en nuestra cuenta " +
			"de la red social Twitter @lineamadrid.</p>"
			+ "<div style=\"background-color:#aaaaaa; padding:10px;max-width:600px;\">"
			+ "<p style=\"color:#ffffff;font-size:small\">Los datos personales recogidos ser&#225n incorporados " +
			"y tratados en el fichero 2132470736 " +
			"cuya finalidad es la gesti&#243n de clientes y usuarios del Servicio P&#250blico de Bicicleta. " +
			"Los datos no podr&#225n ser cedidos a terceros salvo en los supuesto previstos en el art&#237culo 11 " +
			"de la Ley Org&#225nica 15/1999, de 13 de diciembre, de Protecci&#243n de Datos de car&#225cter Personal. " +
			"El &#243rgano responsable del fichero es la empresa BONOPARK, S.L, con direcci&#243n en la calle Serrano 85," +
			" 7º Izda, 28006 Madrid ante la que el interesado podr&#225 ejercer los derechos de acceso, rectificaci&#243n," +
			" cancelaci&#243n y oposici&#243n, todo lo cual se informa en cumplimiento del art&#237culo 5 de referida Ley " +
			"Org&#225nica 15/1999.</p>"
			+ "</div>";
	
//	private static String url_imagen_bicimad = "http://146.255.100.64:8080/bicimad/resources/img/logo-small@2x_b.png";
//	private static String url_imagen_ayunt = "http://146.255.100.64:8080/bicimad/resources/img/logo_madrid.png";
	private static String url_imagen_bicimad = "http://www.bicimad.com/resources/img/logo-small@2x_b.png";
	private static String url_imagen_ayunt = "http://www.bicimad.com/resources/img/logo_madrid.png";
	
	public static void main(String[] args) {
		Residente residente = new Residente();
		residente.setFecha(new Date());
		
		Usuario usuario = new Usuario();
		usuario.setClave("KsdjWE");
		STATICsendConfirmacion("m.asuar@booster-bikes.com", "pfjgaKAhs", residente, usuario, 123456);
		STATICsendContrasena("m.asuar@booster-bikes.com", "AsyS98H", residente);
		
	}
	
	public SimpleServicioCorreo() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	private Properties conexionEmail() {
		// Propiedades de la conexiÃ³n
		Properties props = new Properties();
		props.setProperty("mail.smtp.host", "smtp.bonopark.es");//"smtp.dbizi.com");
		props.setProperty("mail.smtp.starttls.enable", "true");
		props.setProperty("mail.smtp.port", "25");
		props.setProperty("mail.smtp.user", "noreply@bonopark.es");//"teleco@dbizi.com");
		props.setProperty("mail.smtp.auth", "true");
		return props;
	}
	private static Properties conexionEmailInfo() {
		// Propiedades de la conexiÃ³n
		Properties props = new Properties();
		props.setProperty("mail.smtp.host", "smtp.bonopark.es");//"smtp.dbizi.com");
		props.setProperty("mail.smtp.starttls.enable", "true");
		props.setProperty("mail.smtp.port", "25");
		props.setProperty("mail.smtp.user", "noreply@bonopark.es");//"info@dbizi.com");
		props.setProperty("mail.smtp.auth", "true");
		return props;
	}
	private static Session getSession() {
		Authenticator authenticator = new Authenticator();

		Properties properties = new Properties();
		properties.put("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.starttls.enable", "false");
		properties.put("mail.smtp.host", "smtp.bonopark.es");
		properties.put("mail.smtp.port", "25");
		properties.put("mail.smtp.localhost", "smtp.bonopark.es");

		Session session = Session.getInstance(properties, authenticator);
//		session.setDebug(true);
		return session;
	}

	private static class Authenticator extends javax.mail.Authenticator {
		private PasswordAuthentication authentication;

		public Authenticator() {
			String username = "noreply@bonopark.es";
			String password = "2r2AP4W1";
			authentication = new PasswordAuthentication(username, password);
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return authentication;
		}
	}
	@Override
	public void sendContacto(String to, String subject, String nombre,
			String apellido, String movil, String email, String mensaje) {
		
		try {// Preparamos la sesion
			Session session = Session.getDefaultInstance(conexionEmail());
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply@bonopark.es"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					"noreply@bonopark.es"));
			message.setSubject("Contacto www.bicimad.com");
			message.setText("Nombre: " + nombre + "\nApellido: " + apellido			+ "\nMovil: " + movil + "\nEmail: " + email + "\nMensaje: "
			+ mensaje);
			//message.setText("Han contactado");
			// Lo enviamos.

			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	@Override
	public String sendConfirmacion(String to, String pwd,
			Residente residente, Usuario usuario, int codId) {
		try {
			System.out.println("SimpleServicioCorreo/sendConfirmacion : Residente: " + residente + "--- Usuario :" + usuario);
			// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmailInfo());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(correoNoReply));
			message.setHeader("Content-Type", "text/html");
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("Alta Abonado BICIMAD - Ayuntamiento de Madrid. Código de identificación/código de usuario: " + codId);
			
			Calendar cal = Calendar.getInstance();    
			cal.setTime( residente.getFecha()); 
			cal.add(Calendar.DATE, 365);
			
			MimeMultipart multipart = new MimeMultipart("related");
			
			BodyPart messageBodyPart1 = new MimeBodyPart();
			String htmlText = "<html>"
					+ "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>"
					+ "<body style=\"background-color:#fff; font-size:medium; color:#727074\">"
					+ "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" "
					+ "bgcolor=\"#2088c2\" style=\"background-color:#2174b7;width:100%;border:0\">"
					+ "<tr>"
					+ "<td>"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ "<img src=\"" + url_imagen_ayunt + "\""
					+ " align=\"left\" style=\"width:auto; height:auto\">"
					+ "<img src=\"" + url_imagen_bicimad + "\""
					+ " align=\"right\" style=\"width:auto; height:auto\">"
					+ "</div>"
					+ "</td>"
					+ "</tr>"
					+ "</table>"
					
					+ "<table style=\"width:100%;background-color:#fff\">"
					+ "<tr>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "<td width=\"96%\" bgcolor=\"#FFFFFF\">"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ cabeceraCorreo
					+ "<p style=\"font-family:Arial,sans-serif;font-size:24px;color:#2088c2;padding: 10px;"
					+ "font-weight:bold;line-height:1;text-align:center\">"
					+ "Gracias por darse de alta como abonado de BiciMAD.</p>"
					+ "<p style=\"font-size:18px;font-family:arial;font:bold;margin:10px\">"
					+ "La fecha de vigencia ser&#225 de un a&#241o desde el momento en el que recoja " 
					+ "su tarjeta.</p>"
					+ "<p style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "Para poder hacer uso del servicio de Bicicleta p&#250blica necesita recoger la " 
					+ "tarjeta BiciMAD en el T&#211TEM.</p>"
					+ "<p style=\"font-size:18px;font-family:arial;font:bold;margin:10px\">"
					+ "<strong>En &#233ste deber&#225 seleccionar en el men&#250 de la pantalla t&#225ctil " 
					+ "\"Recoger abono\" e introducir los datos de identificaci&#243n y el siguiente c&#243digo:</strong></p>"
					+ "<p align=\"center\" style=\"font-size:20px;font-family:arial;font:bold;margin:10px\">"
					+ "<strong>" + usuario.getClave() + "</strong></p>"
					+ "<p style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "Es preciso que recargue su tarjeta para utilizar el sistema. Si no ha realizado una "
					+ "recarga inicial al darse de alta, podr&#225 hacerlo desde el propio t&#243tem, "
					+ "la aplicaci&#243n m&#243vil o la p&#225gina web:</p>"
					+ "<p align=\"center\" style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "<a href=\"http://bicimad.com\">www.bicimad.com</a></p>"
					+ "<p style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "Puede consultar las condiciones de participaci&#243n, el mapa de estaciones de Madrid y la ocupaci&#243n en tiempo real</p>"
					+ "</div>"
					+ pieCorreo
					+ "</td>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "</tr>"
					+ "</table>"
					+ "</body>"
					+ "</html>";
			
			messageBodyPart1.setContent(htmlText, "text/html");
			
			multipart.addBodyPart(messageBodyPart1);
			
			message.setContent(multipart);
			
			// Lo enviamos.
			
			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
			
		} catch (SendFailedException e) {
			return e.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		return "ok";
		
	}
	
	@Override
	public String sendConfirmacionSinClave(String to, String codId, String fechaCaducidad) {
		try {
			// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmailInfo());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(correoNoReply));
			message.setHeader("Content-Type", "text/html");
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("Alta Abonado BICIMAD - Ayuntamiento de Madrid. Código de identificación/código de usuario: " + codId);
			
			MimeMultipart multipart = new MimeMultipart("related");
			
			BodyPart messageBodyPart1 = new MimeBodyPart();
			String htmlText = "<html>"
					+ "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>"
					+ "<body style=\"background-color:#fff; font-size:medium; color:#727074\">"
					+ "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" "
					+ "bgcolor=\"#2088c2\" style=\"background-color:#2174b7;width:100%;border:0\">"
					+ "<tr>"
					+ "<td>"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ "<img src=\"" + url_imagen_ayunt + "\""
					+ " align=\"left\" style=\"width:auto; height:auto\">"
					+ "<img src=\"" + url_imagen_bicimad + "\""
					+ " align=\"right\" style=\"width:auto; height:auto\">"
					+ "</div>"
					+ "</td>"
					+ "</tr>"
					+ "</table>"
					
					+ "<table style=\"width:100%;background-color:#fff\">"
					+ "<tr>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "<td width=\"96%\" bgcolor=\"#FFFFFF\">"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ cabeceraCorreo
					+ "<p style=\"font-family:Arial,sans-serif;font-size:24px;color:#2088c2;padding: 10px;"
					+ "font-weight:bold;line-height:1;text-align:center\">"
					+ "Gracias por darse de alta como abonado de BiciMAD.</p>"
					+ "<p style=\"font-size:18px;font-family:arial;font:bold;margin:10px\">"
					+ "La fecha de vigencia es hasta el " + fechaCaducidad + "</p>"
					+ "Es preciso que recargue su tarjeta para utilizar el sistema. Si no ha realizado una "
					+ "recarga inicial al darse de alta, podr&#225 hacerlo desde el propio t&#243tem, "
					+ "la aplicaci&#243n m&#243vil o la p&#225gina web:</p>"
					+ "<p align=\"center\" style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "<a href=\"http://bicimad.com\">www.bicimad.com</a></p>"
					+ "<p style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "Puede consultar las condiciones de participaci&#243n, el mapa de estaciones de Madrid y la ocupaci&#243n en tiempo real</p>"
					+ "</div>"
					+ pieCorreo
					+ "</td>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "</tr>"
					+ "</table>"
					+ "</body>"
					+ "</html>";
			
			messageBodyPart1.setContent(htmlText, "text/html");
			
			multipart.addBodyPart(messageBodyPart1);
			
			message.setContent(multipart);
			
			// Lo enviamos.
			
			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
			
		} catch (SendFailedException e) {
			return e.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		return "ok";
		
	}
	
	public static String STATICsendConfirmacion(String to, String pwd,
			Residente residente, Usuario usuario, int codId) {
		try {
			// Preparamos la sesion
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(correoNoReply));
			message.setHeader("Content-Type", "text/html");
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("Alta Abonado BICIMAD - Ayuntamiento de Madrid. Código de identificación/" +
					"código de usuario: " + codId);
			
			Calendar cal = Calendar.getInstance();    
			cal.setTime( residente.getFecha()); 
			cal.add(Calendar.DATE, 365);
			
			System.out.println("Usuario: : " + usuario.toString());
			System.out.println("Residente: : " + residente.toString());
			
			MimeMultipart multipart = new MimeMultipart("related");
			
			BodyPart messageBodyPart1 = new MimeBodyPart();
			String htmlText = "<html>"
					+ "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>"
					+ "<body style=\"background-color:#fff; font-size:medium; color:#727074\">"
					+ "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" "
					+ "bgcolor=\"#2088c2\" style=\"background-color:#2174b7;width:100%;border:0\">"
					+ "<tr>"
					+ "<td>"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ "<img src=\"" + url_imagen_ayunt + "\""
					+ " align=\"left\" style=\"width:auto; height:auto\">"
					+ "<img src=\"" + url_imagen_bicimad + "\""
					+ " align=\"right\" style=\"width:auto; height:auto\">"
					+ "</div>"
					+ "</td>"
					+ "</tr>"
					+ "</table>"
					
					+ "<table style=\"width:100%;background-color:#fff\">"
					+ "<tr>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "<td width=\"96%\" bgcolor=\"#FFFFFF\">"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ cabeceraCorreo
					+ "<p style=\"font-family:Arial,sans-serif;font-size:24px;color:#2088c2;padding: 10px;"
					+ "font-weight:bold;line-height:1;text-align:center\">"
					+ "Gracias por darse de alta como abonado de BiciMAD.</p>"
					+ "<p style=\"font-family:Arial,sans-serif;font-size:24px;color:#cc6060;padding: 10px;"
					+ "font-weight:bold;line-height:1;text-align:center\">"
					+ "Podr&#225 recoger su tarjeta BiciMAD a partir del LUNES 23 de JUNIO A LAS 11:00.</p>"
					+ "<p style=\"font-size:18px;font-family:arial;font:bold;margin:10px\">"
					+ "La fecha de vigencia ser&#225 de un a&#241o desde el momento en el que recoja " 
					+ "su tarjeta.</p>"
					+ "<p style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "Para poder hacer uso del servicio de Bicicleta p&#250blica necesita recoger la " 
					+ "tarjeta BiciMAD en el T&#211TEM.</p>"
					+ "<p style=\"font-size:18px;font-family:arial;font:bold;margin:10px\">"
					+ "<strong>En &#233ste deber&#225 seleccionar en el men&#250 de la pantalla t&#225ctil " 
					+ "\"Recoger abono\" e introducir los datos de identificaci&#243n y el siguiente c&#243digo:</strong></p>"
					+ "<p align=\"center\" style=\"font-size:20px;font-family:arial;font:bold;margin:10px\">"
					+ "<strong>" + usuario.getClave() + "</strong></p>"
					+ "<p style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "Es preciso que recargue su tarjeta para utilizar el sistema. Si no ha realizado una "
					+ "recarga inicial al darse de alta, podr&#225 hacerlo desde el propio t&#243tem, "
					+ "la aplicaci&#243n m&#243vil o la p&#225gina web:</p>"
					+ "<p align=\"center\" style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "<a href=\"http://bicimad.com\">www.bicimad.com</a></p>"
					+ "<p style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "Puede consultar las condiciones de participaci&#243n, el mapa de estaciones de Madrid y la ocupaci&#243n en tiempo real</p>"
					+ "</div>"
					+ pieCorreo
					+ "</td>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "</tr>"
					+ "</table>"
					+ "</body>"
					+ "</html>";
			
			messageBodyPart1.setContent(htmlText, "text/html");
			
			multipart.addBodyPart(messageBodyPart1);
			
			message.setContent(multipart);
			
			// Lo enviamos.
			
			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
			
		} catch (SendFailedException e) {
			return e.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		return "ok";
		
	}
	
	public static String STATICsendContrasena(String to,String pwd, Residente residente) {
		try {
			// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmailInfo());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(correoNoReply));
			message.setHeader("Content-Type", "text/html");
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("Comunicación contraseña BICIMAD - Ayuntamiento de Madrid");
			
			MimeMultipart multipart = new MimeMultipart("related");
			
			BodyPart messageBodyPart1 = new MimeBodyPart();
			String htmlText = "<html>"
					+ "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>"
					+ "<body style=\"background-color:#fff; font-size:medium; color:#727074\">"
					+ "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" "
					+ "bgcolor=\"#2088c2\" style=\"background-color:#2174b7;width:100%;border:0\">"
					+ "<tr>"
					+ "<td>"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ "<img src=\"" + url_imagen_ayunt + "\""
					+ " align=\"left\" style=\"width:auto; height:auto\">"
					+ "<img src=\"" + url_imagen_bicimad + "\""
					+ " align=\"right\" style=\"width:auto; height:auto\">"
					+ "</div>"
					+ "</td>"
					+ "</tr>"
					+ "</table>"
					
					+ "<table style=\"width:100%;background-color:#fff\">"
					+ "<tr>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "<td width=\"96%\" bgcolor=\"#FFFFFF\">"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ cabeceraCorreo
					+ "<p style=\"font-size:18px;font-family:arial;font:bold;margin:10px\">"
					+ "Le indicamos que su contrase&#241a para operar en la p&#225gina Web de BICIMAD y la aplicaci&#243n m&#243vil es la siguiente:</p>"
					+ "<p style=\"font-size:18px;font-family:arial;font:bold;margin:10px\">"
					+ "<strong> " + pwd + "</strong>"
					+ "<p align=\"center\" style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "<a href=\"http://bicimad.com\">www.bicimad.com</a></p>"
					+ "</div>"
					+ pieCorreo
					+ "</td>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "</tr>"
					+ "</table>"
					+ "</body>"
					+ "</html>";;
			
			messageBodyPart1.setContent(htmlText, "text/html");
			
			multipart.addBodyPart(messageBodyPart1);
			
			message.setContent(multipart);
			
			// Lo enviamos.
			
			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
			
		} catch (SendFailedException e) {
			return e.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		return "ok";

	}
	
	@Override
	public String sendContrasena(String to,String pwd, Residente residente) {
		try {
			// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmailInfo());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(correoNoReply));
			message.setHeader("Content-Type", "text/html");
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("Comunicación contraseña BICIMAD - Ayuntamiento de Madrid");
			
			MimeMultipart multipart = new MimeMultipart("related");
			
			BodyPart messageBodyPart1 = new MimeBodyPart();
			String htmlText = "<html>"
					+ "<head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head>"
					+ "<body style=\"background-color:#fff; font-size:medium; color:#727074\">"
					+ "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" "
					+ "bgcolor=\"#2088c2\" style=\"background-color:#2174b7;width:100%;border:0\">"
					+ "<tr>"
					+ "<td>"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ "<img src=\"" + url_imagen_ayunt + "\""
					+ " align=\"left\" style=\"width:auto; height:auto\">"
					+ "<img src=\"" + url_imagen_bicimad + "\""
					+ " align=\"right\" style=\"width:auto; height:auto\">"
					+ "</div>"
					+ "</td>"
					+ "</tr>"
					+ "</table>"
					
					+ "<table style=\"width:100%;background-color:#fff\">"
					+ "<tr>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "<td width=\"96%\" bgcolor=\"#FFFFFF\">"
					+ "<div style=\"max-width:600px;margin-right:auto;margin-left:auto\">"
					+ cabeceraCorreo
					+ "<p style=\"font-size:18px;font-family:arial;font:bold;margin:10px\">"
					+ "Le indicamos que su contrase&#241a para operar en la p&#225gina Web de BICIMAD y la aplicaci&#243n m&#243vil es la siguiente:</p>"
					+ "<p style=\"font-size:18px;font-family:arial;font:bold;margin:10px\">"
					+ "<strong> " + pwd + "</strong>"
					+ "<p align=\"center\" style=\"font-size:18px;font-family:arial;margin:10px\">"
					+ "<a href=\"http://bicimad.com\">www.bicimad.com</a></p>"
					+ "</div>"
					+ pieCorreo
					+ "</td>"
					+ "<td width=\"2%\" bgcolor=\"#FFFFFF\"></td>"
					+ "</tr>"
					+ "</table>"
					+ "</body>"
					+ "</html>";;
			
			messageBodyPart1.setContent(htmlText, "text/html");
			
			multipart.addBodyPart(messageBodyPart1);
			
			message.setContent(multipart);
			
			// Lo enviamos.
			
			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
			
		} catch (SendFailedException e) {
			return e.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		return "ok";

	}


	@Override
	public String sendRecargaSaldo(String to, double importe, String saldoTotal, Residente residente) {

		try {
			// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmailInfo());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(correoNoReply));
			message.setHeader("Content-Type", "text/html");
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("Recarga Saldo BICIMAD - Ayuntamiento de Madrid"
					+ "Código de identificación/có-digo de usuario: " + residente.getId()
					+ " " + residente.getNombre() + " " + residente.getApellido1());
			
			MimeMultipart multipart = new MimeMultipart("related");
			
			BodyPart messageBodyPart1 = new MimeBodyPart();
			String htmlText = "<html>"
					+ "<body style=\"background-color:#dfdcdc; font-size:medium; color:#727074\">"
					+ "<table width=\"70% \" style=\"margin-left: auto; margin-right: auto;\">"
					+ "<tr>"
					+ "<td>"
					+ "<img src=\"" + url_imagen_bicimad + "\""
					+ " align=\"left\" style=\"width:auto; height:auto\">"
					+ "<img src=\"" + url_imagen_ayunt + "\""
					+ " align=\"right\" style=\"width:auto; height:auto\">"
					+ "</td>"
					+ "</tr>"
					+ "<tr>"
					+ "<td>"
					+ cabeceraCorreo
					+ "</td>"
					+ "</tr>"
					+ "<tr>"
					+ "<td>"
					+ "<p>Se ha efectuado una recarga de su saldo en el servicio de Bicicleta p&#250blica por un importe de: " + importe + " euros</p>"
					+ "<p>Actualmente su saldo es de:</p>"
					+ "</td>"
					+ "</tr>"
					+ "<tr>"
					+ "<td style=\"border:2px solid #a1a1a1;padding:10px 40px;width:300px;background:#ffffff\"><strong> " + saldoTotal + "</strong></td>"
					+ "</td>"
					+ "</tr>"
					+ pieCorreo
					+ "</table>"
					+ "</body>"
					+ "</html>";
			
			messageBodyPart1.setContent(htmlText, "text/html");
			
			multipart.addBodyPart(messageBodyPart1);
			
			message.setContent(multipart);
			
			// Lo enviamos.
			
			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
			
		} catch (SendFailedException e) {
			return e.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		return "ok";
	}

	@Override
	public boolean sendCambioPwd(String to, String pwd, String DNI) {
		try {
			// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmail());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply@bonopark.es"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("Cambio Contraseña BiciMAD");

			message.setText("Su cuenta ha sido modificada. Sus nuevas credenciales son las siguientes: \n"
					+ "Usuario: " + DNI + "\n" + "contraseña: " + pwd);
			// Lo enviamos.
			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	@Override
	public String sendModificacionDatos(String to, Residente residente) {
		try {
			// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmailInfo());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(correoNoReply));
			message.setHeader("Content-Type", "text/html");
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("Modificacion Datos Personales BICIMAD - Ayuntamiento de Madrid.\n Codifo de identificaciÃ³n/cÃ³digo de usuario: " + residente.getId() + "\n" + residente.getNombre() + " " + residente.getApellido1());
			message.setText("Se ha modificado una modificaciÃ³n de sus datos personales en la base de datos de BICIMAD\n"
					+ "Estamos a su disposiciÃ³n en www.madrid.es/contactar, en nuestras Oficinas de AtenciÃ³n a la CiudadanÃ­a (LÃ­nea Madrid), en el Servicio de AtenciÃ³n TelefÃ³nica 010-LÃ­nea Madrid (accesible marcando 010 desde nuestra ciudad o bien 91 529 82 10) y en nuestra cuenta de la red social Twitter @lineamadrid.\n\n");
			// Lo enviamos.
			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();

		} catch (SendFailedException e) {
			return e.toString();

		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		return "ok";
	}
	
	@Override
	public String sendBaja(String to, Residente residente) {
		try {
			// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmailInfo());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(correoNoReply));
			message.setHeader("Content-Type", "text/html");
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));
			message.setSubject("Baja Abonado BICIMAD - Ayuntamiento de Madrid.\n Codifo de identificaciÃ³n/cÃ³digo de usuario: " + residente.getId() + "\n" + residente.getNombre() + " " + residente.getApellido1());
			message.setText("Se ha efectuado su baja como abonado en la base de datos de BICIMAD\n"
					+ "Estamos a su disposiciÃ³n en www.madrid.es/contactar, en nuestras Oficinas de AtenciÃ³n a la CiudadanÃ­a (LÃ­nea Madrid), en el Servicio de AtenciÃ³n TelefÃ³nica 010-LÃ­nea Madrid (accesible marcando 010 desde nuestra ciudad o bien 91 529 82 10) y en nuestra cuenta de la red social Twitter @lineamadrid.\n\n");			
			// Lo enviamos.
			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();

		} catch (SendFailedException e) {
			return e.toString();

		} catch (Exception e) {
			e.printStackTrace();
			return e.toString();
		}
		return "ok";
	}

	public boolean sendErrorBBDDPago(String texto) {
		try {// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmail());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply@bonopark.es"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					"noreply@bonopark.es"));
			message.setSubject("ERROR INSERTAR DATOS PAGO");

			message.setText("Ha existido un error a la hora de almacenar los datos de una persona que ha realizado el pago correctamente. Los datos de la persona son los siguientes: " + texto);
			// Lo enviamos.

			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean sendErrorSocket(String texto) {
		try {// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmail());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply@bonopark.es"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					"noreply@bonopark.es"));
			message.setSubject("ERROR INSERTAR DATOS EN LAS ESTACIONES");

			message.setText("Ha existido un error en el Socket al mandar a todas las estaciones.\n "
			+ "La sentencia a enviar es : " + texto);
			// Lo enviamos.

			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean sendErrorBBDDFactura(String texto) {
		try {// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmail());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply@bonopark.es"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					"noreply@bonopark.es"));
			message.setSubject("ERROR INSERTAR DATOS FACTURA");

			message.setText("Ha existido un error a la hora de almacenar factura. Los datos de la persona son los siguientes: " + texto);
			// Lo enviamos.

			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
		
	}
	
	public boolean sendError(String texto) {
		try {// Preparamos la sesion
//			Session session = Session.getDefaultInstance(conexionEmail());
			Session session = getSession();
			// Construimos el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress("noreply@bonopark.es"));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					"noreply@bonopark.es"));
			message.setSubject("ERROR");

			message.setText("Ha existido un error : " + texto);
			// Lo enviamos.

			Transport t = session.getTransport("smtp");
			t.connect(correoNoReply, passwordNoReply);
			t.sendMessage(message, message.getAllRecipients());
			// Cierre.
			t.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
		
	}


}
