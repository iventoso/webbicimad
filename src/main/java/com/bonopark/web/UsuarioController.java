package com.bonopark.web;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.bonopark.domain.Abono;
import com.bonopark.domain.Estacion;
import com.bonopark.domain.Residente;
import com.bonopark.service.ValidatorCambioContrasena;
import com.bonopark.service.ResidenteManager;
import com.bonopark.service.ValidatorInscripcion;
import com.bonopark.domain.CambioContrasena;

@Controller
public class UsuarioController implements Serializable {

//	private String recargaSaldoTPV = "http://146.255.100.64:8080/bicimad/recargaSaldoTPV.html";
	private String recargaSaldoTPV = "http://www.bicimad.com/recargaSaldoTPV.html";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private ResidenteManager residenteManager;
	@Autowired
	private ValidatorCambioContrasena validatorCambioContrasena;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private ValidatorInscripcion validatorInscripcion;

	// @InitBinder
	// protected void initBinder(WebDataBinder binder) {
	// binder.setValidator(new ValidatorInscripcion()); // registramos el
	// // validador
	// }

	@ModelAttribute("residente")
	public Residente populateForm() {
		return new Residente(); // creamos el bean para que se pueda popular
	}

	@ModelAttribute("cambioContrasena")
	public CambioContrasena populateForm1() {
		return new CambioContrasena(); // creamos el bean para que se pueda
										// popular
	}

	@RequestMapping(value = "/club/usuario")
	public ModelAndView usuarioInicio(ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {
		// Principal seria el nombre de Usuario al hacer login
		Principal principal = request.getUserPrincipal();
		System.out.println("Principal getName : " + principal.getName());
		Map<String, Object> myModel = new HashMap<String, Object>();
		try {
			

		Residente residente = residenteManager.getUsuariobyDni(principal
				.getName());

		myModel = mostrarUsuarioInicio(request, response, residente);
		return new ModelAndView((String) myModel.get("view"), "model", myModel);
		} catch (Exception e) {
			myModel.put("mensaje", "No ha sido posible la conexión con el servidor");
			return new ModelAndView("/mensaje", "model", myModel);
		}

	}

	@RequestMapping(value = "/club/recorridos", method = RequestMethod.GET)
	public ModelAndView recorridos(ModelMap model, Principal principal,
			HttpServletRequest request, HttpServletResponse response) {
		Residente residente = residenteManager.getUsuariobyDni(principal
				.getName());
		Map<String, Object> myModel = new HashMap<String, Object>();
		if (residente.getAbono().isEmpty() == true) {
			myModel.put("mensaje", "recorridos.sinabono");
			myModel.put("titulomensaje", "Recorridos");
			myModel.put("opcionmenu", "recorridos");
			return new ModelAndView("/club/mensaje", "model", myModel);
		}

		myModel.put("residente", residente);
		// myModel.put("recorrido", residente.getAbonoResidente().iterator()
		// .next().getRfid().getDatos_recorrido());
		return new ModelAndView("/club/recorridos", "model", myModel);

	}

	@RequestMapping(value = "/club/validardatos.html", method = RequestMethod.POST)
	public ModelAndView actualizarUsuario(
			@ModelAttribute("residente") Residente residente,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		validatorInscripcion.validate(residente, result);
		HomeController.detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();

		Residente usuarioActual = residenteManager.getUsuariobyDni(request
				.getUserPrincipal().getName());
		// si no hay errores volvemos a la vista del formulario
		if (result.hasErrors()) {
			return new ModelAndView("/club/usuarioinicio", "model",
					mostrarUsuarioInicio(request, response, residente));
		}
		usuarioActual.setNombre(residente.getNombre());
		usuarioActual.setApellido1(residente.getApellido1());
		usuarioActual.setApellido2(residente.getApellido2());
		usuarioActual.setTipoDocumento(residente.getTipoDocumento());
		usuarioActual.setFecha_nacimiento(residente.getFecha_nacimiento());
		usuarioActual.setMovil(residente.getMovil());
		usuarioActual.setEmail(residente.getEmail());
		usuarioActual.setDireccion(residente.getDireccion());
		usuarioActual.setMunicipio(residente.getMunicipio());
		usuarioActual.setCodigo_postal(residente.getCodigo_postal());

		residenteManager.updateUsuario(usuarioActual);
		myModel = mostrarUsuarioInicio(request, response, usuarioActual);
		return new ModelAndView((String) myModel.get("view"), "model", myModel);
	}

	public static Map<String, Object> mostrarUsuarioInicio(
			HttpServletRequest request, HttpServletResponse response,
			Residente residente) {

		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("usuario", residente);
		myModel.put("view", "/club/usuarioinicio");
		return myModel;

	}

	@RequestMapping(value = "/club/cambiopwd")
	public ModelAndView usuarioCambioPwd(ModelMap model,
			HttpServletRequest request, HttpServletResponse response,
			Principal principal) {

		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("dni", principal.getName());
		return new ModelAndView("/club/cambiopwd", "model", myModel);
	}

	@RequestMapping(value = "/club/recargasaldo")
	public ModelAndView usuarioRecargaSaldo(ModelMap model,
			HttpServletRequest request, HttpServletResponse response,
			Principal principal) {
		String recarga = "0";
		Enumeration e = request.getParameterNames();
		while (e.hasMoreElements()) {
			String imput = (String) e.nextElement();
			if (!(imput.equals("saldoactual")))
				recarga = imput;
		}
		
		System.out.println("Recarga de :" + recarga);
		String saldoActual = request.getParameter("saldoactual");

		Map<String, Object> myModel = new HashMap<String, Object>();
		java.text.SimpleDateFormat formador = new java.text.SimpleDateFormat(
				"YYMMddHHmmss");
		String merchantOrder = formador.format(new java.util.Date());
		String firmaTPV = InscripcionController.calculoFirmaAlta(recarga + "00",
				merchantOrder, recargaSaldoTPV);
		myModel.put("firmaTPV", firmaTPV);
		myModel.put("saldoactual", saldoActual);
		myModel.put("recarga", recarga);
		myModel.put("recargaTPV", recarga + "00");
		myModel.put("dni", principal.getName());
		myModel.put("notificacionTPV", recargaSaldoTPV);
		myModel.put("merchantOrder", merchantOrder);
		return new ModelAndView("/club/recargasaldo", "model", myModel);
	}

	@RequestMapping(value = "/club/saldo")
	public ModelAndView usuarioSaldo(ModelMap model,
			HttpServletRequest request, HttpServletResponse response,
			Principal principal) {
		Map<String, Object> myModel = new HashMap<String, Object>();
		Abono abonoActivado = null;
		Residente usuarioActual = residenteManager.getUsuariobyDni(request
				.getUserPrincipal().getName());
		List<Abono> abonos = usuarioActual.getAbono();
		Iterator i = abonos.listIterator(); // Le solicito a la lista que me
											// devuelva un iterador con todos
											// los el elementos contenidos en
											// ella
		// Mientras que el iterador tenga un proximo elemento
		while (i.hasNext()) {
			Abono b = (Abono) i.next(); // Obtengo el elemento contenido pero
										// visto como un abono
			if (b.getActivado() == 1) {
				abonoActivado = b;
			}

		}
		if (abonoActivado == null) {
			myModel.put("titulomensaje", "Saldo");
			myModel.put("mensaje", "saldo.sinabono");
			myModel.put("opcionmenu", "saldo");
			return new ModelAndView("/club/mensaje", "model", myModel);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");	
		Calendar calendar = new GregorianCalendar(2014,6,10);
		Date fechaHoy = calendar.getTime();
		
		if (abonoActivado.getFecha().before(fechaHoy)) {
			
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			try {
				Date dateIN = formatter.parse(sdf.format(calendar.getTime()));
				abonoActivado.setFecha(dateIN);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		myModel.put("abono", abonoActivado);
		return new ModelAndView("/club/saldo", "model", myModel);
	}

	@RequestMapping(value = "/club/cambiopwdvalidate", method = RequestMethod.POST)
	public ModelAndView usuarioCambioPwdComprobacion(
			@ModelAttribute("cambioContrasena") CambioContrasena datos,
			BindingResult result, HttpServletRequest request) {
		validatorCambioContrasena.validate(datos, result);
		if (result.hasErrors()) {
			return new ModelAndView("/club/cambiopwd");
		} else {
			String newPassword = datos.getPwdNueva();
			Residente usuarioActual = residenteManager.getUsuariobyDni(request
					.getUserPrincipal().getName());
			usuarioActual.setPassword(passwordEncoder.encodePassword(
					newPassword, null));
			residenteManager.updateUsuario(usuarioActual);
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("mensaje", "cambiopwd.confirmacion");
			myModel.put("opcionmenu", "cambiopwd");
			return new ModelAndView("/club/mensaje", "model", myModel);
		}
	}
}
