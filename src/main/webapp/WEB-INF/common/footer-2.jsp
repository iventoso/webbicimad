<!-- footer-2 -->
<div class="footer-2">
	<div class="container">

		<div class="social-btns pull-right">
				<div class="fui-facebook"></div>
				<div class="fui-twitter"></div>
		</div>
		<div class="additional-links">
			Copyright 2014, Derechos Reservados - Bonopark SL - <a href="/resources/docs/Instrucciones servicio bicicleta.pdf">Condiciones
				de Uso</a> - <a href="#">Poltica de Privacidad</a>
		</div>
	</div>
</div>

