<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD :: Normativa</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">
		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="seguridad" value="active" />
		</jsp:include>


		<!-- content-15b -->
		<div class="content-15b v-center bg-bici">

			<div class="container">
				<div class="row">
					<h1></h1>
					<h3>Normativa</h3>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h5>Conozca la normativa específica de la bicicleta en la
							ciudad de Madrid:</h5>
						<p>
							<a <fmt:message key="normativa.pdfnormativa" />>Instrucciones de uso y funcionamiento de BiciMAD</a>
						</p>
						<p><a target="_blank" href="http://www.madrid.es/portales/munimadrid/es/Inicio/Ayuntamiento/Movilidad-y-Transportes/Oficina-de-la-bici/Normativa/Normativa-sobre-el-uso-de-la-bici?vgnextfmt=detNavegacion&vgnextoid=3b70a9b917d89210VgnVCM2000000c205a0aRCRD&vgnextchannel=bbe3ece9eafc8210VgnVCM1000000b205a0aRCRD">Normativa sobre el uso de la bici</a></p>
                   
					</div>
				</div>
			</div>

			<div class="divider-section-big"></div>

		</div>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>