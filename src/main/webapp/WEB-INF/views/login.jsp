<%@page language="java" isELIgnored="false"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>

<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%> --%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>

<body onload='document.f.j_username.focus();'>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="usuarios" value="active" />
		</jsp:include>


		<!-- contacts-2 -->
		<div class="contacts-2">
		<div class="container">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-6 col-xs-left">
						<c:if test="${not empty error}">
							<div class="errorblock">
								<fmt:message key="login.incorrecto" />
							</div>
						</c:if>
						<form title="login" id="form-log" method="post" name="f"
							action="<c:url value='j_spring_security_check' />">
							<label for="j_username" class="h6">Número de Documento</label> <input type='text'
								name='j_username' value='' class="form-control" id="j_username" /> <label
								class="h6" for="j_password">Contraseña</label> <input type='password'
								name='j_password' id="j_password" class="form-control" /> 
                                <input title="submit" type="submit" class="btn btn-primary" name="submit" value="ENVIAR" />
								<a id="olvide_pwd" href="olvidepwd.html" class="olvide_pwd-left"><p>Olvidé mi Contraseña</p></a>
								<div class="olvide_pwd-right"><p>¿No estás registrado? Regístrate <a id="olvide_pwd" href="inscribete.html" > aquí</a></p> </div>
								
						</form>
						
					</div>
				</div>
			</div>
		</div>

		</div>
		<h1></h1>
		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>
