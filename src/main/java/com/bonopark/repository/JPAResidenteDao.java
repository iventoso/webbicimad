package com.bonopark.repository;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bonopark.domain.Abono;
import com.bonopark.domain.Factura;
import com.bonopark.domain.Residente;
import com.bonopark.domain.Tutor;
import com.bonopark.domain.Usuario;
import com.bonopark.domain.UsuarioInfoExtra;

@Repository(value = "residenteDao")
public class JPAResidenteDao implements ResidenteDao {

	private EntityManager em = null;

	/*
	 * Sets the entity manager.
	 */
	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Residente> getUsuarioList() {
		return em.createQuery("select p from Residente p order by p.DNI")
				.getResultList();
	}

	@Transactional(readOnly = false)
	public String saveUsuario(Usuario usuario) {
		try {
			em.clear();
			em.merge(usuario);

		} catch (Exception e) {
			em.getTransaction().rollback();
			return e.toString() + " Error en New Usuario";
		}

		return null;

	}

	@Transactional(readOnly = false)
	public String saveResidente(Residente residente) {
		try {
			em.clear();
			em.merge(residente);
		} catch (Exception e) {
			em.getTransaction().rollback();
			return e.toString() + " Error en JPSResidenteDao.saveResidente"+ 
			"e.getMessage(): " + e.getMessage() + "\n" +
			"e.getCause(): " + e.getCause() + "\n" +
			"residente: " + residente.toString() + "\n";
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public Residente getUsuarioByDni(String DNI) {
		Residente residente = null;
		try {

			em.clear();
			 Query query =  em.createQuery("select p from Residente p where upper(p.dni) = ?1").setParameter(1, DNI.toUpperCase());
			 
			 if (!query.getResultList().isEmpty()) {
				residente = (Residente) query.getSingleResult();
			}
			

		} catch (Exception e) {
			// TODO: handle exception
			System.out
					.println("getUsuarioByDni. Error en la consulta");
			System.out.println("**************** Excepcion ***************"+DNI+" "+e);
			System.out.println("Fecha: " + Calendar.getInstance().getTime());
			System.out.println("e.getMessage(): " + e.getMessage() + "\n" +
			"e.getCause(): " + e.getCause() + "\n" +
			"residente: " + DNI + "\n"); 
		}
		return residente;

	}

	@Override
	@Transactional(readOnly = true)
	public Residente getUsuarioByConfirmacion(String confirmacion) {
		try {

			Residente residente = (Residente) em
					.createQuery(
							"select p from Residente p where p.confirmacion =?1")
					.setParameter(1, confirmacion).getSingleResult();
			return residente;
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	@Transactional(readOnly = false)
	public boolean updateUsuario(Residente residente) {
		System.out.println("JPA update Usuario con DNI: " + residente.getDni());
		//En la base de datos es el idioma, lo utilizamos para la validacion
		residente.setTipoDocumento("es");
		try {
			em.merge(residente);
			return true;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return false;
		}
	}

	@Override
	public boolean deleteUsuarioByDni(String dni) {
		try {
			em.createQuery("delete p from Residente p where p.dni =?1")
					.setParameter(1, dni);
			return true;

		} catch (Exception e) {
			em.getTransaction().rollback();
			return false;
		}

	}

	@Override
	public int numFactura(String anno) {
		try {

			int numFactura = (Integer) em
					.createQuery(
							"SELECT id_factura FROM Factura WHERE YEAR( fecha ) = "
									+ anno + " ORDER BY id_factura DESC")
					.setMaxResults(1).setFirstResult(0).getSingleResult();
			return numFactura;
		} catch (NoResultException e) {
			return 0;
		} catch (Exception e) {
			return -1;
		}
	}

	@Transactional(readOnly = false)
	public String saveDatosFactura(Factura factura) {
		try {
			em.merge(factura);

		} catch (Exception e) {
			em.getTransaction().rollback();
			return e.toString();
		}

		return null;
	}

	@Override
	@Transactional(readOnly = false)
	public double updateAbono(Residente residente, double importe) {
		try {
			double saldoActual = 0.0;
			
			Abono abono = (Abono) em
					.createQuery(
							"select p from Abono p where UPPER(p.residente.dni) = UPPER(?1) AND p.activado=1")
					.setParameter(1, residente.getDni()).getSingleResult();

			if (abono != null) {
				saldoActual = abono.getSaldo();
				saldoActual += importe;
				abono.setSaldo(saldoActual);
				em.merge(abono);
			}

			return saldoActual;
		} catch (Exception e) {
			em.getTransaction().rollback();
			return -1;
		}
	}

	@Transactional(readOnly = false)
	public String saveUsuario(Residente residente,
			Usuario usuario, UsuarioInfoExtra usuarioInfoExtra, Tutor tutor) {
		try {
			em.clear();
			em.merge(usuario);

		} catch (Exception e) {
			em.getTransaction().rollback();
			e.getMessage();
			e.getCause();
			return e.toString() + "\n Error en New Usuario" + 			
			"Fecha: " + Calendar.getInstance().getTime() + "\n" +
			"e.getMessage(): " + e.getMessage() + "\n" +
			"e.getCause(): " + e.getCause() + "\n" +
			"residente: " + residente.toString() + "\n" + 
			"usuario: " + usuario.toString() + "\n" +
			"usuarioInfoExtra: " + usuarioInfoExtra.toString() + "\n";
		}
		if (tutor != null) {
			
			try {
//				em.clear();
				em.merge(tutor);
	
			} catch (Exception e) {
//				em.getTransaction().rollback();
//				return e.toString() + " Error en New Tutor";
			}
		}
		try {
			// em.clear();
			em.merge(residente);
		} catch (Exception e) {
			em.getTransaction().rollback();
			return e.toString() + " Error en residente"+ 
			"Fecha: " + Calendar.getInstance().getTime() + "\n" +
			"e.getMessage(): " + e.getMessage() + "\n" +
			"e.getCause(): " + e.getCause() + "\n" +
			"residente: " + residente.toString() + "\n" + 
			"usuario: " + usuario.toString() + "\n" +
			"usuarioInfoExtra: " + usuarioInfoExtra.toString() + "\n";
		}
		try {
			// em.clear();
			em.merge(usuarioInfoExtra);
		} catch (Exception e) {
			em.getTransaction().rollback();
			return e.toString() + " Error en usuarioInfoExtra"+ 
			"Fecha: " + Calendar.getInstance().getTime() + "\n" +
			"e.getMessage(): " + e.getMessage() + "\n" +
			"e.getCause(): " + e.getCause() + "\n" +
			"residente: " + residente.toString() + "\n" + 
			"usuario: " + usuario.toString() + "\n" +
			"usuarioInfoExtra: " + usuarioInfoExtra.toString() + "\n";
		}

		return null;

	}

}
