package com.bonopark.domain;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "estacion")
public class Estacion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	private int idestacion;
	private String numero_estacion;
	private String nombre;
	private String direccion;
	private String latitud;
	private String longitud;
	private String barrio;
	private Date fecha_ultima_conexion;
	@Column(columnDefinition = "INT")
	private boolean luz;
	@Column(columnDefinition = "INT")
	private boolean activo; 
	private String ip;
	@OneToMany(fetch=FetchType.EAGER, mappedBy = "estacion")
	private List<Base> bases = new ArrayList<Base>();

	public Estacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdestacion() {
		return idestacion;
	}

	public void setIdestacion(int idestacion) {
		this.idestacion = idestacion;
	}

	public String getNumero_estacion() {
		return numero_estacion;
	}

	public void setNumero_estacion(String numero_estacion) {
		this.numero_estacion = numero_estacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public Date getFecha_ultima_conexion() {
		return fecha_ultima_conexion;
	}

	public void setFecha_ultima_conexion(Date fecha_ultima_conexion) {
		this.fecha_ultima_conexion = fecha_ultima_conexion;
	}

	public boolean isLuz() {
		return luz;
	}

	public void setLuz(boolean luz) {
		this.luz = luz;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public List<Base> getBases() {
		return bases;
	}

	public void setBases(List<Base> bases) {
		this.bases = bases;
	}

	@Override
	public String toString() {
		return "Estacion [idestacion=" + idestacion 
				+ ", nombre=" + nombre
				+ ", direccion=" + direccion 
				+ ", latitud=" + latitud 
				+ ", longitud=" + longitud
				+ ", barrio=" + barrio 
				+ ", fecha_ultima_conexion=" + fecha_ultima_conexion
				+ ", luz=" + luz 
				+ ", ip=" + ip + "]";
	}

	public boolean isActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

}
