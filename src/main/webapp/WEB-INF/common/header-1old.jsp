
<!-- header-1 -->
<header class="header-1">
	<div class="container">
		<div class="row">

			<div class="navbar col-sm-12" role="navigation">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle"></button>
					<a class="brand" href="index.html"><img src="/resources/img/logo@2x.png"
						width="50" height="50" alt=""> Bici<strong>MAD</strong></a>

				</div>
				<div class="navbar-collapse collapse">

					<ul class="nav navbar-nav pull-right">

						<li class="dropdown"><a href="#" data-toggle="dropdown"
							class="dropdown-toggle">�Que es?</a>
							<ul class="dropdown-menu">
								<li><a href="que.html">Descripci�n del Servicio</a></li>
								<li class="divider"></li>
								<li><a href="componentes.html">Componentes</a></li>
								<li class="divider"></li>
								<li><a href="preguntas.html">Preguntas Frecuentes</a></li>
							</ul></li>

						<li class="dropdown"><a href="#" data-toggle="dropdown"
							class="dropdown-toggle">Servicios</a>
							<ul class="dropdown-menu">
								<li><a href="tarifas.html">Informaci�n de Tarifas</a></li>
								<li class="divider"></li>
								<li><a href="seguro.html">Seguro</a></li>
								<li class="divider"></li>
								<li><a href="horario.html">Horarios</a></li>
								<li class="divider"></li>
								<li><a href="normativa.html">Normativa</a></li>
								<li class="divider"></li>
								<li><a href="app.html">Descarga de App</a></li>
							</ul></li>

						<li class="dropdown"><a href="#" data-toggle="dropdown"
							class="dropdown-toggle">Contactos</a>
							<ul class="dropdown-menu">
								<li><a href="mapa.html">Mapa Oficinas</a></li>
								<li class="divider"></li>
								<li><a href="telefono.html">Tel�fonos</a></li>
							</ul></li>

						<li class="dropdown"><a href="#" data-toggle="dropdown"
							class="dropdown-toggle">Usuarios</a>
							<ul class="dropdown-menu">
								<li><a href="info.html">T� Informaci�n</a></li>
								<li class="divider"></li>
								<li><a href="sistema.html">Informaci�n del Sistema</a></li>
								<li class="divider"></li>
								<li><a href="recarga.html">Recarga de Saldo</a></li>
							</ul></li>

						<li class="dropdown"><a href="#" data-toggle="dropdown"
							class="dropdown-toggle">Inscribete</a>
							<ul class="dropdown-menu">
								<li><a href="datos.html">Rellene sus Datos</a></li>
								<li class="divider"></li>
								<li><a href="compra.html">Compra</a></li>

							</ul></li>

						<li class="dropdown"><a href="login.html"
							class="dropdown-toggle">Recarga</a></li>
					</ul>

				</div>
			</div>
		</div>
	</div>
</header>
<!-- header-1 -->