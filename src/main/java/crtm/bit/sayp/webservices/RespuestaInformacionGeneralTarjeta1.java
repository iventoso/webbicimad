/**
 * RespuestaInformacionGeneralTarjeta1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package crtm.bit.sayp.webservices;

public class RespuestaInformacionGeneralTarjeta1  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private java.lang.Long ICallId;

    private java.lang.String ICallLog;

    private java.lang.Integer IResultCode;

    private java.lang.Integer ITarjetaActiva;

    private java.lang.Integer ITieneTituloActivo;

    public RespuestaInformacionGeneralTarjeta1() {
    }

    public RespuestaInformacionGeneralTarjeta1(
           java.lang.Long ICallId,
           java.lang.String ICallLog,
           java.lang.Integer IResultCode,
           java.lang.Integer ITarjetaActiva,
           java.lang.Integer ITieneTituloActivo) {
           this.ICallId = ICallId;
           this.ICallLog = ICallLog;
           this.IResultCode = IResultCode;
           this.ITarjetaActiva = ITarjetaActiva;
           this.ITieneTituloActivo = ITieneTituloActivo;
    }


    /**
     * Gets the ICallId value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @return ICallId
     */
    public java.lang.Long getICallId() {
        return ICallId;
    }


    /**
     * Sets the ICallId value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @param ICallId
     */
    public void setICallId(java.lang.Long ICallId) {
        this.ICallId = ICallId;
    }


    /**
     * Gets the ICallLog value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @return ICallLog
     */
    public java.lang.String getICallLog() {
        return ICallLog;
    }


    /**
     * Sets the ICallLog value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @param ICallLog
     */
    public void setICallLog(java.lang.String ICallLog) {
        this.ICallLog = ICallLog;
    }


    /**
     * Gets the IResultCode value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @return IResultCode
     */
    public java.lang.Integer getIResultCode() {
        return IResultCode;
    }


    /**
     * Sets the IResultCode value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @param IResultCode
     */
    public void setIResultCode(java.lang.Integer IResultCode) {
        this.IResultCode = IResultCode;
    }


    /**
     * Gets the ITarjetaActiva value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @return ITarjetaActiva
     */
    public java.lang.Integer getITarjetaActiva() {
        return ITarjetaActiva;
    }


    /**
     * Sets the ITarjetaActiva value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @param ITarjetaActiva
     */
    public void setITarjetaActiva(java.lang.Integer ITarjetaActiva) {
        this.ITarjetaActiva = ITarjetaActiva;
    }


    /**
     * Gets the ITieneTituloActivo value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @return ITieneTituloActivo
     */
    public java.lang.Integer getITieneTituloActivo() {
        return ITieneTituloActivo;
    }


    /**
     * Sets the ITieneTituloActivo value for this RespuestaInformacionGeneralTarjeta1.
     * 
     * @param ITieneTituloActivo
     */
    public void setITieneTituloActivo(java.lang.Integer ITieneTituloActivo) {
        this.ITieneTituloActivo = ITieneTituloActivo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RespuestaInformacionGeneralTarjeta1)) return false;
        RespuestaInformacionGeneralTarjeta1 other = (RespuestaInformacionGeneralTarjeta1) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ICallId==null && other.getICallId()==null) || 
             (this.ICallId!=null &&
              this.ICallId.equals(other.getICallId()))) &&
            ((this.ICallLog==null && other.getICallLog()==null) || 
             (this.ICallLog!=null &&
              this.ICallLog.equals(other.getICallLog()))) &&
            ((this.IResultCode==null && other.getIResultCode()==null) || 
             (this.IResultCode!=null &&
              this.IResultCode.equals(other.getIResultCode()))) &&
            ((this.ITarjetaActiva==null && other.getITarjetaActiva()==null) || 
             (this.ITarjetaActiva!=null &&
              this.ITarjetaActiva.equals(other.getITarjetaActiva()))) &&
            ((this.ITieneTituloActivo==null && other.getITieneTituloActivo()==null) || 
             (this.ITieneTituloActivo!=null &&
              this.ITieneTituloActivo.equals(other.getITieneTituloActivo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getICallId() != null) {
            _hashCode += getICallId().hashCode();
        }
        if (getICallLog() != null) {
            _hashCode += getICallLog().hashCode();
        }
        if (getIResultCode() != null) {
            _hashCode += getIResultCode().hashCode();
        }
        if (getITarjetaActiva() != null) {
            _hashCode += getITarjetaActiva().hashCode();
        }
        if (getITieneTituloActivo() != null) {
            _hashCode += getITieneTituloActivo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RespuestaInformacionGeneralTarjeta1.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://webservices.sayp.bit.crtm/", "respuestaInformacionGeneralTarjeta1"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ICallId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ICallId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ICallLog");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ICallLog"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IResultCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "IResultCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ITarjetaActiva");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ITarjetaActiva"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ITieneTituloActivo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ITieneTituloActivo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
