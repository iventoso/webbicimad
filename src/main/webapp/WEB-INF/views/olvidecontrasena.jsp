<%@page language="java" isELIgnored="false"
	contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Dbizi</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="usuarios" value="active" />
		</jsp:include>
		<div class="contacts-2">
		<div class="container">
			<div class="col-xs-12">
				<div class="row">
					<div class="col-xs-6 col-xs-left">
						<form:form id="form-log" method="post" action="validarolvidopwd.html"
							commandName="residente">
							<div class="form_settings">
							<p>Introduzca su N�mero de Documento y su Email y le enviaremos una nueva contrase�a a su direcci�n de Email</p>
								<form:errors path="*" cssClass="errorblock" element="div" />
								<p>
									<span><form:label class="h6" path="dni">DNI: </form:label></span>
									<form:input path="dni" type="text" required="required" class="form-control" />
								</p>
								<p>
									<span><form:label class="h6" path="email">
											<fmt:message key="inscripcion.ema" />:</form:label></span>
									<form:input path="email" type="email" required="required" class="form-control"/>
								</p>
								<input type="submit" class="btn btn-primary"
									value="<fmt:message
						key="inscripcion.env" />" />
							</div>

						</form:form>
					</div>
				</div>
			</div>
		</div>
		</div>
		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />
</body>
</html>