<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="comofunciona" value="active" />
		</jsp:include>

		<!-- content-15 -->
		<div class="content-15 v-center bg-bici">

			<div class="container">
				<div class="row">
					<h3>La Estación</h3>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h6>Además de los anclajes y las bicicletas, cada estación
							dispone, además, de un punto de información con pantallas
							interactivas (tótem).</h6>
					</div>
				</div>
				<div class="row features">
					<div class="col-xs-6">
						<p>
							<img src="/resources/img/Estacion.jpg" alt="estación" />
						</p>
					</div>
					<div class="col-xs-6">
						<p>Éstas permiten realizar funciones como consultar la
							disponibilidad de anclajes libres y bicicletas en tiempo real,
							realizar reservas o recargar el saldo de su tarjeta simplemente
							navegando a través del sencillo menú de la pantalla táctil.</p>

					</div>
				</div>
			</div>
			<h1></h1>

			<div class="divider-section-small bg-bici"></div>
		</div>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>