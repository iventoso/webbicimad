<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD ::</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="abonostarifas" value="active" />
		</jsp:include>

		<!-- content-15 -->
		<div class="content-15 v-center bg-bici">
			<div>
				<div class="container">
					<div class="row">
						<h3>Tarifas</h3>
					</div>
				</div>
			</div>
		</div>

		<!-- price-2 -->
		<div class="price-2 bg-bici">
			<div class="container">
				<div class="plans">
					<div class="plan">
						<div class="title">ABONADO ANUAL</div>
						<div class="description">
							<div class="description-box">
								Tarifa abono anual sin abono transporte del Consorcio Regional
								de Transportes de la Comunidad de Madrid: <b>25 €</b>
							</div>
							<div class="description-box">
								Tarifa abono anual con abono transporte del Consorcio Regional
								de Transportes de la Comunidad de Madrid <b>15 €</b>
							</div>
							<div class="description-box">
								Primera fracción de 30 minutos: <b>0,50 €</b>
							</div>
							<div class="description-box">
								Siguiente fracción de 30 minutos:<b> 0,60 €</b>
							</div>
							<div class="description-box">
								Bonificación por coger la bicicleta en estación excedentaria
								(con más de un 70% de ocupación):<b> 0,10 €</b>
							</div>
							<div class="description-box">
								Bonificación por devolver la bicicleta en estación deficitaria
								(con menos de un 30% de ocupación):<b> 0,10 €</b>
							</div>
							<div class="description-box">
								Bonificación por reserva anclaje en destino (no acumulable con
								bonificación por devolución en estación deficitaria):<b>
									0,10 € </b>
							</div>
							<div class="description-box">
								Tarifa de penalización por haber excedido las dos horas, por
								hora o fracción:<b> 4 €</b>
							</div>
						</div>
					</div>

					<div class="plan plan-2">
						<div class="title">TARJETA OCASIONAL</div>
						<div class="description">
							<div class="description-box">
								<p style="color: #a6555a;">Actualmente no disponible</p>
							</div>
							<div class="description-box">
								Tarifa primera hora o fracción: <b>2 €</b>
							</div>
							<div class="description-box">
								Tarifa segunda hora o fracción: <b>4 €</b>
							</div>
							<div class="description-box">
								Bonificación por coger la bicicleta en estación excedentaria
								(con más de un 70% de ocupación):<b>0,10 €</b>
							</div>
							<div class="description-box">
								Bonificación por devolver la bicicleta en estación deficitaria
								(con menos de un 30% de ocupación):<b> 0,10 €</b>
							</div>
							<div class="description-box">
								Bonificación por reserva anclaje en destino (no acumulable con
								bonificación por devolución en estación deficitaria):<b>
									0,10 € </b>
							</div>
							<div class="description-box">
								Tarifa de penalización por haber excedido las dos horas, por
								hora o fracción:<b> 4 €</b>
							</div>
							<div class="description-box">-</div>
							<div class="description-box">-</div>
						</div>
					</div>



				</div>
			</div>
			<div class="divider-section-small bg-bici"></div>
			<h1></h1>
		</div>


		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>