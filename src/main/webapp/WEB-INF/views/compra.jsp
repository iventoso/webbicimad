<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD ::</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp" />
		<div class="divider-section"></div>
		<!-- contacts-2 -->
		<section class="contacts-2 bg-bici">
		<div class="container">
			<div class="col-sm-12">

				<div class="row">

					<div class="col-sm-6 col-sm-offset-3">
						<h3>Recarga tu tarjeta</h3>
						<form>
							<label class="h6">Usuario</label> <input type="text"
								class="form-control"> <label class="h6">Contraseña</label>
								<input type="text" class="form-control">
									<div class="btn btn-primary">
										<a href="recarga2.html">ENVIAR</a>
									</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		</section>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>