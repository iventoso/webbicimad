<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
<script src="<%=request.getContextPath()%>/resources/js/sorttable.js"></script>
<style>
	.divider-section-small{ min-height:350px !important;}
</style>
</head>
<body>
	<div class="page-wrapper">

		<!-- header-1 -->
		<c:if test="${model.opcionmenu eq 'usuario' }">	
			<jsp:include page="/WEB-INF/common/usuarioheader.jsp">
				<jsp:param name="usuario" value="active"/>
			</jsp:include>
		</c:if>
		<c:if test="${model.opcionmenu eq 'recorridos' }">	
			<jsp:include page="/WEB-INF/common/usuarioheader.jsp">
				<jsp:param name="recorridos" value="active"/>
			</jsp:include>
		</c:if>
		<c:if test="${model.opcionmenu eq 'saldo' }">	
			<jsp:include page="/WEB-INF/common/usuarioheader.jsp">
				<jsp:param name="saldo" value="active"/>
			</jsp:include>
		</c:if>
		<!-- content-15b -->

		<div class="content-15 v-center bg-bici">
		<div class="container">
			<div class="row">
				<h3>${model.titulomensaje}</h3>
			</div>
			<p>
				<c:set var="mensaje">
					<c:out value="${model.mensaje}" />
				</c:set>
				<fmt:message key="${mensaje}" />
			</p>
			<div class="divider-section-small bg-bici"></div>
		</div>

	</div>


    <!-- footer-6 -->
    <jsp:include page="/WEB-INF/common/footer-6.jsp" />

    <!-- footer-2 -->
    <jsp:include page="/WEB-INF/common/footer-2.jsp" />

	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>