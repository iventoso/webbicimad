package com.bonopark.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="abono") 
public class Abono implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="idrfid")
	private String idrfid;
	
	@OneToOne(mappedBy = "abono")
	private Rfid rfid;

	@ManyToOne
	@JoinColumn(name="dni")
    private Residente residente;
	

//	@Id
//	private String dni;
//	@Id
//	private String idrfid;
	
	private Date fecha;
	private int duracion;
	private int tipo;
	private int activado;

	private int bicis_simultaneas;
	private int motivo;
	private double saldo;
	
	public Rfid getRfid() {
		return rfid;
	}
	public void setRfid(Rfid rfid) {
		this.rfid = rfid;
	}

	
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	public Residente getResidente() {
		return residente;
	}
	public void setResidente(Residente residente) {
		this.residente = residente;
	}
	public String getIdrfid() {
		return idrfid;
	}
	public void setIdrfid(String idrfid) {
		this.idrfid = idrfid;
	}
	
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public int getActivado() {
		return activado;
	}
	public void setActivado(int activado) {
		this.activado = activado;
	}
	public int getBicis_simultaneas() {
		return bicis_simultaneas;
	}
	public void setBicis_simultaneas(int bicis_simultaneas) {
		this.bicis_simultaneas = bicis_simultaneas;
	}
	public int getMotivo() {
		return motivo;
	}
	public void setMotivo(int motivo) {
		this.motivo = motivo;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	@Override
	public String toString() {
		return "Abono [idrfid=" + idrfid + ", rfid=" + rfid + ", residente="
				+ residente + ", fecha=" + fecha + ", duracion=" + duracion
				+ ", tipo=" + tipo + ", activado=" + activado
				+ ", bicis_simultaneas=" + bicis_simultaneas + ", motivo="
				+ motivo + ", saldo=" + saldo + "]";
	}

	
}