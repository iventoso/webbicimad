<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
<script src="<%=request.getContextPath()%>/resources/js/sorttable.js"></script>
<style>
	.divider-section-small{ min-height:300px !important;}
	
	table { 
		width: 100%; 
		border-collapse: collapse; 
	}
	/* Zebra striping */
	tr:nth-of-type(odd) { 
		background: #eee; 
	}
	th { 
		background: #2174b7; 
		color: white; 
		font-weight: bold; 
	}
	td, th { 
		padding: 6px; 
		border: 1px solid #ccc; 
		text-align: left; 
	}
	
	
	@media only screen and (max-width: 760px), (min-device-width: 768px) and (max-device-width: 1024px)  {
	
		table, thead, tbody, th, td, tr { 
			display: block; 
		}
		
		thead tr { 
			position: absolute;
			top: -9999px;
			left: -9999px;
		}
		
		tr { border: 1px solid #ccc; }
		
		td { 
			border: none;
			border-bottom: 1px solid #eee; 
			position: relative;
			padding-left: 50%; 
		}
		
		td:before { 
			position: absolute;
			top: 6px;
			left: 6px;
			width: 45%; 
			padding-right: 10px; 
			white-space: nowrap;
		}
		
		
		td:nth-of-type(1):before { content: "Fecha Desenganche"; }
		td:nth-of-type(2):before { content: "Fecha Enganche"; }
		td:nth-of-type(3):before { content: "Estación Origen"; }
		td:nth-of-type(4):before { content: "Estación Destino"; }
		td:nth-of-type(5):before { content: "Saldo Anterior"; }
		td:nth-of-type(6):before { content: "Saldo Nuevo"; }
		
		#fila1{ display:none !important;}
	}
	
	@media only screen
	and (min-device-width : 320px)
	and (max-device-width : 480px) {
		body { 
			padding: 0; 
			margin: 0; 
			width: 320px; }
		}
	
	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
	}
	

</style>
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/usuarioheader.jsp">
			<jsp:param name="recorridos" value="active" />
		</jsp:include>
		<!-- content-15b -->

		<div class="content-15 v-center bg-bici">
		<div class="container">
			<div class="row">
            	<h1></h1>
				<h3>Recorridos</h3>
			</div>

			<table class="sortable">
				<tr id="fila1">
					<th><fmt:message key="recorridos.fechadesenganche" /></th>
					<th><fmt:message key="recorridos.fechaenganche" /></th>
<%-- 					<th><fmt:message key="recorridos.estacionorigen" /></th>
					<th><fmt:message key="recorridos.estaciondestino" /></th> --%>
					<th>Saldo Anterior</th>
					<th>Saldo Nuevo</th>
				</tr>
				<c:forEach items="${model.residente.abono}" var="abono"
					varStatus="i">
					<c:forEach items="${abono.rfid.enganche}" var="enganche">
						<tr class="seleccion_fila">
							<td><fmt:formatDate type="both" dateStyle="short"
									timeStyle="short" value="${enganche.fecha_desenganche}" /></td>
							<td><fmt:formatDate type="both" dateStyle="short"
									timeStyle="short" value="${enganche.fecha_enganche}" /></td>
<%-- 							<td>${enganche.idestacion_desenganche.nombre}</td>
							<td>${enganche.idestacion_enganche.nombre}</td> --%>
							<td>${enganche.saldo_anterior}</td>
							<td>${enganche.saldo_nuevo}</td>
							
							<%-- <fmt:parseNumber var="saldoanterior" type="number"
									value="${enganche.saldo_anterior}" /> <fmt:parseNumber
									var="saldonuevo" type="number" value="${enganche.saldo_nuevo}" />

								<c:out value="${'${saldoanterior}'+'${saldonuevo}'}" /> --%>
						</tr>
					</c:forEach>
				</c:forEach>
			</table>
			<div class="divider-section-small bg-bici"></div>
		</div>

		</div>

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	
	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>