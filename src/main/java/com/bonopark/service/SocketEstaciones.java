package com.bonopark.service;

import java.io.*;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class SocketEstaciones {
	public static String envioSocket(int puerto, String direccion,
			String mensaje){

			String servidor = direccion;
			try {
				Socket socket = new Socket(servidor, puerto);
				// conseguimos el canal de entrada
				BufferedReader entrada = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				// conseguimos el canal de salida
				PrintWriter salida = new PrintWriter(new OutputStreamWriter(
						socket.getOutputStream()), true);
				salida.println(mensaje);
				//salida.println("respuesta");
				// recibimos la respuesta del servidor
				System.out.println(entrada.readLine());
				socket.close();
			} catch (UnknownHostException e) {
				e.printStackTrace();
				System.out
						.println("Host " + servidor + "no encontrado");
				return e.toString();
			} catch (IOException e) {
				e.printStackTrace();
				return e.toString();
			}
			return "ok";
}


}
