<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD ::</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">
		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="seguridad" value="active" />
		</jsp:include>
		<!-- content-15b -->
		<div class="content-15b v-center bg-bici">

			<div class="container">
				<div class="row">
					<h1></h1>
					<h3>Consejos y Recomendaciones</h3>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>Al retirar una bicicleta compruebe su buen estado antes de
							comenzar a utilizarla, en especial vigile las ruedas, frenos,
							sillín y timbre. Si detecta alguna anomalía debe dar un aviso o
							incidencia a través de las pantallas táctiles del tótem (CIC).</p>
						<p>Desde la retirada hasta su correcta devolución la bicicleta
							es su responsabilidad, no la abandone y haga uso de ella y del
							servicio con la mayor diligencia posible.</p>
					</div>
				</div>


				<div class="col-xs-12">
					<div class="row">
						<h5>
							<strong>Circule con precaución y respetando las normas
								de tráfico en todo momento.</strong>
						</h5>

						<ul>
							<li>Está prohibido circular por la acera. Utilice siempre la
								calzada y preferentemente las vías ciclistas (carriles bici o
								ciclocarriles) siempre que sea posible.</li>
							<li>Respete los semáforos y las señales de tráfico.</li>
							<li>No transporte pasajeros, animales u objetos pesados.</li>
							<li>Adelante a otros vehículos solo si hay espacio
								suficiente para ello y es seguro.</li>
							<li>Circule manteniendo una distancia prudencial con el
								resto de vehículos.</li>
							<li>Circule por el centro del carril, no circule muy cerca
								de vehículos estacionados.</li>
							<li>No utilice la bicicleta bajo la influencia de drogas o
								alcohol.</li>
							<li>No utilice el teléfono móvil o auriculares mientras
								circula.</li>
							<li>Por la noche utilice ropa fluorescente o reflectante.</li>
							<li>Considere la posibilidad de utilizar casco. Es
								obligatorio para menores de 16 años.</li>
						</ul>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12">
						<p>En las siguientes situaciones deber dar un aviso o
							incidencia a través de cualquiera de estos canales: en el tótem
							de la estación, teléfono 900 50 54 63, sin cita previa en las
							Oficinas de Atención al Ciudadano Línea Madrid y aplicación para móvil BiciMAD:</p>
						<ul>
							<li>En caso de encontrar una bicicleta abandonada</li>
							<li>En caso de avería o cualquier otra incidencia del
								servicio. Además , el usuario tiene la obligación de anclarla en
								la estación más próxima. Si fuera imposible el traslado de la
								bicicleta a la estación más cercana, el usuario la custodiará
								hasta que la empresa encargada de la gestión del servicio
								proceda a recogerla tras recibir el aviso de la incidencia.</li>
							<li>En el supuesto de pérdida, robo o hurto de la bicicleta,
								el usuario denunciará los hechos, con la mayor brevedad posible,
								a las Fuerzas y Cuerpos de Seguridad, y comunicará la incidencia
								en un plazo máximo de 2 horas. Asimismo, entregará una copia de
								la denuncia a la empresa Bonopark S.L., con domicilio en la
								calle Serrano 85, 7º Izda, 28006 Madrid.</li>
						</ul>

						<p>
							<strong>Otra información de interés: </strong> <br /> <a
								target="_blank"
								href="http://www.madrid.es/portales/munimadrid/es/Inicio/Ayuntamiento/Movilidad-y-Transportes/Oficina-de-la-bici/Consejos-y-recomendaciones/Consejos-para-circular-en-bicicleta-por-la-ciudad?vgnextfmt=detNavegacion&vgnextoid=ca94292dc3f0b210VgnVCM2000000c205a0aRCRD&vgnextchannel=d74319927c278210VgnVCM2000000c205a0aRCRD">Consejos
								para circular en bicicleta por la ciudad</a><br /> <a
								href="http://www.madrid.es/portales/munimadrid/es/Inicio/Ayuntamiento/Movilidad-y-Transportes/Oficina-de-la-bici/Consejos-y-recomendaciones/Consejos-Practicos-para-el-uso-de-la-Bicicleta?vgnextfmt=detNavegacion&vgnextoid=f7be99648d476210VgnVCM2000000c205a0aRCRD&vgnextchannel=d74319927c278210VgnVCM2000000c205a0aRCRD">Consejos
								Prácticos para el uso de la Bicicleta</a><br /> <a
								href="http://www.madrid.es/portales/munimadrid/es/Inicio/Ayuntamiento/Movilidad-y-Transportes/Oficina-de-la-bici/Consejos-y-recomendaciones/En-bici-al-trabajo?vgnextfmt=detNavegacion&vgnextoid=066f5b4372f7a210VgnVCM2000000c205a0aRCRD&vgnextchannel=d74319927c278210VgnVCM2000000c205a0aRCRD">En
								bici al trabajo</a>
						</p>
					</div>
				</div>

			</div>
		</div>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>