<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%-- <html>
<head>
<title>DBIZI</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>
<body>
	<div id="main">
		<jsp:include page="/WEB-INF/common/header.jsp">
			<jsp:param name="queesbicimad" value="active" />
		</jsp:include>
		<div id="site_content">
			<div id="pagina">
				<div class="row clearfix">
					<jsp:include page="/WEB-INF/common/ultimasnoticias.jsp" />
					<section id="content">
					<h2>
						<fmt:message key="inicio.quees" />
					</h2>
					<img class="imagen_texto"
						src="/resources/images/bidegorri2.jpg"> <fmt:message
						key="inicio.contenido" /> </section>
				</div>
			</div>
		</div>

		<jsp:include page="/WEB-INF/common/footer.jsp" />
	</div>
	<script
		src="<%=request.getContextPath()%>/resources/js/language_slide.js"></script>
</body>
</html> --%>
<html lang="en">
    <head>
        <title>BiciMAD</title>
        <jsp:include page="/WEB-INF/common/head.jsp" />

    </head>

    <body>
        <div class="page-wrapper">
        <!-- header-1 -->
               <jsp:include page="/WEB-INF/common/header-1.jsp" />
               
        <section class="header-1-sub">
            <div id="pt-main" class="page-transitions pt-perspective">
                <div class="pt-page pt-page-1 bg-midnight-blue">
                    <div class="background">
                        &nbsp;
                    </div>
                  <div class="caption">
                        <div class="container">
                            <h3>Transporte público ecológico</h3>
                            <p class="lead">
                                Ofrecemos un medio de transporte económico y ecológico a los habitantes de Madrid, reduciendo el tráfico, mejorando la movilidad y el medio ambiente.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pt-page pt-page-2 bg-midnight-blue">
                    <div class="background">
                        &nbsp;
                    </div>
                  <div class="caption">
                        <div class="container">
                            <h3>Las Estaciones</h3>
                            <p class="lead">
                                Nuestras estaciones son zonas de aparcamiento y puntos de información. Realice consultas, verifique disponibilidad, reserve servicios y mucho más.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pt-page pt-page-3 bg-midnight-blue">
                    <div class="background">
                        &nbsp;
                    </div>
                  <div class="caption">
                        <div class="container">
                            <h3>Apps para iOS y Android</h3>
                            <p class="lead">
                                Acceda el servicio desde cualquier parte del mundo, consulte disponibilidad, realice pagos desde su dispositivo, haga reservaciones y mucho más.
                            </p>
                        </div>
                    </div>
                </div>
              <div class="pt-page pt-page-4 bg-midnight-blue">
                    <div class="background">
                        &nbsp;
                    </div>
                <div class="caption">
                        <div class="container">
                            <h3>Las Bicicletas</h3>
                            <p class="lead">
                                Bicicletas eléctricas 100% hechas en España que al pedalear el motor se activa, realizando gran parte del esfuerzo que debería hacer el ciclista.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="container controls">
                    <a class="control-prev" href="#"><span class="fui-arrow-left"> </span></a>
                    <a class="control-next" href="#"><span class="fui-arrow-right"> </span></a>
                </div>
            </div>
        </section>
        

        <!-- content-26 -->
      <section class="content-26 bg-peter-river">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
              <h3>&nbsp;</h3>
             
            </div>
          </div>
          <div class="row features">
            <div class="col-sm-3">
              <a href="servicios.html"><img src="/resources/img/Card@2x.png" alt="" width="120" height="120"></a>
              <h4>Tarifas</h4>
              <h7>Tenemos precios adaptados<br>a cada necesidad</h7>
            </div>
            <div class="col-sm-4 col-sm-offset-1">
              <a href="servicios.html"><img src="/resources/img/Watches@2x.png" alt="" width="120" height="120"></a>
              <h4>Horarios</h4>
              <h7>24 horas al día<br>
              365 días del año</h7>
            </div>
            <div class="col-sm-3 col-sm-offset-1">
              <img src="/resources/img/Map@2x.png" alt="" width="120" height="120">
              <h4>Estaciones</h4>
                <h7>Tenemos una estación cada 300 metros, escoje la más cercana</h7> 
            </div>
            <div class="col-sm-3">
              <img src="/resources/img/Bici@2x.png" alt="" width="120" height="120">
              <h4>Bicicletas</h4>
              <h7>La forma más rápida y ecológica<br>de moverse por la ciudad</h7>
            </div>
            <div class="col-sm-4 col-sm-offset-1">
              <img src="/resources/img/Club@2x.png" alt="" width="120" height="120">
              <h4>Club  Bike Boosters</h4>
            <h7>Forma parte de nuestro club<br>y aprovecha nuestras<br>promociones</h7></div>
            <div class="col-sm-3 col-sm-offset-1">
              <img src="/resources/img/Compas@2x.png" alt="" width="120" height="120">
              <h4>Centro de Control</h4>
                <h7>Entérate de todo lo que<br>pasa en tiempo real</h7></div>
          </div>
          
        </div>
      </section>
      <!--<div class="delimiter"></div>-->
      
      
      <!-- logos -->
            <section class="logos">
                <div class="container">
                    <div><img src="/resources/img/logos/madrid.png" alt="">
                    </div>
                    <div><img src="/resources/img/logos/bb.png" alt="">
                    </div>
                    <div><img src="/resources/img/logos/bonopark.png" alt="">
                    </div>
                </div><!--/.container-->
            </section>
            
                    
        <!-- footer-6 -->
        <jsp:include page="/WEB-INF/common/footer-6.jsp" />
        
        <!-- footer-2 -->
        <jsp:include page="/WEB-INF/common/footer-2.jsp" />

        <!-- Placed at the end of the document so the pages load faster -->
        <jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />


    </body>
</html>