package com.bonopark.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tutor")
public class Tutor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dni_tutor;
	private int tipo_documento;
	private String nombre;
	private String apellido1;
	private String apellido2;

	public String getDni_tutor() {
		return dni_tutor;
	}

	public void setDni_tutor(String dni_tutor) {
		this.dni_tutor = dni_tutor;
	}

	public int getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(int tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	@Override
	public String toString() {
		return "Tutor [dni=" + dni_tutor + ", tipo=" + tipo_documento + ", nombre=" + nombre
				+ "]";
	}
}