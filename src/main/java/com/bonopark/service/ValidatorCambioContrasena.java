package com.bonopark.service;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.bonopark.domain.CambioContrasena;
import com.bonopark.domain.Residente;


@Service("validatorCambioContrasena")
public class ValidatorCambioContrasena implements Validator {
	@Override
	public boolean supports(Class<?> clazz) {
		return CambioContrasena.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pwdVieja",
				"required.password", "Field name is required.");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pwdNueva",
				"required.password", "Field name is required.");
		CambioContrasena cambio = (CambioContrasena) target;
		Residente r = new Residente();
		try {
			String dni = cambio.getDni();
		} catch (Exception e) {
			// TODO: handle exception
		}
		try {
			if (!cambio.getPwdNueva().equalsIgnoreCase(cambio.getPwdRep())) {
				errors.rejectValue("pwdRep", "notmatch.password");
			}
		} catch (Error e) {
		}

	}
}
