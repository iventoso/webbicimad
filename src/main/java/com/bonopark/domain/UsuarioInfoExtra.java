package com.bonopark.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="usuario_informacion_extra") 
public class UsuarioInfoExtra implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dni;
	private int canal;
	private String numero_consorcio;
	
	@OneToOne
	@PrimaryKeyJoinColumn
	private Usuario usuario;
	
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public int getCanal() {
		return canal;
	}
	public void setCanal(int operador) {
		this.canal = operador;
	}
	
	public String getNumero_consorcio() {
		return numero_consorcio;
	}
	public void setNumero_consorcio(String numero_consorcio) {
		this.numero_consorcio = numero_consorcio;
	}
	@Override
	public String toString() {
		return "Usuario [dni=" + dni + ", operador=" + canal + ", numero consorcio=" + numero_consorcio + "]";
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public UsuarioInfoExtra() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UsuarioInfoExtra(String dni, int canal, String numero_consorcio,
			String idoperador) {
		super();
		this.dni = dni;
		this.canal = canal;
		this.numero_consorcio = numero_consorcio;


	}
	
	
	
	
	
}
