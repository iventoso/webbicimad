package com.bonopark.web;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.bonopark.domain.Residente;
import com.bonopark.domain.Tutor;
import com.bonopark.domain.Usuario;
import com.bonopark.domain.UsuarioInfoExtra;
import com.bonopark.service.ResidenteManager;
import com.bonopark.service.SimpleServicioCorreo;
import com.bonopark.service.SocketEstaciones;
import com.bonopark.service.ValidatorInscripcion;
import com.bonopark.service.ValidatorInscripcionTutor;

import crtm.bit.sayp.webservices.TarjetaBITWSProxy;

@Controller
@SessionAttributes
public class InscripcionController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// private String notificacionTPV =
	// "http://146.255.100.64:8080/bicimad/notificacionTPV.html";
	// private String notificacionTutorTPV =
	// "http://146.255.100.64:8080/bicimad/notificacionTutorTPV.html";
	private String notificacionTPV = "http://www.bicimad.com/notificacionTPV.html";
	private String notificacionTutorTPV = "http://www.bicimad.com/notificacionTutorTPV.html";

	@Autowired
	private ResidenteManager residenteManager;

	@InitBinder("residente")
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(new ValidatorInscripcion()); // registramos el
															// validador
	}

	@InitBinder("tutor")
	protected void initBinder1(WebDataBinder binder) {
		binder.setValidator(new ValidatorInscripcionTutor()); // registramos el
																// validador
	}

	@ModelAttribute("residente")
	public Residente populateForm() {
		return new Residente(); // creamos el bean para que se pueda popular
	}

	@ModelAttribute("tutor")
	public Tutor populateForm1() {
		return new Tutor(); // creamos el bean para que se pueda popular
	}

	@RequestMapping(value = "/inscribete.html")
	public ModelAndView inscribete(HttpServletRequest request,
			HttpServletResponse response) {
		HomeController.detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("residente", new Residente());
		// return new ModelAndView("inscripcion", "command", new Residente());
		return new ModelAndView("inscribete", "model", myModel);
//		myModel.put("mensaje", "error.incripciones");
//	myModel.put("titulo", "Alta desactivada");
//		return new ModelAndView("mensaje", "model", myModel);
	}

	@RequestMapping(value = "/inscribete_prueba.html")
	public ModelAndView inscribete_prueba(HttpServletRequest request,
			HttpServletResponse response) {
		HomeController.detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("residente", new Residente());
		// return new ModelAndView("inscripcion", "command", new Residente());
		return new ModelAndView("inscribete", "model", myModel);
//		myModel.put("mensaje", "error.incripciones");
//		myModel.put("titulo", "Alta desactivada");
//		return new ModelAndView("mensaje", "model", myModel);
	}
	
	@RequestMapping(value = "/validarinscripcion.html", method = RequestMethod.POST)
	public ModelAndView addContact(@Valid Residente residente,
			BindingResult result, HttpServletRequest request,
			HttpServletResponse response) {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HomeController.detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// Recogemos todos los datos y los metemos en myModel para pasarlos a
		// todas las vistas
		try {
			String fecha = new SimpleDateFormat("yyyy/MM/dd").format(residente
					.getFecha_nacimiento());
			myModel.put("fecha", fecha);
		} catch (Exception e) {
			System.out.println("Error en fecha + " + e);

		}
		myModel.put("usuario", UsuarioController.mostrarUsuarioInicio(request,
				response, residente));
		myModel.put("repetiremail", request.getParameter("repetiremail"));
		String numConsorcio = request.getParameter("consorcio");
		// empezamos con 25 euros como si no tuviera Abono transporte
		int importeAbono = 25;
		// Chequeamos si ha activado check de Tarjeta de Consorcio
		String checkConsorcio = request.getParameter("checkConsorcio");
		if ((checkConsorcio != null) && checkConsorcio.equals("on")) {
			// Ha activado casilla del Consorcio

			System.out.println("Numero del Consorcio : " + numConsorcio);
			try {

				TarjetaBITWSProxy p = new TarjetaBITWSProxy();

//				if ((p.informacionGeneralTarjeta1(numConsorcio)
//						.getITarjetaActiva() == null)
//						|| p.informacionGeneralTarjeta1(numConsorcio)
//								.getITarjetaActiva() != 1) {
				if (false) {
					myModel.put("errorconsorcio",
							"Número de tarjeta de consorcio incorrecto o utilizado previamente");
					myModel.put("consorcioCheck", "checked=\"checked\"");
					myModel.put("numConsorcio", numConsorcio);
		
	

					return new ModelAndView("inscribete", "model", myModel);
				} else {
					importeAbono = 15;
				}

//			}

//			catch (AxisFault e) {
//				// TODO Auto-generated catch block
//				System.out.println("Error Axis :");
//				e.printStackTrace();
//			} catch (RemoteException e) {
//				// TODO Auto-generated catch block
//				System.out.println("Error Remote Exception :");
//				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("Exception :");
				e.printStackTrace();
			}
		}

		// si hay errores volvemos a la vista del formulario
		if (result.hasErrors()) {
			myModel.put("usuario", UsuarioController.mostrarUsuarioInicio(
					request, response, residente));
			if ((checkConsorcio != null) && checkConsorcio.equals("on")) {
				myModel.put("consorcioCheck", "checked=\"checked\"");
				myModel.put("numConsorcio", numConsorcio);
			}
			return new ModelAndView("inscribete", "model", myModel);
		}

		// Comprobamos que ese Usuario no exista ya en la base de datos,
		// comprobando con el DNI
		if (residenteManager.comprobarUsuarioByDni(residente.getDni()) == false) {

			// si no hay errores, manejamos los datos validados...
			myModel.put("importeAbono", importeAbono);
			int saldoInicial = 0;
			int tipoDocumento = 1;
			try {
				tipoDocumento = Integer.parseInt(residente.getTipoDocumento());

			} catch (Exception e) {
				System.out
						.println("Error al recoger TipoDocumento en formulario : "
								+ e);
			}

			try {
				tipoDocumento = Integer.parseInt(residente.getTipoDocumento());
				saldoInicial = Integer.parseInt(request
						.getParameter("saldoinicial"));

			} catch (Exception e) {
				System.out.println("Saldo Inicial Erroneo: " + e);

			}
			myModel.put("saldoinicial", saldoInicial);
			myModel.put("importeTotal", saldoInicial + importeAbono);
			// Preparamos datos a pasar al TPV
			int importeTPV = (saldoInicial + importeAbono) * 100;
			java.text.SimpleDateFormat formador = new java.text.SimpleDateFormat(
					"YYMMddHHmmss");
			String merchantOrder = formador.format(new java.util.Date());
			String firmaTPV = calculoFirmaAlta(String.valueOf(importeTPV),
					merchantOrder, notificacionTPV);
			myModel.put("notificacionTPV", notificacionTPV);
			myModel.put("importeTPV", importeTPV);
			myModel.put("firmaTPV", firmaTPV);
			myModel.put("merchantOrder", merchantOrder);

			DateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			String datos = "";
			String clave = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
			datos = residente.getDni() + "//" + residente.getNombre() + "//"
					+ residente.getApellido1() + "//"
					+ residente.getApellido2() + "//es//"
					+ formatter.format(residente.getFecha_nacimiento()) + "//"
					+ residente.getMovil() + "//" + residente.getEmail() + "//"
					+ residente.getDireccion() + "//"
					+ residente.getMunicipio() + "//"
					+ residente.getProvincia() + "//"
					+ residente.getCodigo_postal() + "//" + numConsorcio + "//" + "4"
					+ "//" + "IdOpe" + "//" + tipoDocumento + "//" + clave
					+ "//" + saldoInicial;

			myModel.put("datos", datos);
			myModel.put("residente", residente);
			// Conprobamos si tiene menos de 16 a�os en tal caso habria que
			// rellenar datos tutor
			if (ValidatorInscripcion.calcularEdad(residente
					.getFecha_nacimiento()) < 16) {
				return new ModelAndView("inscribetetutor", "model", myModel);
			}
			return new ModelAndView("pago", "model", myModel);

		} else {
			myModel.put("mensaje", "confirmacion.usuariorepetido");
			return new ModelAndView("mensaje", "model", myModel);
		}

	}

	@RequestMapping(value = "/validartutor.html", method = {
			RequestMethod.POST, RequestMethod.GET })
	public ModelAndView addContact1(@Valid Tutor tutor, BindingResult result,
			HttpServletRequest request, HttpServletResponse response) {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HomeController.detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("tutor", tutor);
		String datos = request.getParameter("datos");
		String[] arrayDatos = datos.split("//");

		// Se meten en el model los campos necesarios, para que si existe un
		// error no se pierdan
		myModel.put("dni", arrayDatos[0]);
		myModel.put("nombre", arrayDatos[1]);
		myModel.put("apellido1", arrayDatos[2]);
		myModel.put("apellido2", arrayDatos[3]);
		myModel.put("movil", arrayDatos[6]);
		myModel.put("email", arrayDatos[7]);

		datos = datos + "//" + tutor.getDni_tutor() + "//"
				+ tutor.getTipo_documento() + "//" + tutor.getNombre() + "//"
				+ tutor.getApellido1() + "//" + tutor.getApellido2();
		myModel.put("importeTotal", request.getParameter("importeTotal"));
		myModel.put("datos", datos);

		myModel.put("importeTPV", request.getParameter("importeTPV"));
		myModel.put("saldoinicial", request.getParameter("saldoinicial"));
		myModel.put("importeTotal", request.getParameter("importeTotal"));
		myModel.put("importeAbono", request.getParameter("importeAbono"));
		// si hay errores volvemos a la vista del formulario
		if (result.hasErrors()) {

			return new ModelAndView("inscribetetutor", "model", myModel);
		}
		// Se comprueba si el tutor a metido el mismo dni que el usuario
		if (arrayDatos[0].equals(tutor.getDni_tutor())) {

			myModel.put("errordnitutor",
					"El número de identificación del tutor no puede ser igual que el del usuario");
			return new ModelAndView("inscribetetutor", "model", myModel);
		}

		// si no hay errores, manejamos los datos validados...

		java.text.SimpleDateFormat formador = new java.text.SimpleDateFormat(
				"YYMMddHHmmss");
		String merchantOrder = formador.format(new java.util.Date());
		String firmaTPV = calculoFirmaAlta(
				String.valueOf(request.getParameter("importeTPV")),
				merchantOrder, notificacionTutorTPV);
		myModel.put("notificacionTPV", notificacionTutorTPV);
		myModel.put("merchantOrder", merchantOrder);
		myModel.put("firmaTPV", firmaTPV);
		return new ModelAndView("pagomenores", "model", myModel);

	}

	@RequestMapping(value = "/pagook", method = RequestMethod.GET)
	public ModelAndView pagook(HttpServletRequest request,
			HttpServletResponse response) {
		HomeController.detectarIdioma(request, response);
		String num_pedido = request.getParameter("Ds_Order");
		String respuesta = request.getParameter("Ds_Response");
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mensaje", "pago.pagook");
		myModel.put("titulo", "Pago correcto");
		myModel.put("pedido", num_pedido);
		myModel.put("respuesta", respuesta);
		return new ModelAndView("mensaje", "model", myModel);
	}

	@RequestMapping(value = "/pagoko", method = RequestMethod.GET)
	public ModelAndView pagoko(HttpServletRequest request,
			HttpServletResponse response) {
		HomeController.detectarIdioma(request, response);
		String DS_Response = request.getParameter("Ds_Response");
		String mensaje2 = "pago.nada";

		// switch (DS_Response) {
		// case "0101":
		// mensaje2 = "pago.tarjetacaducada";
		// break;
		// case "0102":
		// mensaje2 = "pago.tarjetaerronea";
		// break;
		// case "0129":
		// mensaje2 = "pago.codsegincorrecto";
		// break;
		// case "0184":
		// mensaje2 = "pago.errorautenticacion";
		// break;
		// case "0191":
		// mensaje2 = "pago.fechaerronea";
		// break;
		// }
		Map<String, Object> myModel = new HashMap<String, Object>();
		// residenteManager.saveUsuarioWeb(residente);
		myModel.put("mensaje", "pago.pagoko");
		myModel.put("mensaje2", mensaje2);
		myModel.put("titulo", "Error en el pago");
		return new ModelAndView("mensaje", "model", myModel);
	}

	@RequestMapping(value = "/notificacionTPV", method = RequestMethod.POST)
	public void notificacionTPV(HttpServletRequest request,
			HttpServletResponse response) {

		try {
			response.setContentType("text/html;charset=UTF-8");
			request.setCharacterEncoding("UTF-8");

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HomeController.detectarIdioma(request, response);
		String respuesta = request.getParameter("Ds_Response");
		String datos = request.getParameter("Ds_MerchantData");
		System.out.println("Datos Recogidos : " + datos);
		String importe = request.getParameter("Ds_Amount");
		String[] arrayDatos = datos.split("//");
		String identificadorTPV = request
				.getParameter("Ds_Merchant_Identifier");
		String fechaCadTarjeta = request.getParameter("Ds_ExpiryDate");
		// Creamos primero el Usuario con el DNI, tipo, estado y Clave de
		// recogida

		Usuario usuario = new Usuario();
		usuario.setDni(arrayDatos[0]);
		usuario.setTipo(1);
		try {
			usuario.setTipo_documento(Integer.valueOf(arrayDatos[15]));
		} catch (NumberFormatException e) {
			usuario.setTipo_documento(1);
		} catch (Exception e) {
			usuario.setTipo_documento(1);
		}
		usuario.setEstado(2);
		// Si la clave la genero yo
		// Generamos clave recogida de 4 digitos
		// int clave = new Random().nextInt(8999)+1000; //siempre 4 digitos
		// usuario.setClave(String.valueOf(clave));

		// Si la clave me viene por en Merchant_Data
		usuario.setClave(arrayDatos[16]);

		String saldoInicial = arrayDatos[17];
		System.out.println("Saldo inicial :" + saldoInicial);
		try {
			usuario.setSaldo_inicial(Integer.valueOf(saldoInicial));
		} catch (NumberFormatException e) {
			usuario.setSaldo_inicial(0);
		} catch (Exception e) {
			usuario.setSaldo_inicial(0);
		}

		// usuario.getUsuarioInfoExtra().setDni(usuario.getDni());
		int origenAlta = 0;
		try {
			origenAlta = Integer.valueOf(arrayDatos[13]);
		} catch (NumberFormatException e) {

		}
		// Creamos objeto UsuarioInfoExtra para insertar luego en BBDD.
		// uaurioExtra(DNI, Operador, Consorcio, idOperador)
		UsuarioInfoExtra usuarioInfoExtra = new UsuarioInfoExtra(arrayDatos[0],
				origenAlta, arrayDatos[12], arrayDatos[14]);

		// Recogemos datos de la notificacion respecto al Residente y vamos
		// creando un objeto Residente
		// Para luego meter en la base de datos

		Residente residente = new Residente();
		residente.setDni(arrayDatos[0]);
		residente.setNombre(arrayDatos[1]);
		residente.setApellido1(arrayDatos[2]);
		residente.setApellido2(arrayDatos[3]);
		residente.setTipoDocumento("ES");
		try {
			// Tratamos la fecha que viene como String para convertirla en tipo
			// dato DATE
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			Date dateIN = formatter.parse(arrayDatos[5]);

			residente.setFecha_nacimiento(dateIN);
		} catch (ParseException e) {
			System.out.println("Excepcion en la fecha " + e);
		}
		residente.setMovil(arrayDatos[6]);
		residente.setEmail(arrayDatos[7]);
		residente.setDireccion(arrayDatos[8]);
		residente.setMunicipio(arrayDatos[9]);
		residente.setProvincia(arrayDatos[10]);
		residente.setCodigo_postal(arrayDatos[11]);
		// residente.getUsuario().getUsuarioInfoExtra().setNumero_consorcio("fd");
		// residente.getUsuario().getUsuarioInfoExtra().setOperador(origenAlta);
		residente.setNumCuenta(identificadorTPV);
		residente.setPassword("password");
		residente.setCaducidad_tarjeta(fechaCadTarjeta);
		// 2014/05/30 Si el usuario no tiene tutor
		residente.setTutor(false);

		if (Integer.parseInt(respuesta) < 100) {
			String nombreFactura = "";
			try {
				// Si el usuario tiene tarjeta de transporte la guardamos en
				// 'usuario_informacion_extra'
				// Guardamos datos de Usuario y Residente
				String e = residenteManager.saveUsuario(residente, usuario,
						usuarioInfoExtra, null);
				if (!e.equals("okok")) {
					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
					servicioCorreo.sendErrorBBDDPago(datos + "\t" + e);
				}

//				String consulta = "INSERT INTO usuario (dni, tipo, clave, estado, saldo_inicial, tipo_documento) VALUES ('"
//						+ usuario.getDni()
//						+ "', "
//						+ usuario.getTipo()
//						+ ", '"
//						+ usuario.getClave()
//						+ "', "
//						+ usuario.getEstado()
//						+ ", " + usuario.getSaldo_inicial()
//						+ ", " + usuario.getTipo_documento() + ")";
//				int bytesConsulta = consulta.getBytes().length;
//				String mensajeSocket = "1#3#web#" + bytesConsulta + "#"
//						+ consulta + "#0##0#CRCH#CRCL";
//				String rSocket = SocketEstaciones.envioSocket(3020, "192.168.10.1",
//						mensajeSocket);
//				//Si el resultado es diferente de ok ha habido un error
//				if (!rSocket.equals("ok")) {
//					System.out.println("Error Al mandar Socket");
//					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
//					servicioCorreo
//							.sendErrorSocket(consulta + "\n Error: " + rSocket);
//
//				}

				// Generamos Factura
//				String[] resultadoGenerarFactura = residenteManager
//						.generarFactura(residente, Integer.valueOf(importe));
//				e = resultadoGenerarFactura[0];
//				nombreFactura = resultadoGenerarFactura[1];
//
//				if (!e.equals("ok")) {
//					datos = datos
//							+ "\n El error se ha producido al generar la factura: "
//							+ e;
//					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
//					servicioCorreo.sendErrorBBDDFactura(datos + "\tLinea 264"
//							+ e);
//				}

			} catch (Exception e) {
				SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();

				servicioCorreo.sendErrorBBDDPago(datos + " Linea 280" + e);
			}

		}
	}

	@RequestMapping(value = "/notificacionTutorTPV", method = RequestMethod.POST)
	public void notificacionTutorTPV(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			response.setContentType("text/html;charset=UTF-8");
			request.setCharacterEncoding("UTF-8");

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HomeController.detectarIdioma(request, response);
		String respuesta = request.getParameter("Ds_Response");
		String datos = request.getParameter("Ds_MerchantData");
		String importe = request.getParameter("Ds_Amount");
		String[] arrayDatos = datos.split("//");
		String identificadorTPV = request
				.getParameter("Ds_Merchant_Identifier");
		String fechaCadTarjeta = request.getParameter("Ds_ExpiryDate");
		// Creamos primero el Usuario con el DNI, tipo, estado y Clave de
		// recogida

		Usuario usuario = new Usuario();
		usuario.setTipo(1);
		usuario.setEstado(2);
		usuario.setDni(arrayDatos[0]);
		try {
			usuario.setTipo_documento(Integer.valueOf(arrayDatos[15]));
		} catch (NumberFormatException e) {
			usuario.setTipo_documento(1);
		} catch (Exception e) {
			usuario.setTipo_documento(1);
		}
		// Si la clave la genero yo
		// Generamos clave recogida de 4 digitos
		// int clave = new Random().nextInt(8999)+1000; //siempre 4 digitos
		// usuario.setClave(String.valueOf(clave));

		// Si la clave me viene por en Merchant_Data
		usuario.setClave(arrayDatos[16]);

		String saldoInicial = arrayDatos[17];
		System.out.println("Saldo inicial :" + saldoInicial);
		try {
			usuario.setSaldo_inicial(Integer.valueOf(saldoInicial));
		} catch (NumberFormatException e) {
			usuario.setSaldo_inicial(0);
		} catch (Exception e) {
			usuario.setSaldo_inicial(0);
		}

		// usuario.getUsuarioInfoExtra().setDni(usuario.getDni());

		// Almacenamos los datos del tutor
		Tutor tutor = new Tutor();
		tutor.setDni_tutor(arrayDatos[18]);
		try {
			tutor.setTipo_documento(1);
			// tutor.setTipo_documento(Integer.valueOf(arrayDatos[19]));
		} catch (NumberFormatException e) {
			tutor.setTipo_documento(1);
		} catch (Exception e) {
			tutor.setTipo_documento(1);
		}
		tutor.setNombre(arrayDatos[20]);
		tutor.setApellido1(arrayDatos[21]);
		try {
			tutor.setApellido2(arrayDatos[22]);
		} catch (Exception e) {
			tutor.setApellido2("");
		}
		usuario.setDni_tutor(arrayDatos[18]);
		int origenAlta = 0;
		try {
			origenAlta = Integer.valueOf(arrayDatos[13]);
		} catch (NumberFormatException e) {

		}
		// Creamos objeto UsuarioInfoExtra para insertar luego en BBDD.
		// uaurioExtra(DNI, Operador, Consorcio, idOperador)
		UsuarioInfoExtra usuarioInfoExtra = new UsuarioInfoExtra(arrayDatos[0],
				origenAlta, arrayDatos[12], arrayDatos[14]);

		// Recogemos datos de la notificacion respecto al Residente y vamos
		// creando un objeto Residente
		// Para luego meter en la base de datos

		Residente residente = new Residente();
		residente.setDni(arrayDatos[0]);
		residente.setNombre(arrayDatos[1]);
		residente.setApellido1(arrayDatos[2]);
		residente.setApellido2(arrayDatos[3]);
		residente.setTipoDocumento("ES");
		// Tratamos la fecha que viene como String para convertirla en tipo dato
		// DATE
		try {
			// Tratamos la fecha que viene como String para convertirla en tipo
			// dato DATE
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			Date dateIN = formatter.parse(arrayDatos[5]);

			residente.setFecha_nacimiento(dateIN);
		} catch (ParseException e) {
			System.out.println("Excepcion en la fecha " + e);
		}
		residente.setMovil(arrayDatos[6]);
		residente.setEmail(arrayDatos[7]);
		residente.setDireccion(arrayDatos[8]);
		residente.setMunicipio(arrayDatos[9]);
		residente.setProvincia(arrayDatos[10]);
		residente.setCodigo_postal(arrayDatos[11]);
		// residente.getUsuario().getUsuarioInfoExtra().setNumero_consorcio("fd");
		// residente.getUsuario().getUsuarioInfoExtra().setOperador(origenAlta);
		residente.setNumCuenta(identificadorTPV);
		residente.setPassword("password");
		residente.setTutor(true);
		residente.setCaducidad_tarjeta(fechaCadTarjeta);

		if (Integer.parseInt(respuesta) < 100) {
			String nombreFactura = "";
			try {
				// Si el usuario tiene tarjeta de transporte la guardamos en
				// 'usuario_informacion_extra'
				// Guardamos datos de Usuario y Residente
				String e = residenteManager.saveUsuario(residente, usuario,
						usuarioInfoExtra, tutor);
				if (!e.equals("okok")) {
					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
					servicioCorreo.sendErrorBBDDPago(datos
							+ "\t Error en los dos correos enviados" + e);
				}
//				String consulta = "INSERT INTO usuario (dni, tipo, clave, estado, saldo_inicial, tipo_documento) VALUES ('"
//						+ usuario.getDni()
//						+ "', "
//						+ usuario.getTipo()
//						+ ", '"
//						+ usuario.getClave()
//						+ "', "
//						+ usuario.getEstado()
//						+ ", " + usuario.getSaldo_inicial()
//						+ ", " + usuario.getTipo_documento() + ")";
//				int bytesConsulta = consulta.getBytes().length;
//				String mensajeSocket = "1#3#web#" + bytesConsulta + "#"
//						+ consulta + "#0##0#CRCH#CRCL";
//				String rSocket = SocketEstaciones.envioSocket(3020, "192.168.10.1",
//						mensajeSocket);
//				if (!rSocket.equals("ok")) {
//					System.out.println("Error Al mandar Socket");
//					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
//					servicioCorreo
//							.sendErrorBBDDPago(consulta
//									+ "\n Error : "
//									+ rSocket);
//
//				}
				// Generamos Factura
				String[] resultadoGenerarFactura = residenteManager
						.generarFactura(residente, Integer.valueOf(importe));
				e = resultadoGenerarFactura[0];
				nombreFactura = resultadoGenerarFactura[1];

				if (!e.equals("ok")) {
					datos = datos
							+ "\n El error se ha producido al generar la factura: "
							+ e;
					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
					servicioCorreo.sendErrorBBDDFactura(datos + "\tLinea 264"
							+ e);
				}

			} catch (Exception e) {
				SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();

				servicioCorreo.sendErrorBBDDPago(datos + " Linea 280" + e);
			}
		}
	}

	@RequestMapping(value = "/recargaSaldoTPV", method = RequestMethod.POST)
	public void recargaSaldoTPV(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			request.setCharacterEncoding("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HomeController.detectarIdioma(request, response);
		System.out.println("RecargaSaldoTPV");
		 String respuesta = request.getParameter("Ds_Response");
//		String respuesta = "0";
		String datos = request.getParameter("Ds_MerchantData");
		System.out.println("Datos en URL :" + datos);
		String[] arrayDatos = datos.split("//");

		Residente residente = residenteManager.getUsuariobyDni(arrayDatos[0]);

		// A parte obtenemos el importe que se le ha cobrado que también está
		// incluido en Ds_Merchant_Data
		double importeAbono = 0;
		try {
			importeAbono = Double.parseDouble(request.getParameter("Ds_Amount")) / 100;
		} catch (NumberFormatException e) {
			System.out.println("El campo Merchant_Error no es numerico: "
					+ request.getParameter("Ds_Amount"));
			SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
			servicioCorreo.sendErrorBBDDPago("Datos: " + datos + "\t " + e
					+ " Importe recibido: "
					+ request.getParameter("Ds_Amount"));
			importeAbono = 0;
		}
		// Comprobamos que la transaccion ha sido correcta mirando que el
		// DS_REsponse es menor de 99
		System.out.println("Respuesta Recarga Saldo (Ds_Response) : " + respuesta + "Recarga Saldo : " + importeAbono);
		if (Integer.parseInt(respuesta) < 100) {
			try {
				System.out.println("Recarga Cobrada: " + residente + "-"
						+ importeAbono);
				String e = residenteManager.recargaUsuario(residente,
						importeAbono);
				if (!e.equals("ok")) {
					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
					servicioCorreo.sendErrorBBDDPago(datos + "\t" + e);
				}

			} catch (Exception e) {
				SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
				servicioCorreo.sendErrorBBDDPago("Error en recarga Saldo. \n DNI: " + datos + "\nImporte Recarga : " + importeAbono + "\nError : " + e);
			}
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "registro/confirmacion")
	public ModelAndView registroconfirmacionUsuario(
			@RequestParam(value = "t") String token) {
		Map<String, Object> myModel = new HashMap<String, Object>();
		if (residenteManager.confirmarUsuario(token) == false) {

			myModel.put("mensaje", "confirmacion.incorrecta");
		} else {

			myModel.put("mensaje", "confirmacion.correcta");

		}
		return new ModelAndView("mensaje", "model", myModel);

	}

	@RequestMapping(value = "/correosAltaTotem", method = RequestMethod.GET)
	public void correosAltaTotem(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			response.setContentType("text/html;charset=UTF-8");
			request.setCharacterEncoding("UTF-8");

		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String to = request.getParameter("email");
		String pwd = request.getParameter("pwd");
		String codId = request.getParameter("cod");
		String clave = request.getParameter("clave");
		String fechaCaducidad = request.getParameter("fecha");
		SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();

		String resultadoConf = servicioCorreo.sendConfirmacionSinClave(to,
				codId, fechaCaducidad);
		String resultadoContra = servicioCorreo.sendContrasena(to, pwd,
				new Residente());
		if (!resultadoConf.equals("ok") | !resultadoContra.equals("ok")) {
			servicioCorreo
					.sendError("Error al enviar los correos de alta desde totem. \nCorreo Contrasena: "
							+ resultadoContra
							+ "\nCorreo Confirmacion: "
							+ resultadoConf);
		}

	}

	public static String calculoFirmaAlta(String cantidad,
			String merchant_Order, String merchant_URL) {
		final int SHA1_DIGEST_LENGTH = 20;
		String Merchant_Signature = new String();

		String Merchant_Code = "285970679";
		String Merchant_Order = merchant_Order;
		// //Clave de pruebas
		// String Merchant_Password = "qwertyasdf0123456789";
		// //Clave real
		String Merchant_Password = "js55qx78sw35ug98lk35";
		String Merchant_Amount = cantidad;
		String Merchant_Currency = "978";
		String Merchant_TransactionType = "0";
		// //URL pruebas
		// String Merchant_MerchantURL =
		// "http://bonopark.no-ip.org:8080/bicimad/notificacionTPV.html";
		// //URL Produccion
		// String Merchant_MerchantURL =
		// "http://146.255.100.64:8080/bicimad/notificacionTPV.html";
		String Merchant_MerchantURL = merchant_URL;

		String Merchant_Identifier = "REQUIRED";

		byte bAmount[] = new byte[Merchant_Amount.length()];
		byte bOrder[] = new byte[Merchant_Order.length()];
		byte bCode[] = new byte[Merchant_Code.length()];
		byte bCurrency[] = new byte[Merchant_Currency.length()];
		byte bTransactionType[] = new byte[Merchant_TransactionType.length()];
		byte bMerchantURL[] = new byte[Merchant_MerchantURL.length()];
		byte bMerchantIdentifier[] = new byte[Merchant_Identifier.length()];
		byte bPassword[] = new byte[Merchant_Password.length()];

		bAmount = Merchant_Amount.getBytes();
		bOrder = Merchant_Order.getBytes();
		bCode = Merchant_Code.getBytes();
		bCurrency = Merchant_Currency.getBytes();
		bTransactionType = Merchant_TransactionType.getBytes();
		bMerchantURL = Merchant_MerchantURL.getBytes();
		bMerchantIdentifier = Merchant_Identifier.getBytes();
		bPassword = Merchant_Password.getBytes();

		MessageDigest sha;
		try {
			sha = MessageDigest.getInstance("SHA-1");
			sha.update(bAmount);
			sha.update(bOrder);
			sha.update(bCode);
			sha.update(bCurrency);
			sha.update(bTransactionType);
			sha.update(bMerchantURL);
			sha.update(bMerchantIdentifier);
			byte[] hash = sha.digest(bPassword);

			int h = 0;
			String s = new String();

			for (int i = 0; i < SHA1_DIGEST_LENGTH; i++) {
				h = (int) hash[i]; // Convertir de byte a int
				if (h < 0)
					h += 256; // Si son valores negativos, pueden haber
								// problemas de
								// conversi¢n.
				s = Integer.toHexString(h); // Devuelve el valor hexadecimal
											// como un
											// String
				if (s.length() < 2)
					Merchant_Signature = Merchant_Signature.concat("0"); // A€ade
																			// un
																			// 0
																			// si
																			// es
																			// necesario
				Merchant_Signature = Merchant_Signature.concat(s); // A€ade la
																	// conversi¢n
																	// a
																	// la cadena
																	// ya
																	// existente

			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Merchant_Signature;

	}
	
	@RequestMapping(value = "/notificacionOperacionesTPV", method = RequestMethod.GET)
	public void notificacionOperacionesTPV(HttpServletRequest request,
			HttpServletResponse response) {

//		try {
//			response.setContentType("text/html;charset=UTF-8");
//			request.setCharacterEncoding("UTF-8");
//
//		} catch (UnsupportedEncodingException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
		System.out.println("*******************************************************************************************************************************************");
		HomeController.detectarIdioma(request, response);
		String respuesta = "1"; //request.getParameter("Ds_Response");
		String datos = request.getParameter("Ds_MerchantData");
		System.out.println("Datos Recogidos : " + datos);
		String importe = request.getParameter("Ds_Amount");
		String[] arrayDatos = datos.split("//");
		String identificadorTPV = request
				.getParameter("Ds_Merchant_Identifier");
		String fechaCadTarjeta = request.getParameter("Ds_ExpiryDate");
		// Creamos primero el Usuario con el DNI, tipo, estado y Clave de
		// recogida

		Usuario usuario = new Usuario();
		usuario.setDni(arrayDatos[0]);
		usuario.setTipo(1);
		try {
			usuario.setTipo_documento(Integer.valueOf(arrayDatos[15]));
		} catch (NumberFormatException e) {
			usuario.setTipo_documento(1);
		} catch (Exception e) {
			usuario.setTipo_documento(1);
		}
		usuario.setEstado(2);
		// Si la clave la genero yo
		// Generamos clave recogida de 4 digitos
		// int clave = new Random().nextInt(8999)+1000; //siempre 4 digitos
		// usuario.setClave(String.valueOf(clave));

		// Si la clave me viene por en Merchant_Data
		usuario.setClave(arrayDatos[16]);

		String saldoInicial = arrayDatos[17];
		System.out.println("Saldo inicial :" + saldoInicial);
		try {
			usuario.setSaldo_inicial(Integer.valueOf(saldoInicial));
		} catch (NumberFormatException e) {
			usuario.setSaldo_inicial(0);
		} catch (Exception e) {
			usuario.setSaldo_inicial(0);
		}

		// usuario.getUsuarioInfoExtra().setDni(usuario.getDni());
		int origenAlta = 0;
		try {
			origenAlta = Integer.valueOf(arrayDatos[13]);
		} catch (NumberFormatException e) {

		}
		// Creamos objeto UsuarioInfoExtra para insertar luego en BBDD.
		// uaurioExtra(DNI, Operador, Consorcio, idOperador)
		UsuarioInfoExtra usuarioInfoExtra = new UsuarioInfoExtra(arrayDatos[0],
				origenAlta, arrayDatos[12], arrayDatos[14]);

		// Recogemos datos de la notificacion respecto al Residente y vamos
		// creando un objeto Residente
		// Para luego meter en la base de datos

		Residente residente = new Residente();
		residente.setDni(arrayDatos[0]);
		residente.setNombre(arrayDatos[1]);
		residente.setApellido1(arrayDatos[2]);
		residente.setApellido2(arrayDatos[3]);
		residente.setTipoDocumento("ES");
		try {
			// Tratamos la fecha que viene como String para convertirla en tipo
			// dato DATE
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			Date dateIN = formatter.parse(arrayDatos[5]);

			residente.setFecha_nacimiento(dateIN);
		} catch (ParseException e) {
			System.out.println("Excepcion en la fecha " + e);
		}
		residente.setMovil(arrayDatos[6]);
		residente.setEmail(arrayDatos[7]);
		residente.setDireccion(arrayDatos[8]);
		residente.setMunicipio(arrayDatos[9]);
		residente.setProvincia(arrayDatos[10]);
		residente.setCodigo_postal(arrayDatos[11]);
		// residente.getUsuario().getUsuarioInfoExtra().setNumero_consorcio("fd");
		// residente.getUsuario().getUsuarioInfoExtra().setOperador(origenAlta);
		residente.setNumCuenta(identificadorTPV);
		residente.setPassword("password");
		residente.setCaducidad_tarjeta(fechaCadTarjeta);
		// 2014/05/30 Si el usuario no tiene tutor
		residente.setTutor(false);
		System.out.println("Respuesta :" + respuesta);

		if (Integer.parseInt(respuesta) < 100) {
			String nombreFactura = "";
			try {
				// Si el usuario tiene tarjeta de transporte la guardamos en
				// 'usuario_informacion_extra'
				// Guardamos datos de Usuario y Residente
				String e = residenteManager.saveUsuario(residente, usuario,
						usuarioInfoExtra, null);
				if (!e.equals("okok")) {
					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
					servicioCorreo.sendErrorBBDDPago(datos + "\t" + e);
				}

//				String consulta = "INSERT INTO usuario (dni, tipo, clave, estado, saldo_inicial, tipo_documento) VALUES ('"
//						+ usuario.getDni()
//						+ "', "
//						+ usuario.getTipo()
//						+ ", '"
//						+ usuario.getClave()
//						+ "', "
//						+ usuario.getEstado()
//						+ ", " + usuario.getSaldo_inicial()
//						+ ", " + usuario.getTipo_documento() + ")";
//				int bytesConsulta = consulta.getBytes().length;
//				String mensajeSocket = "1#3#web#" + bytesConsulta + "#"
//						+ consulta + "#0##0#CRCH#CRCL";
//				String rSocket = SocketEstaciones.envioSocket(3020, "192.168.10.1",
//						mensajeSocket);
//				if (!rSocket.equals("ok")) {
//					System.out.println("Error Al mandar Socket");
//					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
//					servicioCorreo
//							.sendErrorBBDDPago(consulta
//									+ "\n Error : "
//									+ rSocket);
//
//				}

				// Generamos Factura
				String[] resultadoGenerarFactura = residenteManager
						.generarFactura(residente, Integer.valueOf(importe));
				e = resultadoGenerarFactura[0];
				nombreFactura = resultadoGenerarFactura[1];

				if (!e.equals("ok")) {
					datos = datos
							+ "\n El error se ha producido al generar la factura: "
							+ e;
					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
					servicioCorreo.sendErrorBBDDFactura(datos + "\tLinea 264"
							+ e);
				}

			} catch (Exception e) {
				SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();

				servicioCorreo.sendErrorBBDDPago(datos + " Linea 280" + e);
			}

		}
	}
	@RequestMapping(value = "/notificacionOperacionesTotemTPV", method = RequestMethod.GET)
	public void notificacionOperacionesTotemTPV(HttpServletRequest request,
			HttpServletResponse response) {
		
		System.out.println("*******************************************************************************************************************************************");
		HomeController.detectarIdioma(request, response);
		String respuesta = "1";//request.getParameter("Ds_Response");
		String datos = request.getParameter("Ds_MerchantData");
		System.out.println("Datos Recogidos : " + datos);
		String importe = request.getParameter("Ds_Amount");
		
		String[] arrayDatos = null;
		if (datos.contains("%2F%2F")) {
			arrayDatos = datos.split("%2F%2F");
		} else {
			arrayDatos = datos.split("//");
		}
		String identificadorTPV = request
				.getParameter("Ds_Merchant_Identifier");
		String fechaCadTarjeta = request.getParameter("Ds_ExpiryDate");
		// Creamos primero el Usuario con el DNI, tipo, estado y Clave de
		// recogida
		
		String dni = arrayDatos[0];
		String nombre = arrayDatos[1];
		String apellido1 = arrayDatos[2];
		String apellido2 = arrayDatos[3];
		String tipoDoc = arrayDatos[4];
		String duracion = arrayDatos[5];
		String tipoAbo = arrayDatos[6];
		String fechaNac = arrayDatos[7];
		String telefono = arrayDatos[8];
		String email = arrayDatos[9];
		String direccion = arrayDatos[10];
		String municipio = arrayDatos[11];
		String provincia = arrayDatos[12];
		String codigoPos = arrayDatos[13];
		String saldo = arrayDatos[14];
		String club = arrayDatos[15];
		String tutor = arrayDatos[16];
		String numTarjetaTransporte = new String();
		
		if (arrayDatos.length == 18) {
			numTarjetaTransporte = arrayDatos[17];
		} else {
			numTarjetaTransporte = "NO";
		} 
		
		Usuario usuario = new Usuario();
		usuario.setDni(dni);
		usuario.setTipo(1);
		try {
			usuario.setTipo_documento(Integer.valueOf(tipoDoc));
		} catch (NumberFormatException e) {
			usuario.setTipo_documento(1);
		} catch (Exception e) {
			usuario.setTipo_documento(1);
		}
		usuario.setEstado(2);
		
		String clave = RandomStringUtils.randomAlphanumeric(5).toLowerCase();
		usuario.setClave(clave);
		
		try {
			usuario.setSaldo_inicial(Integer.valueOf(saldo));
		} catch (NumberFormatException e) {
			usuario.setSaldo_inicial(0);
		} catch (Exception e) {
			usuario.setSaldo_inicial(0);
		}
		
		// usuario.getUsuarioInfoExtra().setDni(usuario.getDni());
		int origenAlta = 6;
		
		// Creamos objeto UsuarioInfoExtra para insertar luego en BBDD.
		// uaurioExtra(DNI, Operador, Consorcio, idOperador)
		numTarjetaTransporte = numTarjetaTransporte.replaceAll("\\s+","");
		UsuarioInfoExtra usuarioInfoExtra = new UsuarioInfoExtra(dni,
				origenAlta, numTarjetaTransporte, "0");
		
		// Recogemos datos de la notificacion respecto al Residente y vamos
		// creando un objeto Residente
		// Para luego meter en la base de datos
		
		Residente residente = new Residente();
		residente.setDni(dni);
		residente.setNombre(nombre);
		residente.setApellido1(apellido1);
		residente.setApellido2(apellido2);
		residente.setTipoDocumento("ES");
		try {
			// Tratamos la fecha que viene como String para convertirla en tipo
			// dato DATE
			Date dateIN = null;
			String fechaNacimiento = "19851212";
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd"); 
			dateIN = formatter.parse(fechaNacimiento);
			residente.setFecha_nacimiento(dateIN);
		} catch (ParseException e) {
			System.out.println("Excepcion en la fecha " + e);
		}
		residente.setMovil(telefono);
		residente.setEmail(email);
		residente.setDireccion(direccion);
		residente.setMunicipio(municipio);
		residente.setProvincia(provincia);
		residente.setCodigo_postal(codigoPos);
		residente.setNumCuenta(identificadorTPV);
		residente.setPassword("password");
		residente.setCaducidad_tarjeta(fechaCadTarjeta);
		residente.setTutor(false);
		// 2014/05/30 Si el usuario no tiene tutor
		residente.setTutor(false);
		System.out.println("Respuesta :" + respuesta);
		
		if (Integer.parseInt(respuesta) < 100) {
			String nombreFactura = "";
			try {
				// Si el usuario tiene tarjeta de transporte la guardamos en
				// 'usuario_informacion_extra'
				// Guardamos datos de Usuario y Residente
				String e = residenteManager.saveUsuario(residente, usuario,
						usuarioInfoExtra, null);
				if (!e.equals("okok")) {
					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
					servicioCorreo.sendErrorBBDDPago(datos + "\t" + e);
				}
				
//				String consulta = "INSERT INTO usuario (dni, tipo, clave, estado, saldo_inicial, tipo_documento) VALUES ('"
//						+ usuario.getDni()
//						+ "', "
//						+ usuario.getTipo()
//						+ ", '"
//						+ usuario.getClave()
//						+ "', "
//						+ usuario.getEstado()
//						+ ", " + usuario.getSaldo_inicial()
//						+ ", " + usuario.getTipo_documento() + ")";
//				int bytesConsulta = consulta.getBytes().length;
//				String mensajeSocket = "1#3#web#" + bytesConsulta + "#"
//						+ consulta + "#0##0#CRCH#CRCL";
//				String rSocket = SocketEstaciones.envioSocket(3020, "192.168.10.1",
//						mensajeSocket);
//				if (!rSocket.equals("ok")) {
//					System.out.println("Error Al mandar Socket");
//					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
//					servicioCorreo
//					.sendErrorBBDDPago(consulta
//							+ "\n Error : "
//							+ rSocket);
//					
//				}
				
				// Generamos Factura
				String[] resultadoGenerarFactura = residenteManager
						.generarFactura(residente, Integer.valueOf(importe));
				e = resultadoGenerarFactura[0];
				nombreFactura = resultadoGenerarFactura[1];
				
				if (!e.equals("ok")) {
					datos = datos
							+ "\n El error se ha producido al generar la factura: "
							+ e;
					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
					servicioCorreo.sendErrorBBDDFactura(datos + "\tLinea 264"
							+ e);
				}
				
			} catch (Exception e) {
				SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
				
				servicioCorreo.sendErrorBBDDPago(datos + " Linea 280" + e);
			}
			
		}
	}

}