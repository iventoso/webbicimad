<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="club" value="active" />
		</jsp:include>

		<div class="content-15b v-center bg-bici">

			<div class="container">
				<div class="row">
					<h1></h1>
					<h3>CLUB BiciMAD</h3>
					<h6>¿ Eres fan de BiciMAD? ¡Este es tu club! Próximamente
						pondremos en marcha el CLUB BiciMAD, un lugar de encuentro virtual
						donde todos los usuarios del sistema público de bici eléctrica de
						Madrid encontrarán información sobre formación, eventos, normativa
						sobre el mundo de la bici así como promociones y descuentos. Si
						eres usuario de BiciMAD, ya formas parte de nuestro club.</h6>
				</div>
			</div>
		</div>

		<!-- header-6 -->
		<div class="header-6-madrid">
			<div class="background"></div>
		</div>


		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>