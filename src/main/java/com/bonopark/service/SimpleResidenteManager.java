package com.bonopark.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.bonopark.domain.Factura;
import com.bonopark.domain.Residente;
import com.bonopark.domain.Tutor;
import com.bonopark.domain.Usuario;
import com.bonopark.domain.UsuarioInfoExtra;
import com.bonopark.repository.ResidenteDao;

@Component
public class SimpleResidenteManager implements ResidenteManager {
	private static final long serialVersionUID = 1L;

	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private ResidenteDao residenteDao;

	public void setResidenteDao(ResidenteDao residenteDao) {
		this.residenteDao = residenteDao;
	}

	@Override
	public List<Residente> getUsuarios() {
		return residenteDao.getUsuarioList();
	}



	@Override
	public boolean confirmarUsuario(String token) {
		Residente residente = residenteDao.getUsuarioByConfirmacion(token);
		if (residente == null) {
			return false;
		}
		residente.setActivo(true);
		residente.setConfirmacion(java.util.UUID.randomUUID().toString());
		residenteDao.updateUsuario(residente);
		return true;
	}

	@Override
	public boolean comprobarUsuarioByDni(String dni) {
		if (residenteDao.getUsuarioByDni(dni) == null)
			return false;
		return true;

	}

	@Override
	public Residente getUsuariobyDni(String dni) {
		return residenteDao.getUsuarioByDni(dni);
	}

	@Override
	public Residente findUsuarioByDniandEmail(String dni, String email) {
		Residente residente = getUsuariobyDni(dni);
		if ((residente == null) || !(residente.getEmail().equals(email))) {
			return null;
		}

		return residente;
	}

	@Override
	public boolean generarPwd(Residente residente) {
		String pwd = java.util.UUID.randomUUID().toString().substring(0, 7);
		residente.setPassword(passwordEncoder.encodePassword(pwd, null));
		SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
		// Comprobamos que el update de Usuario y el envio de email se hace
		// correctamente
		if ((residenteDao.updateUsuario(residente))
				&& (servicioCorreo.sendCambioPwd(residente.getEmail(), pwd,
						residente.getDni()))) {
			return true;
		}
		return false;

	}

	@Override
	public boolean updateUsuario(Residente residente) {
		return residenteDao.updateUsuario(residente);

	}

	@Override
	public boolean borrarUsuarioByDni(String DNI) {
		return residenteDao.deleteUsuarioByDni(DNI);
	}
	
	@Override
	public String[] generarFactura(Residente residente) {
		String nombreFactura;
		try {
			Map<String, Object> prueba = new HashMap<String, Object>();
			String anno = new SimpleDateFormat("yyyy").format(new Date());
			//Sacamos el numero de faturas del año actual y le sumamos 1. Si es 0 habra existido un error en la consulta
			int numFactura = residenteDao.numFactura(anno) + 1;
			System.out.println("Numero Factura :" + numFactura);
			if (numFactura == 0)
			{
				return new String[] {"Fallo al consultar numero de factura"};
				//return "Fallo al consultar numero de factura";
			}
			String codigoCiudad = "01";
			nombreFactura = anno + "-" + codigoCiudad + "-" + numFactura + "-" + residente.getDni().toUpperCase();
			prueba.put("nombreFactura", nombreFactura);
			//Generamos la factura en PDF y la almacenamos
			Pdf.crearPdf(prueba, residente);
			//Cramos un objeto factura con los datos de la factura para luego almacenar en la base de datos
			Factura factura = new Factura();
			factura.setId_factura(numFactura);
			factura.setNombreFactura(nombreFactura);
			factura.setImporte(45);
			factura.setResidente(residente);
			factura.setMedios_pago("tarjeta");
			//factura.setRfid(null);
			factura.setFecha(new Date());
			String e = residenteDao.saveDatosFactura(factura);
			if(e != null)
			{
				return new String[] {e};
			}
			
			
		} 
		catch (Exception e) 
		{
			return new String[] {e.toString()};
		}
		return new String[] {"ok", nombreFactura};
	}
	
	@Override
	public String recargaUsuario(Residente residente, double importe) {

		double resultado = residenteDao.updateAbono(residente, importe);
		
		if (resultado != -1)
		{
			SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
			servicioCorreo.sendRecargaSaldo(residente.getEmail(), importe, String.format("%.02f", resultado), residente);
			return "ok";
		}
		return null;

	}

	@Override
	public String saveUsuario(Residente residente, Usuario usuario, UsuarioInfoExtra usuarioInfoExtra, Tutor tutor) {
		residente.setFecha(new Date());
		residente.setActivo(true);
		residente.setAuthority("ROLE_USER");
		residente.setConfirmacion(java.util.UUID.randomUUID().toString());
		String pwd = java.util.UUID.randomUUID().toString().substring(0, 5).toLowerCase();
	
		residente.setPassword(passwordEncoder.encodePassword(pwd, null));
		String resultado = null;
		// Se debe incluir el número de tarjeta de transporte en el caso de que el usuario lo indique
		resultado = residenteDao.saveUsuario(residente, usuario, usuarioInfoExtra, tutor);
		try {
			if (resultado == null)
				{
					Residente nuevoResidente = getUsuariobyDni(residente.getDni());
					System.out.println("Residente recien insertado : " + nuevoResidente);
					System.out.println("Fecha: " + Calendar.getInstance().getTime());
					int codId = getUsuariobyDni(nuevoResidente.getDni()).getId();
					SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
					
					return servicioCorreo.sendConfirmacion(residente.getEmail(), pwd, residente, usuario, codId) + servicioCorreo.sendContrasena(residente.getEmail(), pwd, residente);
				}
			else
			{
				System.out.println("RESULTADO SAVE USUARIO: "+resultado);
				SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
				servicioCorreo.sendErrorBBDDPago(resultado + " Error en SimpleResidenteManager al hacer saveUsuario");
				return resultado;
			}

		} 
		catch (Exception e) {
//			SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
//			servicioCorreo.sendErrorBBDDPago(e.toString());
			System.out.println("Fecha: " + Calendar.getInstance().getTime());
			return "RESULTADO SAVE USUARIO: " + resultado + "/n Excepcion: " + e.toString();

		}
	}


	@Override
	public String[] generarFactura(Residente residente, int importeAbono) {
		String nombreFactura;
		try {
			Map<String, Object> prueba = new HashMap<String, Object>();
			String anno = new SimpleDateFormat("yyyy").format(new Date());
			//Sacamos el numero de faturas del aÃ±o actual y le sumamos 1. Si es 0 habra existido un error en la consulta
			int numFactura = residenteDao.numFactura(anno) + 1;
			System.out.println("Numero Factura :" + numFactura);
			if (numFactura == 0)
			{
				return new String[] {"Fallo al consultar numero de factura"};
				//return "Fallo al consultar numero de factura";
			}
			String codigoCiudad = "02";
			nombreFactura = anno + "-" + codigoCiudad + "-" + numFactura + "-" + residente.getDni().toUpperCase();
			prueba.put("nombreFactura", nombreFactura);
			//Generamos la factura en PDF y la almacenamos
			Pdf.crearPdf(prueba, residente);
			//Cramos un objeto factura con los datos de la factura para luego almacenar en la base de datos
			Factura factura = new Factura();
			factura.setId_factura(numFactura);
			factura.setNombreFactura(nombreFactura);
			factura.setImporte(importeAbono);
			
			// 23/04/2014 - BICIMAD
			// Se añade valor a 'importe_inicial'
			factura.setImporte_inicial(importeAbono);		
			factura.setResidente(residente);
			factura.setMedios_pago("tarjeta");
			//factura.setRfid(null);
			factura.setFecha(new Date());
			String e = residenteDao.saveDatosFactura(factura);
			if(e != null)
			{
				return new String[] {e};
			}
			
			
		} 
		catch (Exception e) 
		{
			return new String[] {e.toString()};
		}
		return new String[] {"ok", nombreFactura};
	}
	
	
}
