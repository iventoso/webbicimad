<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD :: Servicios</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp" />
		<div class="divider-section"></div>



		<!-- content-15 -->
		<section class="content-15 v-center bg-bici">
		<div>
			<div class="container">
				<div class="row">

					<div class="col-sm-4 col-sm-offset-1">
						<h3>Horarios</h3>
						<h6>El servicio de alquiler de bicicletas se prestará las 24
							horas al día los 365 días del año.</h6>
						<p>
							Puede revisar <a href="telefono.html">aqui</a> los horarios de
							Atencíon al Público.
						</p>

					</div>
					<div class="col-sm-7">
						<img src="/resources/img/horario.jpg" width="540" height="441" alt="">
					</div>
				</div>
			</div>
		</div>
		</section>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>