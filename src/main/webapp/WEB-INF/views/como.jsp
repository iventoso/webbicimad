<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="comofunciona" value="active" />
		</jsp:include>

		<!-- content-15 -->
		<div class="content-15 v-center bg-bici">
			<div>
				<div class="container">
					<div class="row">
						<h3>Cómo se utiliza</h3>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<h6>Retirar y devolver una bicicleta en BiciMAD es un
								proceso rápido y sencillo. Para poder utilizar el servicio de
								Bici MAD, es necesario registrarse previamente.</h6>
						</div>
					</div>
					<div class="row features">
						<div class="col-xs-6">
							<p>
								<strong>REGISTRO</strong>
							</p>
							<ul>
								<li>Como <strong>abonado anual</strong>, a través del tótem
									de la estación, en www.bicimad.com, en la aplicación móvil de
									Bicimad, en el teléfono 010 (915 298 210 si llama fuera de
									Madrid) o en las Oficinas de Atención al Ciudadano Línea
									Madrid. El usuario recibirá una clave de abonado con la que
									recoger su tarjeta en el tótem de la estación. A partir de este
									momento, el abonado simplemente tiene que acercarse a
									cualquiera de los anclajes de BiciMAD para disfrutar del servicio,
									siempre y cuando disponga de saldo en su tarjeta.
								</li>
								<li>Como <strong>usuario ocasional</strong>, a través del tótem de la
									estación, donde se le proporcionará una tarjeta de 1, 3 o 5
									días y se le facturará al final del período seleccionado en
									función de la utilización del servicio.</li>
								<li style="color: #a6555a">La venta de abono ocasional no está disponible actualmente.</li>
							</ul>
							<p>
								<strong>USO</strong>
							</p>
							<ul>
								<li>Para retirar una bicicleta, el anclaje debe tener luz
									verde (bicicleta disponible). Aproxime la tarjeta al lugar
									indicado hasta que escuche un pitido. Tire entonces suavemente
									de la bicicleta hasta liberarla.</li>
								<li>Para devolver una bicicleta, el anclaje debe tener luz
									roja (anclaje libre). Empuje dentro la bicicleta hasta que la luz
									pase a color verde y escuche un pitido de confirmación.
									Asegúrese que la bicicleta está bien anclada tirando suavemente
									de ella.</li>
								<li>Si el anclaje tiene color azul, es que ha sido reservada
									por otro usuario.</li>
								<li>Si no tiene la luz encendida, está desconectada y no
									podrá utilizarla.</li>
							</ul>
							<p>
								<strong>FORMAS DE PAGO</strong>
							</p>
							<p>Abono Anual</p>
							<p>
								El abonado anual tiene que cargar con saldo la tarjeta de abono
								anual para el cobro del tiempo de uso del servicio de alquiler.
								</br>Cada vez que el abonado hace un uso de la bicicleta, se
								disminuye el saldo por el importe correspondiente al tiempo de
								uso efectuado. </br>Las recargas de saldo serán por un importe mínimo
								de 10€. </br>En caso de saldo negativo en la tarjeta de abono anual,
								el abonado no puede utilizar el servicio de alquiler hasta que
								no recargue el saldo.
							</p>
							<p>Usuario Ocasional</p>
							<p style="color: #a6555a;">Actualmente no disponible.</p>
							<p>El usuario ocasional tiene que seleccionar en el tótem una
								tarjeta de un día, tres días o cinco días. Esta tarjeta,
								gratuita, le permite retirar la bicicleta las veces que quiera
								durante el periodo de validez de la misma, realizándose el cobro
								que corresponda por los tiempos de uso efectuados al final de
								dicho periodo de validez.</br> Como garantía del cobro, al expedir el
								tótem la tarjeta, queda bloqueado un importe de 150€ en la
								disposición de saldo de la tarjeta bancaria ("pre-autorización")
								que se desbloquea cuando se ha realizado el cargo en la tarjeta
								bancaria del importe que corresponde por los tiempos de uso
								efectuados.</p>
						</div>
						<div class="col-xs-6">
							<p>
								<img src="/resources/img/Labase_3.jpg" alt="base" />
							</p>
						</div>
					</div>
					<div class="divider-section-small bg-bici"></div>
					<h1></h1>

				</div>
			</div>
		</div>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>