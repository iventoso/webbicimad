<%-- <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> --%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD</title>

<%@ page import="java.security.*"%>
<jsp:include page="/WEB-INF/common/head.jsp" />

<script>
	function aMayusculas(obj, id)
	{
		obj = obj.toUpperCase();
		document.getElementById(id).value = obj;
	}
</script>

</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="inscribete" value="active" />
		</jsp:include>

		<!-- contacts-3 -->
		<div class="contacts-3">
			<div class="container">
				<div class="row">
					<div class="col-xs-10" style="margin-bottom: 40px;">
						<h3>INSCRIPCIÓN</h3>
						<h6>Tiene menos de 16 años, su tutor deberá rellenar este formulario de incripción para darse de alta
							en nuestros servicios online y formar parte de nuestro Club BiciMAD.</h6>
						
					</div>
					<form:form commandName="tutor" method="post"
						action="validartutor.html" name="form">
						<div class="col-xs-6">
							<div>
								<form:label path="tipo_documento">Tipo de Documento*</form:label>
								<form:select id="tipodocumento" name="tipodocumento"
									path="tipo_documento">
									<option value="1">DNI</option>
									<option value="2">Tarjeta Residente</option>
									<option value="3">Pasaporte</option>
								</form:select>
								<form:errors path="*" cssClass="errorblock" element="div" />
								<c:if test="${model.errordnitutor != null}">
									<div class="errorblock">${model.errordnitutor}</div>
								</c:if>
							</div>
							<form:label path="dni_tutor">Número de Documento*</form:label>
							<%-- <form:errors path="dni" cssClass="error" /> --%>
							<form:input path="dni_tutor" type="text"
								placeholder="Nº Documento" class="form-control"
								 />

							<form:label path="nombre">Nombre*</form:label>
							<%-- <form:errors path="nombre" cssClass="error" /> --%>
							<form:input path="nombre" type="text" placeholder="Nombre"
								class="form-control" 
								onblur="aMayusculas(this.value, this.id)"/>

							<form:label path="apellido1">Primer Apellido*</form:label>
							<form:input path="apellido1" type="text"
								placeholder="Primer Apellido" class="form-control"
								onblur="aMayusculas(this.value, this.id)"  />
							<%-- <form:errors path="apellido1" cssClass="error" /> --%>

							<form:label path="apellido2">Segundo Apellido</form:label>
							<form:input path="apellido2" type="text"
								placeholder="Segundo Apellido" class="form-control"
								onblur="aMayusculas(this.value, this.id)"  />
							<input title="comprar" type="submit" class="btn btn-primary"
								value="COMPRAR" name="submit" /> 
								<input type="hidden"
								name=Ds_Merchant_Order value="${model.merchantOrder}"></input> 
								<input
								type="hidden" name=datos value="${model.datos}"></input> 
								<input
								type="hidden" name=firmaTPV value="${model.firmaTPV}"></input> 
								<input
								type="hidden" name=importeTPV value="${model.importeTPV}"></input>
							<input type="hidden" name=importeAbono
								value="${model.importeAbono}"></input> 
								<input type="hidden"
								name=saldoinicial value="${model.saldoinicial}"></input> 
								<input
								type="hidden" name=importeTotal value="${model.importeTotal}"></input>
								<p>Los datos con * son obligatorios</p>
						</div>
						<!-- 							<button type="submit" class="btn btn-primary">COMPRAR</button> -->
					</form:form>
				</div>
			</div>
		</div>


		<%-- <div>
			<input title="He leido y aceptado las condiciones de uso"
				type="checkbox" id="checkcondiciones" value="check"
				name="checkcondiciones" /> He leido y aceptado las <a
				<fmt:message key="normativa.pdfnormativa" />><fmt:message
					key="normativa.txt3" /></a>
		</div> --%>
	</div>
	<!-- footer-6 -->
	<jsp:include page="/WEB-INF/common/footer-6.jsp" />

	<!-- footer-2 -->
	<jsp:include page="/WEB-INF/common/footer-2.jsp" />


	<!-- Placed at the end of the document so the pages load faster -->

	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
</body>
</html>