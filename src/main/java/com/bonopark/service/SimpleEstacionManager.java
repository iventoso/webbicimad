package com.bonopark.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bonopark.domain.Estacion;
import com.bonopark.repository.EstacionDao;

@Component
public class SimpleEstacionManager implements EstacionManager{
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private EstacionDao estacionDao;

	@Override
	public List<Estacion> getEstaciones() {
		return estacionDao.getEstacionList();
	}

}
