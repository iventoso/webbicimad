
<!-- header-1 -->
<div class="header-1">
	<div class="container">
		<div class="row">

			<div class="navbar col-xs-12" role="navigation">
				<div class="col-xs-5">
					<a class="logo2" href="index.html"><img
						src="/resources/img/logo_madrid.png" alt="logo_madrid" longdesc="index.html"  width="161"
						height="60" /></a>
				</div>

				<div class="col-xs-3">
					<a class="logo" href="index.html"><img
						src="/resources/img/logo-small@2x_b.png" alt="logo_madrid_peque�o" longdesc="index.html"  width="201"
						height="46" /></a>
				</div>

				<!-- 				<div class="col-xs-4">
					<ul class="nav navbar-nav pull-right">

						<li><a href="#">ESPA�OL</a></li>

						<li><a href="#">FRAN�AIS</a></li>

						<li><a href="#">ENGLISH</a></li>
					</ul>
				</div> -->

			<!-- 	<div class="col-xs-4">
					<ul class="nav navbar-nav pull-right">


						<li><a href="#">
								<div class="fui-facebook"></div>
						</a></li>

						<li><a href="#">
								<div class="fui-twitter"></div>
						</a></li>
					</ul>
				</div> -->

			</div>

		</div>
	</div>
</div>
<!-- header-1 -->

<!-- header-6 -->
<div class="header-6">
	<div class="container">
		<div class="row">
			<div class="navbar col-xs-12" role="navigation">

				<div>

					<ul class="nav navbar-nav">

						<li class="dropdown"><a href="#" data-toggle="dropdown"
							class="dropdown-toggle ${param.comofunciona}">C�MO FUNCIONA</a>
							<ul class="dropdown-menu">
								<li><a href="/que.html">Qu� es</a></li>
								<li class="divider"></li>
								<li><a href="/como.html">C�mo se utiliza</a></li>
								<li class="divider"></li>
								<li><a href="/bicicleta.html">La Bicicleta</a></li>
								<li class="divider"></li>
								<li><a href="/estacion.html">La Estaci�n</a></li>
								<li class="divider"></li>
								<li><a href="/preguntas.html">Preguntas Frecuentes</a></li>
								<li class="divider"></li>
								<li><a href="/app.html">Descarga la App</a></li>
							</ul></li>

						<li class="dropdown"><a href="mapa.html"
							class="dropdown-toggle ${param.plano}">PLANO & ESTACIONES</a></li>

						<li class="dropdown"><a href="#" data-toggle="dropdown"
							class="dropdown-toggle ${param.abonostarifas}">ABONOS & TARIFAS</a>
							<ul class="dropdown-menu">
								<li><a href="/abonos.html">Abonos</a></li>
								<li class="divider"></li>
								<li><a href="/tarifas.html">Tarifas</a></li>
								<li class="divider"></li>
								<!-- <li><a href="cargos.html">Otros Cargos</a></li> -->
							</ul></li>

						<li class="dropdown"><a href="#" data-toggle="dropdown"
							class="dropdown-toggle ${param.seguridad}">SEGURIDAD</a>
							<ul class="dropdown-menu">
								<li><a href="/seguro.html">Seguro</a></li>
								<li class="divider"></li>
								<li><a href="/normativa.html">Normativa</a></li>
								<li class="divider"></li>
								<li><a href="/conducta.html">Consejos y recomendaciones</a></li>
							</ul></li>
						<li class="dropdown"><a href="/inscribete.html"
							class="dropdown-toggle ${param.inscribete}">INSCR�BETE</a></li>
						<li class="dropdown"><a href="/login"
							class="dropdown-toggle ${param.usuarios}">�REA USUARIO</a></li>
						<li class="dropdown"><a href="/contacto.html"
							class="dropdown-toggle ${param.contacto}">CONTACTO</a></li>
						<li class="dropdown"><a href="/club.html"
							class="dropdown-toggle ${param.club}">CLUB BiciMAD</a></li>

					</ul>



				</div>
			</div>
		</div>
	</div>
</div>
<div class="header-6-sub2">
	<div class="row desplegable">
    	<label for="select"></label>
    	<select onchange="select_link();" id="select" class="select-option"> 
            <option value="" selected="selected">Como funciona</option> 
            <option value="/que.html"> - Que es</option> 
            <option value="/como.html"> - C�mo se utiliza</option> 
            <option value="/bicicleta.html"> - La bicicleta</option> 
            <option value="/estacion.html"> - La estaci�n</option> 
            <option value="/preguntas.html"> - Preguntas frecuentes</option>
            <option value="/mapa.html" >Plano & Estaciones</option>
            <option value="" >Abonos & Tarifas</option>
            <option value="/abonos.html" > - Abonos</option>
            <option value="/tarifas.html" > - Tarifas</option>
            <option value="" >Seguridad</option>
            <option value="/seguro.html" > - Seguro</option>
            <option value="/normativa.html" > - Normativa</option>
            <option value="/conducta.html" > - Consejos y recomendaciones</option>
            <option value="/inscribete.html" >Inscr�bete</option>
            <option value="/login" >�rea usuario</option>
            <option value="/contacto.html" >Contacto</option>
            <option value="/club.html" >Club BB</option>
    	</select> 
    </div>
	<div class="background2"></div>

</div>

<script>
	function select_link() {
		var myselect = document.getElementById("select");
  		window.location = myselect.options[myselect.selectedIndex].value;
	}
</script>