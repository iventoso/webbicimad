<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD :: Servicios</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="seguridad" value="active" />
		</jsp:include>

		<!-- content-15 -->
		<div class="content-15b v-center">
		<div>
			<div class="container">
				<div class="row">
					<h3>Seguros</h3>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h6>Todos los usuarios de BiciMAD están asegurados durante la utilización del servicio.
							Puede consultar aquí las condiciones del seguro.</h6>

					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 link-big">
						<a
							href="/resources/docs/Seguro de Accidentes Personales.pdf" />Condiciones
						del seguro</a>
					</div>
				</div>

			</div>
		</div>

		<h1></h1>
		<div class="divider-section"></div>

		<div class="divider-section-big"></div>

		</div>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>