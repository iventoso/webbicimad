<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD :: Componentes</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp" />
		<div class="divider-section"></div>



		<!-- content-15 -->
		<section class="content-15b v-center">
		<div>
			<div class="container">
				<div class="row">
					<h5>COMPONENTES:</h5>
					<h3>La Base</h3>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<h6>Alquilar y devolver una bicicleta en nuestra red es un
							proceso muy rápido y sencillo, siga estos pasos que no le
							llevarán más de un minuto. Para más información consulte las
							pantallas interactivas localizadas en nuestras Estaciones.</h6>
					</div>
				</div>
				<div class="row features">
					<div class="col-sm-6">
						<img src="/resources/img/Labase_3.jpg" width="500" height="453" alt="">
					</div>
					<div class="col-sm-6">
						<h6>Como retirar una bicicleta</h6>
						<p>En una base con una bicicleta disponible (luz verde),
							aproxime la tarjeta al lugar indicado en el dibujo, y cuando
							escuche el pitido y la luz comience a parpadear tire suavemente
							de la bicicleta hasta liberarla.</p>
						<h6>Como devolver una bicicleta</h6>
						<p>En una base libre (luz roja) empuje dentro la bicicleta
							hasta que la luz pase a color verde y escuche un pitido de
							confirmación. Asegúrese de que la bicicleta está bien anclada
							tirando suavemente de ella.</p>


					</div>
				</div>
			</div>
		</div>
		</section>


		<!-- content-15b -->
		<section class="content-15 v-center bg-bici">
		<div>
			<div class="container">
				<div class="row">
					<h3>La Bicicleta</h3>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<h6>La bicicleta eléctrica es una bicicleta convencional a la
							que se añade un pequeño motor eléctrico que ayuda al usuario en
							el pedaleo. Al pedalear el motor se activa, realizando gran parte
							del esfuerzo que debería hacer el ciclista, y se detiene cuando
							este deja de pedalear.</h6>
					</div>
				</div>
				<div class="row features">
					<div class="col-sm-7">
						<img src="/resources/img/Labici.jpg" width="540" height="441" alt="">
					</div>
					<div class="col-sm-4 col-sm-offset-1">
						<p>
							<strong>· Para encender y apagar</strong> la bicicleta eléctrica
							debe accionar el botón rojo (ON/OFF) que se encuentra en el
							manillar).
						</p>
						<p>
							<strong>· Una vez encendida,</strong> con el botón azul (LIGHT)
							puede encender y apagar las luces.
						</p>
						<p>
							<strong>· Pulsando el botón verde (MODE)</strong> regulará el
							nivel de asistencia eléctrica. Puede ver la asistencia
							seleccionada con las luces que se encuentran a la derecha del
							botón. Las posiciones de la luz en LOW, MED y HIGH significan
							respectivamente que la ayuda seleccionada es baja, media o alta.
							Con todas las luces apagadas no hay asistencia eléctrica.
						</p>
						<p>
							<strong>· Las luces en la parte derecha del mando de
								control</strong> indican el nivel de batería de la bicicleta en cada
							momento. Durante el uso se apagarán progresivamente, de izquierda
							a derecha conforme se consuma la batería de la bicicleta.
						</p>


					</div>
				</div>
			</div>
		</div>
		</section>

		<!-- content-15 -->
		<section class="content-15b v-center bg-bici">
		<div>
			<div class="container">
				<div class="row">
					<h3>La Estación</h3>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<h6>Además de las bases y las bicicletas, las estaciones
							están formadas por puntos de información y pantallas
							interactivas. Gracias a estos elementos, el usuario puede
							realizar diferentes consultas sin necesidad de disponer de un
							ordenador o de tener que acudir a la oficina.</h6>
					</div>
				</div>
				<div class="row features">
					<div class="col-sm-7">
						<img src="/resources/img/Estacion.jpg" width="540" height="441" alt="">
					</div>
					<div class="col-sm-4 col-sm-offset-1">
						<p>Puede consultar la disponibilidad de aparcamientos y
							bicicletas en tiempo real, realizar reservas, consultar
							promociones y mucho más. Tan solo debe acercarse y navegar a
							través de los diferentes menús. Además, si lo desea, en el punto
							de información puede obtener abonos de corta duración e imprimir
							tickets.</p>


					</div>
				</div>
			</div>
		</div>
		</section>


		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>