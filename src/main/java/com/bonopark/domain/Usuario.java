package com.bonopark.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	private String dni;
	private int tipo;
	private int estado;
	private String clave;
	private double saldo_inicial;
	private int tipo_documento;

	@OneToOne
	@PrimaryKeyJoinColumn
	private Residente residente;

	@OneToOne(mappedBy = "usuario")
	private UsuarioInfoExtra usuarioInfoExtra;

	private String dni_tutor;
	
//	@ManyToOne
//	@JoinColumn(name = "dni_tutor")
//	private Tutor tutor;

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Residente getResidente() {
		return residente;
	}

	public void setResidente(Residente residente) {
		this.residente = residente;
	}

	@Override
	public String toString() {
		return "Usuario [dni=" + dni + ", tipo=" + tipo + ", estado=" + estado
				+ "]";
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public UsuarioInfoExtra getUsuarioInfoExtra() {
		return usuarioInfoExtra;
	}

	public void setUsuarioInfoExtra(UsuarioInfoExtra usuarioInfoExtra) {
		this.usuarioInfoExtra = usuarioInfoExtra;
	}

	public double getSaldo_inicial() {
		return saldo_inicial;
	}

	public void setSaldo_inicial(double saldo_inicial) {
		this.saldo_inicial = saldo_inicial;
	}

//	public Tutor getTutor() {
//		return tutor;
//	}
//
//	public void setTutor(Tutor tutor) {
//		this.tutor = tutor;
//	}

	public int getTipo_documento() {
		return tipo_documento;
	}

	public String getDni_tutor() {
		return dni_tutor;
	}

	public void setDni_tutor(String dni_tutor) {
		this.dni_tutor = dni_tutor;
	}

	public void setTipo_documento(int tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

}