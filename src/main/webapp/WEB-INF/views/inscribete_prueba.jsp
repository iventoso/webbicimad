<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%-- <%@ page contentType="text/html; charset=ISO-8859-1" --%>
<!-- 	pageEncoding="UTF-8"%> -->
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD</title>

<%@ page import="java.security.*"%>
<jsp:include page="/WEB-INF/common/head.jsp" />

<style>
.ui-datepicker {
	width: 216px;
	height: auto;
	margin: 5px auto 0;
	font: 9pt Arial, sans-serif;
	-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	-moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
}

.ui-datepicker a {
	text-decoration: none;
}

.ui-datepicker table {
	width: 100%;
}

.ui-datepicker-header {
	background: #066eb6;
	color: #e0e0e0;
	font-weight: bold;
	-webkit-box-shadow: inset 0px 1px 1px 0px rgba(250, 250, 250, 2);
	-moz-box-shadow: inset 0px 1px 1px 0px rgba(250, 250, 250, .2);
	box-shadow: inset 0px 1px 1px 0px rgba(250, 250, 250, .2);
	text-shadow: 1px -1px 0px #000;
	filter: dropshadow(color =                     #000, offx =                    
		1, offy =    
		          
		     -1);
	line-height: 30px;
	border-width: 1px 0 0 0;
	border-style: solid;
	border-color: #0E68C8;
}

.ui-datepicker-title {
	text-align: center;
}

.ui-datepicker-prev,.ui-datepicker-next {
	display: inline-block;
	width: 30px;
	height: 30px;
	text-align: center;
	cursor: pointer;
	line-height: 600%;
	overflow: hidden;
}

.ui-datepicker-prev {
	float: left;
	background: url('resources/img/arrow.png') no-repeat center -30px;
}

.ui-datepicker-next {
	float: right;
	background: url('resources/img/arrow.png') no-repeat center 0px;
}

.ui-datepicker thead {
	background-color: #f7f7f7;
	background-image: -moz-linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #f7f7f7),
		color-stop(100%, #f1f1f1));
	background-image: -webkit-linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	background-image: -o-linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	background-image: -ms-linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	background-image: linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(            startColorstr='#f7f7f7',
		endColorstr='#f1f1f1', GradientType=0);
	border-bottom: 1px solid #bbb;
}

.ui-datepicker th {
	text-transform: uppercase;
	font-size: 6pt;
	padding: 5px 0;
	color: #666666;
	text-shadow: 1px 0px 0px #fff;
	filter: dropshadow(color =                     #fff, offx =                    
		1, offy =    
		          
		     0);
}

.ui-datepicker tbody td {
	padding: 0;
	border-right: 1px solid #bbb;
	background: #ecf0f1;
}

.ui-datepicker tbody td:last-child {
	border-right: 0px;
}

.ui-datepicker tbody tr {
	border-bottom: 1px solid #bbb;
}

.ui-datepicker tbody tr:last-child {
	border-bottom: 0px;
}

.ui-datepicker td span,.ui-datepicker td a {
	display: inline-block;
	font-weight: bold;
	text-align: center;
	width: 30px;
	height: 30px;
	line-height: 30px;
	color: #666666;
	text-shadow: 1px 1px 0px #fff;
	filter: dropshadow(color =                     #fff, offx =                    
		1, offy =    
		          
		     1);
}

.ui-datepicker-calendar .ui-state-default {
	background: #ededed;
	background: -moz-linear-gradient(top, #ededed 0%, #dedede 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ededed),
		color-stop(100%, #dedede));
	background: -webkit-linear-gradient(top, #ededed 0%, #dedede 100%);
	background: -o-linear-gradient(top, #ededed 0%, #dedede 100%);
	background: -ms-linear-gradient(top, #ededed 0%, #dedede 100%);
	background: linear-gradient(top, #ededed 0%, #dedede 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(            startColorstr='#ededed',
		endColorstr='#dedede', GradientType=0);
	-webkit-box-shadow: inset 1px 1px 0px 0px rgba(250, 250, 250, .5);
	-moz-box-shadow: inset 1px 1px 0px 0px rgba(250, 250, 250, .5);
	box-shadow: inset 1px 1px 0px 0px rgba(250, 250, 250, .5);
}

.ui-datepicker-unselectable .ui-state-default {
	background: #f4f4f4;
	color: #b4b3b3;
}

.ui-datepicker-calendar .ui-state-hover {
	background: #f7f7f7;
}

.ui-datepicker-calendar .ui-state-active {
	background: #6eafbf;
	-webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
	-moz-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
	box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
	color: #e0e0e0;
	text-shadow: 0px 1px 0px #4d7a85;
	filter: dropshadow(color =                     #4d7a85, offx =
		                    0, offy =   
		 
		               1);
	border: 1px solid #55838f;
	position: relative;
	margin: -1px;
}

.ui-datepicker-calendar td:first-child .ui-state-active {
	width: 29px;
	margin-left: 0;
}

.ui-datepicker-calendar td:last-child .ui-state-active {
	width: 29px;
	margin-right: 0;
}

.ui-datepicker-calendar tr:last-child .ui-state-active {
	height: 29px;
	margin-bottom: 0;
}

.ui-datepicker-month,.ui-datepicker-year {
	color: #000;
}
</style>
<script>
	function validar() {
		if (validarFormatoFecha()) {
			return emailrepetido(this.form.email.value,
					this.form.repetiremail.value);
		}
		return false;

	}
	function validarFormatoFecha() {
		var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
		var campo = this.form.fecha_nacimiento.value;

		if ((campo.match(RegExPattern)) && (campo != '')) {
			return existeFecha(campo);
		} else {
			alert("Formato de fecha incorrecto");
			return false;
		}
	}
	function existeFecha(fecha) {
		var fechaf = fecha.split("/");
		var day = fechaf[0];
		var month = fechaf[1];
		var year = fechaf[2];
		var date = new Date(year, month, '0');
		if ((day - 0) > (date.getDate() - 0)) {
			alert("Fecha incorrecta");
			return false;
		}
		return true;
	}
	function aMayusculas(obj, id) {
		obj = obj.toUpperCase();
		document.getElementById(id).value = obj;
	}
	function emailrepetido(email, repetiremail) {
		if (email == repetiremail) {
			return true;

		} else {
			alert("Los correos no coinciden");
			return false;
		}
	}
</script>
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="inscribete" value="active" />
		</jsp:include>

		<!-- contacts-3 -->
		<div class="contacts-3">
			<div class="container">
				<div class="row">
					<div class="col-xs-10" style="margin-bottom: 40px;">
						<h1></h1>
						<h3>INSCRIPCIÓN</h3>
						<h6>Rellene este formulario de inscripción para darse de alta como abonado anual. 
						Al realizar el alta, pasa además a formar parte de nuestro Club BiciMAD</h6>
					</div>
					<form:form commandName="residente" method="post"
						action="validarinscripcion.html" name="form"
						onsubmit="return validar()">
						<div class="col-xs-6">
							<div>
								<form:label path="tipoDocumento">Tipo de Documento*</form:label>
								<form:select id="tipodocumento" name="tipodocumento"
									path="tipoDocumento">
									<option value="1">DNI</option>
									<option value="2">Tarjeta Residente</option>
									<option value="3">Pasaporte</option>
								</form:select>
								<form:errors path="*" cssClass="errorblock" element="div" />

								<c:if test="${model.errorconsorcio != null}">
									<div class="errorblock">${model.errorconsorcio}</div>
								</c:if>
							</div>
							<form:label path="dni">Número de Documento*</form:label>
							<%-- <form:errors path="dni" cssClass="error" /> --%>
							<form:input path="dni" type="text" placeholder="Nº Documento"
								class="form-control" onblur="aMayusculas(this.value, this.id)" />

							<form:label path="nombre">Nombre*</form:label>
							<%-- <form:errors path="nombre" cssClass="error" /> --%>
							<form:input path="nombre" type="text" placeholder="Nombre"
								class="form-control" onblur="aMayusculas(this.value, this.id)" />

							<form:label path="apellido1">Primer Apellido*</form:label>
							<form:input path="apellido1" type="text"
								placeholder="Primer Apellido" class="form-control"
								onblur="aMayusculas(this.value, this.id)" />
							<%-- <form:errors path="apellido1" cssClass="error" /> --%>

							<form:label path="apellido2">Segundo Apellido</form:label>
							<form:input path="apellido2" type="text"
								placeholder="Segundo Apellido" class="form-control"
								onblur="aMayusculas(this.value, this.id)" />

							<form:label path="fecha_nacimiento">
								<fmt:message key="inscripcion.nac" /> (dd/MM/aaaa)*: </form:label>
							<fmt:formatDate value='${model.usuario.fecha_nacimiento}'
								type="date" pattern="dd/MM/yyyy" var="f" />
							<form:input path="fecha_nacimiento" type="text"
								class="form-control" id="fecha_nacimiento"
								name="fecha_nacimiento" placeholder="Fecha de Nacimiento"
								value="${f}" />
							<%-- <form:errors path="fecha_nacimiento" cssClass="error" /> --%>

							<form:label path="movil">Móvil*</form:label>
							<form:input path="movil" type="text" title="Introduzca 9 cifras"
								maxlength="9" placeholder="Móvil" class="form-control" />
						</div>

						<div class="col-xs-6 form-colm-2">

							<form:label path="email">Email*</form:label>
							<form:input id="email" name="email" path="email" type="text"
								placeholder="Email" class="form-control" />
							<%-- <form:errors path="email" cssClass="error" /> --%>
							<label>Repetir Email*</label> <input name="repetiremail"
								type="text" id="repetiremail" placeholder="Repetir Email"
								class="form-control" value='${model.repetiremail}'>
							<form:label path="direccion">Dirección*</form:label>
							<form:input path="direccion" type="text" placeholder="Dirección"
								class="form-control" onblur="aMayusculas(this.value, this.id)" />
							<%-- <form:errors path="direccion" cssClass="error" /> --%>
							<form:label path="municipio">Municipio*</form:label>
							<form:input path="municipio" type="text" pattern="[A-Za-Z]"
								placeholder="Municipio" class="form-control"
								onblur="aMayusculas(this.value, this.id)" />
							<%-- <form:errors path="municipio" cssClass="error" /> --%>

							<form:label path="provincia">Provincia*</form:label>
							<form:input path="provincia" type="text" placeholder="Provincia"
								class="form-control" onblur="aMayusculas(this.value, this.id)" />
							<%-- <form:errors path="provincia" cssClass="error" /> --%>
							<form:label path="codigo_postal">Código Postal*</form:label>
							<form:input path="codigo_postal" type="text"
								title="Introduzca 5 cifras" maxlength="5"
								placeholder="Código Postal" class="form-control" />
							<%-- <form:errors path="codigo_postal" cssClass="error" /> --%>
							<div class="panel-group" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										<div class="panel-title">
											<p>El importe del abono anual es de 25&#8364. Si dispone
												de tarjeta del transporte público del Consorcio de
												Transportes de Madrid, el importe es de 15&#8364. En este
												caso, marque la casilla e introduzca los 22 dígitos de la
												tarjeta trasporte sin espacios.</p>
											<input id="identifier-1" type="checkbox"
												name="checkConsorcio" ${model.consorcioCheck} /> <label
												for="identifier-1"> </label>
											<div>
												<div class="panel-body">
													<div class="madrid">
														<img src="/resources/img/abono_madrid.jpg"
															style="width: 240px !important; height: 153px !important;"
															title="abono" alt="abono" />
													</div>
													<input placeholder="Número de tarjeta" type="text"
														class="form-control" name="consorcio" id="consorcio"
														title="Numero Consorcio Incorrecto. Introduzca las 22 cifras sin espacios"
														maxlength="22" value="${model.numConsorcio}" />
												</div>
											</div>

										</div>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse">
										<div class="panel-body">
											<div class="madrid">
												<img src="/resources/img/abono_madrid.jpg"
													style="width: 240px !important; height: 153px !important;"
													alt="" />
											</div>
											<input title="Número de tarjeta"
												placeholder="Número de tarjeta" type="text"
												class="form-control" name="consorcio" id="consorcio" />
										</div>
									</div>
								</div>
							</div>
							<p>Si quiere realizar una primera recarga de la tarjeta, por
								favor, elija una cantidad:</p>
							<select
								title="Si quiere realizar una primera recarga de la tarjeta, por
							favor, elija una cantidad"
								name="saldoinicial" id="saldoinicial">
								<option value="0">0</option>
								<option value="10">10</option>
								<option value="15">15</option>
								<option value="25">25</option>
							</select> <input type="submit" class="btn btn-primary" value="COMPRAR"
								name="submit" />
							<p>Los datos con * son obligatorios</p>
						</div>
					</form:form>
				</div>

				<p style="font-size: 0.7em !important;font-style:italic;">
				Los datos personales recogidos de los usuarios del servicio de
							alquiler de bicicleta pública serán incorporados y tratados en un
							fichero cuyo órgano responsable es la empresa BONOPARK, S.L. con
							CIF B71010011 y dirección en la calle Serrano 85, 7º Izda, 28006
							Madrid, ante la que el interesado podrá ejercer los derechos de
							acceso, rectificación, cancelación y oposición, todo lo cual se
							informa en cumplimiento del artículo 5 de referida Ley Orgánica
							15/1999. </br>Los datos no podrán ser cedidos a terceros salvo en los
							supuesto previstos en el artículo 11 de la Ley Orgánica 15/1999,
							de 13 de diciembre, de Protección de Datos de carácter Personal</p>
			</div>
		</div>

	</div>
	<!-- footer-6 -->
	<jsp:include page="/WEB-INF/common/footer-6.jsp" />

	<!-- footer-2 -->
	<jsp:include page="/WEB-INF/common/footer-2.jsp" />




	<!-- Placed at the end of the document so the pages load faster -->

	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>

	<script
		src="<%=request.getContextPath()%>/resources/flat-ui/js/bootstrap.min.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/modernizr.custom.js"></script>
	<!--<script src="<%=request.getContextPath()%>/resources/js/page-transitions.js"></script> -->
	<script src="<%=request.getContextPath()%>/resources/js/easing.min.js"></script>
	<script src="<%=request.getContextPath()%>/resources/js/jquery.svg.js"></script>
	<script
		src="<%=request.getContextPath()%>/resources/js/jquery.svganim.js"></script>
	<script>
		$(function() {
			$("#fecha_nacimiento").datepicker({
				changeMonth : true,
				changeYear : true,
				yearRange : "1920:2014",
				defaultDate : new Date(2000, 0, 1),
			});
		});

		$(document)
				.ready(
						function() {
							$(function($) {
								$.datepicker.regional['es'] = {
									closeText : 'Cerrar',
									prevText : 'Ant',
									nextText : 'Sig',
									currentText : 'Hoy',
									monthNames : [ 'Enero', 'Febrero', 'Marzo',
											'Abril', 'Mayo', 'Junio', 'Julio',
											'Agosto', 'Septiembre', 'Octubre',
											'Noviembre', 'Diciembre' ],
									monthNamesShort : [ 'Ene', 'Feb', 'Mar',
											'Abr', 'May', 'Jun', 'Jul', 'Ago',
											'Sep', 'Oct', 'Nov', 'Dic' ],
									dayNames : [ 'Domingo', 'Lunes', 'Martes',
											'Miércoles', 'Jueves', 'Viernes',
											'Sabado' ],
									dayNamesShort : [ 'Dom', 'Lun', 'Mar',
											'Mié', 'Juv', 'Vie', 'Sab' ],
									dayNamesMin : [ 'Do', 'Lu', 'Ma', 'Mi',
											'Ju', 'Vi', 'Sa' ],
									weekHeader : 'Sm',
									dateFormat : 'dd/mm/yy',
									firstDay : 1,
									isRTL : false,
									showMonthAfterYear : false,
									yearSuffix : '',
								};
								$.datepicker
										.setDefaults($.datepicker.regional['es']);
							});
						});
	</script>

</body>
</html>