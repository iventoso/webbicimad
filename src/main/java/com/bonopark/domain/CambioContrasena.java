package com.bonopark.domain;

import java.io.Serializable;

public class CambioContrasena implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String dni;
	private String 	pwd;
	private String pwdVieja;
	private String pwdNueva;
	private String pwdRep;
	public String getPwdVieja() {
		return pwdVieja;
	}
	public void setPwdVieja(String pwdVieja) {
		this.pwdVieja = pwdVieja;
	}
	public String getPwdNueva() {
		return pwdNueva;
	}
	public void setPwdNueva(String pwdNueva) {
		this.pwdNueva = pwdNueva;
	}
	public String getPwdRep() {
		return pwdRep;
	}
	public void setPwdRep(String pwdRep) {
		this.pwdRep = pwdRep;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	

}
