package com.bonopark.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.bonopark.domain.Residente;
import com.bonopark.domain.Tutor;

@Service("validatorInscripcionTutor")
public class ValidatorInscripcionTutor implements Validator {
	@Autowired
	private ResidenteManager residenteManager;

	private static final int ANNO_ACTUAL = Integer
			.valueOf(new SimpleDateFormat("yyyy").format(new Date()));

	@Override
	public boolean supports(Class<?> clazz) {
		return Tutor.class.isAssignableFrom(clazz); // clase del bean al que
													// da soporte este
													// validador
		// return Residente.class.equals(clazz);

	}

	@Override
	public void validate(Object target, Errors errors) {
		// El nombre es obligatorio
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre",
				"required.nombre", "El nombre es obligatorio");

		// El apellido es obligatorio
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apellido1",
				"required.apellido", "El apellido es obligatorio");

		// El dni es obligatorio
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dni_tutor",
				"required.dni", "El dni es obligatorio");

		Tutor tutor = (Tutor) target;
		if (tutor.getTipo_documento() != 3) {
			if (!isDNI(tutor.getDni_tutor()) && !isTarjetaDeResidenciaValida(tutor.getDni_tutor())) {
				errors.rejectValue("dni_tutor", "inscripcion.dniincorrecto");
				return;
			}
		}
	}

	public static boolean isDNI(String dni) {

		String primerCaracter = null;
		String segundoCaracter = null;
		String tercerCaracter = null;
		String valoresPrimerCaracter = "KLTXYZ";
		String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String nif = null;
		
		if (dni.length() == 11) {
			primerCaracter = dni.substring(0, 1).toUpperCase();
			segundoCaracter = dni.substring(1, 2).toUpperCase();
			tercerCaracter = dni.substring(2, 4).toUpperCase();
			nif = dni.substring(2, 11).toUpperCase();
			if(valoresPrimerCaracter.indexOf(primerCaracter) == -1)
				return false;
			else if (letras.indexOf(segundoCaracter) == -1)
				return false;
			else if (tercerCaracter.equals("00"))
				return false;
			else
				return isNIFValido(nif);
			
		} else if (dni.toUpperCase().startsWith("X")
				|| dni.toUpperCase().startsWith("Y")
				|| dni.toUpperCase().startsWith("Z"))

			dni = dni.substring(1);

		return isNIFValido(dni);
		
	}

	private static boolean isNIFValido(String dni)
	{
		Pattern dniPattern = Pattern
				.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
		Matcher m = dniPattern.matcher(dni);
		if (m.matches()) {
			String letra = m.group(2);
			// Extraer letra del NIF
			String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
			int numeroDni = Integer.parseInt(m.group(1));
			numeroDni = numeroDni % 23;
			String reference = letras.substring(numeroDni, numeroDni + 1);

			if (reference.equalsIgnoreCase(letra)) {

				return true;
			} else {

				return false;
			}
		} else
			return false;
	}
	
// TARJETA DE RESIDENTE
	public static boolean isTarjetaDeResidenciaValida(String cadena) {

		int longitud = 0;
		boolean correcto = true;
		String nif = null;
		String primerCaracter = null;
		String segundoCaracter = null;
		String tercerCaracter = null;
		String valoresPrimerCaracter = "KLTXYZ";
		String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		// Valida la longitud de la cadena.
		longitud = cadena.length();
		if (longitud == 0) {
			return false;
		} else if (!(longitud == 11 || longitud == 9)) {
			return false;
		}

		primerCaracter = cadena.substring(0, 1).toUpperCase();

		if (longitud == 11) {
			segundoCaracter = cadena.substring(1, 2).toUpperCase();
			tercerCaracter = cadena.substring(2, 4).toUpperCase();
			nif = cadena.substring(2, 11).toUpperCase();

			if (valoresPrimerCaracter.indexOf(primerCaracter) == -1)
				correcto = false;
			else if (letras.indexOf(segundoCaracter) == -1)
				correcto = false;
			else if (tercerCaracter.equals("00"))
				correcto = false;
			else
				correcto = isNIFValido(nif);
		}

		if (longitud == 9) {
			if (primerCaracter.equals("X")) {
				nif = "0" + cadena.substring(1, 9).toUpperCase();
				correcto = isNIFValido(nif);
			} else if (primerCaracter.equals("Y")) {
				nif = "1" + cadena.substring(1, 9).toUpperCase();
				correcto = isNIFValido(nif);
			} else if (primerCaracter.equals("Z")) {
				nif = "2" + cadena.substring(1, 9).toUpperCase();
				correcto = isNIFValido(nif);
			} else {
				correcto = false;
			}
		}
		return correcto;
	}
}
