package com.bonopark.repository;
import com.bonopark.domain.*;

import java.util.List;
import java.util.Set;



//import org.springframework.dao.DataAccessException;

/**
 * DAO to manage Residente entities.
 * 
 */
public interface ResidenteDao{

	List<Residente> getUsuarioList();
	String saveUsuario(Residente residente, Usuario usuario, UsuarioInfoExtra usuarioInfoExtra, Tutor tutor);
	Residente getUsuarioByDni(String dni);
	Residente getUsuarioByConfirmacion(String confirmacion);
	boolean updateUsuario(Residente residente);
	boolean deleteUsuarioByDni(String dni);
	int numFactura(String anno);
	String saveDatosFactura(Factura factura);
	public double updateAbono(Residente residente, double importe);
;

//	public Residente getUsuario(String login);
//	public Residente findUsuarioByIdUsuario(Integer idUsuario) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByIdUsuario
//	 *
//	 */
//	public Residente findUsuarioByIdUsuario(Integer idUsuario, int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByDirContaining
//	 *
//	 */
//	public Set<Residente> findUsuarioByDirContaining(String dir) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByDirContaining
//	 *
//	 */
//	public Set<Residente> findUsuarioByDirContaining(String dir, int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByDniContaining
//	 *
//	 */
//	public Set<Residente> findUsuarioByDniContaining(String dni) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByDniContaining
//	 *
//	 */
//	public Set<Residente> findUsuarioByDniContaining(String dni, int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findAllUsuarios
//	 *
//	 */
//	public Set<Residente> findAllUsuarios() throws DataAccessException;
//
//	/**
//	 * JPQL Query - findAllUsuarios
//	 *
//	 */
//	public Set<Residente> findAllUsuarios(int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByDir
//	 *
//	 */
//	public Set<Residente> findUsuarioByDir(String dir_1) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByDir
//	 *
//	 */
//	public Set<Residente> findUsuarioByDir(String dir_1, int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByNombre
//	 *
//	 */
//	public Set<Residente> findUsuarioByNombre(String nombre) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByNombre
//	 *
//	 */
//	public Set<Residente> findUsuarioByNombre(String nombre, int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByDni
//	 *
//	 */
//	public Set<Residente> findUsuarioByDni(String dni_1) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByDni
//	 *
//	 */
//	public Set<Residente> findUsuarioByDni(String dni_1, int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByTlfContaining
//	 *
//	 */
//	public Set<Residente> findUsuarioByTlfContaining(String tlf) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByTlfContaining
//	 *
//	 */
//	public Set<Residente> findUsuarioByTlfContaining(String tlf, int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByTlf
//	 *
//	 */
//	public Set<Residente> findUsuarioByTlf(String tlf_1) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByTlf
//	 *
//	 */
//	public Set<Residente> findUsuarioByTlf(String tlf_1, int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByNombreContaining
//	 *
//	 */
//	public Set<Residente> findUsuarioByNombreContaining(String nombre_1) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByNombreContaining
//	 *
//	 */
//	public Set<Residente> findUsuarioByNombreContaining(String nombre_1, int startResult, int maxRows) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByPrimaryKey
//	 *
//	 */
//	public Residente findUsuarioByPrimaryKey(Integer idUsuario_1) throws DataAccessException;
//
//	/**
//	 * JPQL Query - findUsuarioByPrimaryKey
//	 *
//	 */
//	public Residente findUsuarioByPrimaryKey(Integer idUsuario_1, int startResult, int maxRows) throws DataAccessException;

}