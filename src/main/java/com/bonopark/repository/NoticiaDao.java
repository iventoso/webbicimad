package com.bonopark.repository;

import java.util.List;

import com.bonopark.domain.Estacion;
import com.bonopark.domain.Noticia;



public interface NoticiaDao {
	List<Noticia> getNoticias();
	Noticia getNoticiaById(int id);
	List<Noticia> getUltimasNoticias(int numero);

}
