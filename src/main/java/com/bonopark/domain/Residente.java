package com.bonopark.domain;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "residente")
public class Residente implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int id;
	@Id
	private String dni;
	private String nombre;
	private String apellido1;
	private String apellido2;
	private String email;
	@Column(name = "telefono")
	private String movil;
	//El idioma lo vamos a utilizar en la validacion para recoger si ha activado el pasaporte o el dni
	//Por no crear otro campo en la base de datos
	@Column(name = "idioma")
	private String tipoDocumento;
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date fecha_nacimiento;
	@Column(columnDefinition = "TINYINT")
	private boolean tutor;
	private String direccion;
	@Column(name = "poblacion")
	private String municipio;
	private String provincia;
	private String codigo_postal;
	@Column(name = "numcuenta")
	private String numCuenta;

	private Date fecha;
	private String password;
	private String authority;
	private String confirmacion;

	private String caducidad_tarjeta;
	@Column(columnDefinition = "TINYINT")
	private boolean activo;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "residente")
	private List<Abono> abono = new ArrayList<Abono>();
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "residente")
	private List<Factura> facturas = new ArrayList<Factura>();

	@OneToOne(mappedBy = "residente")
	private Usuario usuario;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido1() {
		return apellido1;
	}

	public void setApellido1(String apellido1) {
		this.apellido1 = apellido1;
	}

	public String getApellido2() {
		return apellido2;
	}

	public void setApellido2(String apellido2) {
		this.apellido2 = apellido2;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String idioma) {
		this.tipoDocumento = idioma;
	}

	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public boolean getTutor() {
		return tutor;
	}

	public void setTutor(boolean tutor) {
		this.tutor = tutor;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(String codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	public String getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getConfirmacion() {
		return confirmacion;
	}

	public void setConfirmacion(String confirmacion) {
		this.confirmacion = confirmacion;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	public List<Abono> getAbono() {
		return abono;
	}

	public void setAbono(List<Abono> abono) {
		this.abono = abono;
	}

	public List<Factura> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<Factura> factura) {
		this.facturas = factura;
	}

	public String getCaducidad_tarjeta() {
		return caducidad_tarjeta;
	}

	public void setCaducidad_tarjeta(String caducidad_tarjeta) {
		this.caducidad_tarjeta = caducidad_tarjeta;
	}

	@Override
	public String toString() {
		return "Residente [id=" + id + ", dni=" + dni + ", nombre=" + nombre
				+ ", apellido1=" + apellido1 + ", apellido2=" + apellido2
				+ ", email=" + email + ", movil=" + movil + ", tipoDocumento="
				+ tipoDocumento + ", fecha_nacimiento=" + fecha_nacimiento
				+ ", tutor=" + tutor + ", direccion=" + direccion
				+ ", municipio=" + municipio + ", provincia=" + provincia
				+ ", codigo_postal=" + codigo_postal + ", numCuenta="
				+ numCuenta + ", fecha=" + fecha + ", password=" + password
				+ ", authority=" + authority + ", confirmacion=" + confirmacion
				+ ", caducidad_tarjeta=" + caducidad_tarjeta + ", activo="
				+ activo + ", usuario=" + usuario + "]";
	}

	
}
