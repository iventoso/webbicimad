<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



<!-- header-1 -->
<div class="header-1">
	<div class="container">
		<div class="row">

			<div class="navbar col-xs-12" role="navigation">
				<div class="col-xs-5">
					<a class="logo2" href="../index.html"><img
						src="/resources/img/logo_madrid.png" alt="logo_madrid" longdesc="index.html"  width="161"
						height="60" /></a>
				</div>

				<div class="col-xs-3">
					<a class="logo" href="../index.html"><img
						src="/resources/img/logo-small@2x_b.png" alt="logo_madrid_peque�o" longdesc="index.html"  width="201"
						height="46" /></a>
				</div>

				<!-- 				<div class="col-xs-4">
					<ul class="nav navbar-nav pull-right">

						<li><a href="#">ESPA�OL</a></li>

						<li><a href="#">FRAN�AIS</a></li>

						<li><a href="#">ENGLISH</a></li>
					</ul>
				</div> -->

			<!-- 	<div class="col-xs-4">
					<ul class="nav navbar-nav pull-right">


						<li><a href="#">
								<div class="fui-facebook"></div>
						</a></li>

						<li><a href="#">
								<div class="fui-twitter"></div>
						</a></li>
					</ul>
				</div> -->

			</div>

		</div>
	</div>
</div>
<!-- header-1 -->



<!-- header-6 -->
<div class="header-6">
	<div class="container">
		<div class="row">
			<div class="navbar col-xs-12" role="navigation">
				<%-- 					<ul class="nav-id-reg nav-logged-out no-js">
						<li class="clearfix"><a href="<c:url value="/j_spring_security_logout"/>"> Log Out</a></li>
					</ul> --%>
				<ul class="nav navbar-nav">
					<li class="dropdown"><a
						class="${param.usuario} dropdown-toggle" href="usuario">Datos
							Personales</a></li>
					<li><a class="${param.recorridos} dropdown-toggle"
						href="recorridos">Recorridos</a></li>
					<li><a class="${param.saldo} dropdown-toggle"
						href="saldo">Saldo</a></li>
					<%-- 				<li><a class="${param.facturas}"
					href="facturas"><fmt:message key="umenu.c" /></a></li>
				<li><a class="${param.promociones}"
					href="promociones"><fmt:message key="umenu.d" /></a></li> --%>
<%-- 					<li><a class="${param.cambiopwd} dropdown-toggle"
						href="cambiopwd">Cambio Contrase�a</a></li> --%>
					<li><a href="<c:url value="/j_spring_security_logout" />">
							Logout</a></li>
				</ul>
			</div>

		</div>
	</div>
</div>
<div class="header-6-sub2">
	<div class="row desplegable">
    	<label for="select"></label>
    	<select onchange="select_link();" id="select" class="select-option"> 
            <option value="usuario" selected="selected">Datos personales</option> 
            <option value="recorridos">Recorridos</option>
            <option value="saldo">Saldo</option> 
            <option value="<c:url value="/j_spring_security_logout" />">Logout</option>
            
    	</select> 
    </div>
	<div class="background2"></div>
</div>

<script>
	function select_link() {
		var myselect = document.getElementById("select");
  		window.location = myselect.options[myselect.selectedIndex].value;
	}
</script>