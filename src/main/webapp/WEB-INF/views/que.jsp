<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="comofunciona" value="active" />
		</jsp:include>

		<section class="content-15 v-center bg-bici">
		<div>
			<div class="container">
				<div class="row">
					<h3>
						Qué es Bici<strong>MAD</strong>
					</h3>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<h6>BiciMAD es el nuevo medio de transporte público de la ciudad de Madrid, un servicio prestado al 100% con bicicletas eléctricas, práctico, sencillo y ecológico.<br /></h6>
                        
					</div>
				</div>
				<div class="row features">
					<div class="col-xs-6">
						<p>
							<img src="/resources/img/quees.jpg" alt="Ciclista" />
						</p>
					</div>

					<div class="col-xs-6">
						<p>El objetivo de BiciMAD es proporcionar un elemento alternativo de transporte limpio y saludable al ciudadano y fomentar el uso de la bicicleta en la ciudad.<br  />El sistema lo componen, en su primera fase: </p>
                        
                        <ul>
                        	<li>1560 bicicletas</li>
                            <li>3.126 anclajes</li>
                            <li>123 estaciones</li>
                        </ul>
                        
                        <p>
                        	BiciMAD destaca por el empleo de las Tecnologías de la Información y la Comunicación para:
                        </p>
                        <p>Mejorar la experiencia del usuario. Interactividad multiplataforma o acceso a la información mediante la propia estación, la web y dispositivos móviles:</p>
                        
                        <ul>
                        	<li>Dispone de información en tiempo real de bicicletas y estaciones disponibles.</li>
                            <li>Puede realizar el alta instantánea mediante tarjeta de crédito o débito.</li>
                        </ul>
                        <p><strong>Dónde puedo informarme</strong></p>
                        <ul>
                        <li>En la web de BiciMAD (www.bicimad.com)</li>
                        <li>En el tótem de las estaciones BiciMAD</li>
                        <li>En la aplicación móvil de BiciMAD</li>
                        <li>En la Oficina de la Bici (www.madrid.es/oficinadelabici)</li>
                        <li>Atención teléfonica 010</li>
                        <li>Oficinas de atención al ciudadano</li>
                        </ul>
					</div>
				</div>
			</div>
			<div class="divider-section-small bg-bici"></div>
			<h1></h1>

		</div>
		</section>
		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>