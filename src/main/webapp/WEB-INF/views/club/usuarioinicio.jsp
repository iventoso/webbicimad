<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%> --%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
<style>
.ui-datepicker {
	width: 216px;
	height: auto;
	margin: 5px auto 0;
	font: 9pt Arial, sans-serif;
	-webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	-moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
	box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, .5);
}

.ui-datepicker a {
	text-decoration: none;
}

.ui-datepicker table {
	width: 100%;
}

.ui-datepicker-header {
	background: #066eb6;
	color: #e0e0e0;
	font-weight: bold;
	-webkit-box-shadow: inset 0px 1px 1px 0px rgba(250, 250, 250, 2);
	-moz-box-shadow: inset 0px 1px 1px 0px rgba(250, 250, 250, .2);
	box-shadow: inset 0px 1px 1px 0px rgba(250, 250, 250, .2);
	text-shadow: 1px -1px 0px #000;
	filter: dropshadow(color =     #000, offx =     1, offy =     -1);
	line-height: 30px;
	border-width: 1px 0 0 0;
	border-style: solid;
	border-color: #0E68C8;
}

.ui-datepicker-title {
	text-align: center;
}

.ui-datepicker-prev,.ui-datepicker-next {
	display: inline-block;
	width: 30px;
	height: 30px;
	text-align: center;
	cursor: pointer;
	line-height: 600%;
	overflow: hidden;
}

.ui-datepicker-prev {
	float: left;
	background: url('resources/img/arrow.png') no-repeat center -30px;
}

.ui-datepicker-next {
	float: right;
	background: url('resources/img/arrow.png') no-repeat center 0px;
}

.ui-datepicker thead {
	background-color: #f7f7f7;
	background-image: -moz-linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #f7f7f7),
		color-stop(100%, #f1f1f1));
	background-image: -webkit-linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	background-image: -o-linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	background-image: -ms-linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	background-image: linear-gradient(top, #f7f7f7 0%, #f1f1f1 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(    startColorstr='#f7f7f7',
		endColorstr='#f1f1f1', GradientType=0);
	border-bottom: 1px solid #bbb;
}

.ui-datepicker th {
	text-transform: uppercase;
	font-size: 6pt;
	padding: 5px 0;
	color: #666666;
	text-shadow: 1px 0px 0px #fff;
	filter: dropshadow(color =     #fff, offx =     1, offy =     0);
}

.ui-datepicker tbody td {
	padding: 0;
	border-right: 1px solid #bbb;
	background: #ecf0f1;
}

.ui-datepicker tbody td:last-child {
	border-right: 0px;
}

.ui-datepicker tbody tr {
	border-bottom: 1px solid #bbb;
}

.ui-datepicker tbody tr:last-child {
	border-bottom: 0px;
}

.ui-datepicker td span,.ui-datepicker td a {
	display: inline-block;
	font-weight: bold;
	text-align: center;
	width: 30px;
	height: 30px;
	line-height: 30px;
	color: #666666;
	text-shadow: 1px 1px 0px #fff;
	filter: dropshadow(color =     #fff, offx =     1, offy =     1);
}

.ui-datepicker-calendar .ui-state-default {
	background: #ededed;
	background: -moz-linear-gradient(top, #ededed 0%, #dedede 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #ededed),
		color-stop(100%, #dedede));
	background: -webkit-linear-gradient(top, #ededed 0%, #dedede 100%);
	background: -o-linear-gradient(top, #ededed 0%, #dedede 100%);
	background: -ms-linear-gradient(top, #ededed 0%, #dedede 100%);
	background: linear-gradient(top, #ededed 0%, #dedede 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(    startColorstr='#ededed',
		endColorstr='#dedede', GradientType=0);
	-webkit-box-shadow: inset 1px 1px 0px 0px rgba(250, 250, 250, .5);
	-moz-box-shadow: inset 1px 1px 0px 0px rgba(250, 250, 250, .5);
	box-shadow: inset 1px 1px 0px 0px rgba(250, 250, 250, .5);
}

.ui-datepicker-unselectable .ui-state-default {
	background: #f4f4f4;
	color: #b4b3b3;
}

.ui-datepicker-calendar .ui-state-hover {
	background: #f7f7f7;
}

.ui-datepicker-calendar .ui-state-active {
	background: #6eafbf;
	-webkit-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
	-moz-box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
	box-shadow: inset 0px 0px 10px 0px rgba(0, 0, 0, .1);
	color: #e0e0e0;
	text-shadow: 0px 1px 0px #4d7a85;
	filter: dropshadow(color =     #4d7a85, offx =     0, offy =     1);
	border: 1px solid #55838f;
	position: relative;
	margin: -1px;
}

.ui-datepicker-calendar td:first-child .ui-state-active {
	width: 29px;
	margin-left: 0;
}

.ui-datepicker-calendar td:last-child .ui-state-active {
	width: 29px;
	margin-right: 0;
}

.ui-datepicker-calendar tr:last-child .ui-state-active {
	height: 29px;
	margin-bottom: 0;
}
.ui-datepicker-month, .ui-datepicker-year {
	color: #000;
}
</style>
<script>
	function validarFormatoFecha() {
		var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
		var campo = this.form.fecha_nacimiento.value;

		if ((campo.match(RegExPattern)) && (campo != '')) {
			return existeFecha(campo);
		} else {
			alert("Formato de fecha incorrecto");
			return false;
		}
	}
	function existeFecha(fecha) {
		var fechaf = fecha.split("/");
		var day = fechaf[0];
		var month = fechaf[1];
		var year = fechaf[2];
		var date = new Date(year, month, '0');
		if ((day - 0) > (date.getDate() - 0)) {
			alert("Fecha incorrecta");
			return false;
		}
		return true;
	}
	function aMayusculas(obj, id)
	{
		obj = obj.toUpperCase();
		document.getElementById(id).value = obj;
	}
</script>
<style>
	input[type="text"]{ width:97% !important; padding-left:1%; padding-bottom:5px; padding-top:5px;}
	input[type="button"]{ width:97% !important;padding-left:1%;padding-bottom:5px; padding-top:5px;}
	input[type="email"]{ width:97%!important;padding-left:1%;padding-bottom:5px; padding-top:5px;}
	.form_settings{ width:50%; float:left;}
	
	@media (max-width: 600px) {
		.form_settings{ width:100%; margin-left:0%; float:left;}
		input[type="text"]{ width:99% !important;}
		input[type="button"]{ width:99% !important;}
		input[type="email"]{ width:99%!important;}
	}

</style>
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/usuarioheader.jsp">
			<jsp:param name="usuario" value="active" />
		</jsp:include>
		<!-- content-15b -->
		<div class="content-15 v-center bg-bici">
		<div class="container">
			<div class="row">
				<h3>Datos Personales</h3>
			</div>
			<form:form method="post" action="validardatos.html"
				commandName="residente">
				<div class="form_settings">
					<form:errors path="*" cssClass="errorblock" element="div" />
					<p>
						<span><form:label path="dni">Número de Documento: </form:label></span><br />
						<form:input title="dni" value='${model.usuario.dni}' path="dni" type="text"
							readonly="true" />
					</p>
					<p>
						<span><form:label path="nombre">
								<fmt:message key="inscripcion.nom" />:</form:label></span><br />
						<form:input value='${model.usuario.nombre}' path="nombre"
							type="text" onblur="aMayusculas(this.value, this.id)"/>
					</p>
					<p>
						<span><form:label path="apellido1">
								<fmt:message key="inscripcion.ape" /> 1: </form:label></span><br />
						<form:input value='${model.usuario.apellido1}' path="apellido1"
							type="text" onblur="aMayusculas(this.value, this.id)"/>
					</p>
					<p>
						<span><form:label path="apellido2">
								<fmt:message key="inscripcion.ape" /> 2: </form:label></span><br />
						<form:input value='${model.usuario.apellido2}' path="apellido2" 
						onblur="aMayusculas(this.value, this.id)"
						 />
					</p>
					<p>
						<span><form:label path="fecha_nacimiento">
								<fmt:message key="inscripcion.nac" /> (dd/MM/aaaa): </form:label></span><br />
						<fmt:formatDate value='${model.usuario.fecha_nacimiento}'
							type="DATE" pattern="dd/MM/yyyy" var="f" />
						<form:input path="fecha_nacimiento" type="text" value='${f}'
								id="fecha_nacimiento"
								name="fecha_nacimiento"/>
					</p>

					
				</div>
                
                
                <div class="form_settings">
                	<p>
						<span><form:label path="movil">
								<fmt:message key="inscripcion.mov" />:</form:label></span><br />
						<form:input value='${model.usuario.movil}' path="movil"
							type="text" 
								title="Introduzca 9 cifras" maxlength="9"/>
					</p>
					<p>
						<span><form:label path="email">
								<fmt:message key="inscripcion.ema" />:</form:label></span><br />
						<form:input value='${model.usuario.email}' path="email"
							type="email" />
					</p>
					<p>
						<span><form:label path="direccion">
								<fmt:message key="inscripcion.dir" />:</form:label></span><br />
						<form:input path="direccion" type="text"
							value='${model.usuario.direccion}' 
							onblur="aMayusculas(this.value, this.id)"/>
					</p>
					<p>
						<span><form:label path="municipio">
								<fmt:message key="inscripcion.mun" />:</form:label></span><br />
						<form:input path="municipio" type="text"
							value='${model.usuario.municipio}' 
							onblur="aMayusculas(this.value, this.id)"/>
					</p>
					<p>
						<span><form:label path="codigo_postal">
								<fmt:message key="inscripcion.cp" />:</form:label></span><br />
						<form:input path="codigo_postal" type="text"
							value='${model.usuario.codigo_postal}' 
								title="Introduzca 5 cifras"
								maxlength="5"/>
					</p>
					<input title="guardar datos usuario" type="submit" class="submit"
						value="<fmt:message
						key="club.modificar" />" /><br /><br /><a
						id="button" href="usuario">Restablecer</a>
                </div>

			</form:form>
		</div>
		<div class="divider-section-small bg-bici"></div>
		</div>

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

		<!-- Placed at the end of the document so the pages load faster -->

	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script>
		$(function() {
			$("#fecha_nacimiento").datepicker({
				changeMonth : true,
				changeYear : true,
				yearRange : "1920:2014",
				defaultDate : new Date(2000, 0, 1),
			});
		});

		$(document)
				.ready(
						function() {
							$(function($) {
								$.datepicker.regional['es'] = {
									closeText : 'Cerrar',
									prevText : 'Ant',
									nextText : 'Sig',
									currentText : 'Hoy',
									monthNames : [ 'Enero', 'Febrero', 'Marzo',
											'Abril', 'Mayo', 'Junio', 'Julio',
											'Agosto', 'Septiembre', 'Octubre',
											'Noviembre', 'Diciembre' ],
									monthNamesShort : [ 'Ene', 'Feb', 'Mar',
											'Abr', 'May', 'Jun', 'Jul', 'Ago',
											'Sep', 'Oct', 'Nov', 'Dic' ],
									dayNames : [ 'Domingo', 'Lunes', 'Martes',
											'Miércoles', 'Jueves', 'Viernes',
											'Sabado' ],
									dayNamesShort : [ 'Dom', 'Lun', 'Mar',
											'Mié', 'Juv', 'Vie', 'Sab' ],
									dayNamesMin : [ 'Do', 'Lu', 'Ma', 'Mi',
											'Ju', 'Vi', 'Sa' ],
									weekHeader : 'Sm',
									dateFormat : 'dd/mm/yy',
									firstDay : 1,
									isRTL : false,
									showMonthAfterYear : false,
									yearSuffix : '',
								};
								$.datepicker
										.setDefaults($.datepicker.regional['es']);
							});
						});
	</script>
</body>
</html>