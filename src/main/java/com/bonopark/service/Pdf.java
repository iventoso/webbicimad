package com.bonopark.service;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import com.bonopark.domain.Residente;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.List;
import com.lowagie.text.ListItem;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Section;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class Pdf {
	//private static String FILE = "/home/igor/Descargas/Utiles/prueba.pdf";
	private static Font catFont = new Font(10);
	private static Font redFont = new Font(18);
	private static Font subFont = new Font(18);
	private static Font smallBold = new Font(18);

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void crearPdf(Map datos, Residente residente) {
		try {
			Document document = new Document();
			PdfWriter.getInstance(document, new FileOutputStream("/home/bicimad/facturasWeb/" //"/home/igor/facturasWeb/" 
					+ datos.get("nombreFactura") + ".pdf"));
			document.open();
			addMetaData(document, datos.get("nombreFactura").toString());
			addContent(document, datos, residente);
			createTable(document);
			document.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// iText allows to add metadata to the PDF which can be viewed in your Adobe
	// Reader
	// under File -> Properties
	private static void addMetaData(Document document, String nombreFactura) {
		document.addTitle("Dbizi-Factura");
		document.addSubject(nombreFactura);
		document.addAuthor("Bonopark S.L.");
		document.addCreator("Bonopark S.L.");
	}

	private static void addContent(Document document, Map datos, Residente residente) throws DocumentException {
		Paragraph datosEmpresa = new Paragraph();
		catFont.setSize(6);
		catFont.setStyle(java.awt.Font.BOLD);
		datosEmpresa.setFont(catFont);
		datosEmpresa.setLeading(10);
		datosEmpresa.add(new Paragraph("BONOPARK S.L. \n N.I.F.: B-71010011 \n Cuesta de la Reina 1, Escalera C1 Oficina 4 \n 31011 Pamplona \n Tfno: 948197236"));
		datosEmpresa.setIndentationLeft(400);
		datosEmpresa.setAlignment(Element.ALIGN_LEFT);
		document.add(datosEmpresa);
		
		Paragraph datosFactura = new Paragraph();
		catFont.setSize(7);
		datosFactura.setFont(catFont);
		String fecha = new SimpleDateFormat("dd.MM.yyyy").format(new Date()); 
		datosFactura.add(new Paragraph(fecha + "\n Factura: " + datos.get("nombreFactura")));
		datosFactura.setAlignment(Element.ALIGN_LEFT);
		datosFactura.setLeading(10);
		datosFactura.setSpacingAfter(10);
		document.add(datosFactura);
				
		Paragraph nombre = new Paragraph();
		catFont.setSize(14);
		catFont.setStyle(java.awt.Font.BOLD);
		nombre.setFont(catFont);
		nombre.add(new Paragraph(residente.getNombre() + " " + residente.getApellido1() + " " + residente.getApellido2()));
		document.add(nombre);		
		Paragraph datosCliente = new Paragraph();
		catFont.setSize(12);
		datosCliente.add(new Paragraph("DNI: " + residente.getDni() + "\n" + residente.getDireccion() + "\n" + residente.getCodigo_postal() + " " + residente.getMunicipio()));
		document.add(datosCliente);
		

	}

	private static void createTable(Document document)
			throws DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.setSpacingBefore(20);
		float[] anchoTable1 ={600,150};	 
		table.setWidths(anchoTable1);

		// t.setBorderColor(BaseColor.GRAY);
		// t.setPadding(4);
		// t.setSpacing(4);
		// t.setBorderWidth(1);
		PdfPCell c1 = new PdfPCell(new Phrase("Concepto", FontFactory.getFont(FontFactory.TIMES, 12, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Importe", FontFactory.getFont(FontFactory.TIMES, 12, Font.BOLD)));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Cuota Anual Abono Residente \nSeguro", FontFactory.getFont(FontFactory.TIMES, 10, Font.NORMAL)));
		c1.setMinimumHeight(500);
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("33,06\n4,14", FontFactory.getFont(FontFactory.TIMES, 10, Font.NORMAL)));
		c1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		table.addCell(c1);
		table.setSpacingAfter(20);
		document.add(table);
		
		PdfPTable table2 = new PdfPTable(3);
		float[] anchoTable2 = {250,100,250};	 
		table2.setWidths(anchoTable2);

		// t.setBorderColor(BaseColor.GRAY);
		// t.setPadding(4);
		// t.setSpacing(4);
		// t.setBorderWidth(1);
		PdfPCell c2 = new PdfPCell(new Phrase("Base Imponible", FontFactory.getFont(FontFactory.TIMES, 12, Font.BOLD)));
		c2.setHorizontalAlignment(Element.ALIGN_CENTER);
		table2.addCell(c2);
		
		c2 = new PdfPCell(new Phrase("IVA 21%", FontFactory.getFont(FontFactory.TIMES, 12, Font.BOLD)));
		c2.setHorizontalAlignment(Element.ALIGN_CENTER);
		table2.addCell(c2);

		c2 = new PdfPCell(new Phrase("Total Factura", FontFactory.getFont(FontFactory.TIMES, 12, Font.BOLD)));
		c2.setHorizontalAlignment(Element.ALIGN_CENTER);
		table2.addCell(c2);

		c2 = new PdfPCell(new Phrase("37,19", FontFactory.getFont(FontFactory.TIMES, 10, Font.NORMAL)));
		c2.setHorizontalAlignment(Element.ALIGN_CENTER);
		table2.addCell(c2);

		c2 = new PdfPCell(new Phrase("7,81", FontFactory.getFont(FontFactory.TIMES, 10, Font.NORMAL)));
		c2.setHorizontalAlignment(Element.ALIGN_CENTER);
		table2.addCell(c2);

		c2 = new PdfPCell(new Phrase("45,00", FontFactory.getFont(FontFactory.TIMES, 10, Font.NORMAL)));
		c2.setHorizontalAlignment(Element.ALIGN_CENTER);
		table2.addCell(c2);
		
		document.add(table2);

		// t.setBorderColor(BaseColor.GRAY);
		// t.setPadding(4);
		// t.setSpacing(4);
		// t.setBorderWidth(1);
	}

	private static void createList(Section subCatPart) {
		List list = new List(true, false, 10);
		list.add(new ListItem("First point"));
		list.add(new ListItem("Second point"));
		list.add(new ListItem("Third point"));
		subCatPart.add(list);
	}

	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}
}
