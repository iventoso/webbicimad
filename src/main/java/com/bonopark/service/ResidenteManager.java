package com.bonopark.service;
import java.io.Serializable;
import java.util.List;

import com.bonopark.domain.*;

public interface ResidenteManager extends Serializable{

	public List<Residente> getUsuarios();
	public boolean confirmarUsuario(String token);
	public boolean comprobarUsuarioByDni(String dni);
	public Residente getUsuariobyDni(String DNI);
	public Residente findUsuarioByDniandEmail(String dni, String email);
	public boolean generarPwd(Residente residente);
	public boolean updateUsuario(Residente residente);
	public boolean borrarUsuarioByDni(String DNI);
	String[] generarFactura(Residente residente, int importeAbono);
	public String recargaUsuario(Residente residente, double importeAbono);
	String saveUsuario(Residente residente, Usuario usuario, UsuarioInfoExtra usuarioInfoExtra, Tutor tutor);
	String[] generarFactura(Residente residente);
}
