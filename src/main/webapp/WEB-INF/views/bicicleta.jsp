<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="comofunciona" value="active" />
		</jsp:include>

		<!-- content-15b -->
		<div class="content-15 v-center bg-bici">
		<div class="container">
			<div class="row">
				<h3>La Bicicleta</h3>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h6>La Booster Bike es una bicicleta a la que se añade tecnología eléctrica que proporciona al usuario asistencia en el pedaleo. Al pedalear, el motor eléctrico se activa asumiendo gran parte del esfuerzo que, de manera convencional, asumiría el usuario. </h6>
				</div>
			</div>
			<div class="row features">
				<div class="col-xs-6">
					<p><img src="/resources/img/Labici.jpg" alt="bicileta" /></p>
				</div>
				<div class="col-xs-6">
					<ul>
						<li>El botón rojo ON/OFF situado en el manillar le permite encender y apagar la bicicleta.</li>
						<li>El botón azul LIGHT enciende y apaga las luces.</li>
						<li>El botón verde MODE regula el nivel de asistencia eléctrica. Las posiciones LOW, MED y HIGH significan respectivamente que la ayuda seleccionada es baja, media o alta.</li>
						<li>El nivel de batería de la bicicleta se indica en la parte derecha del mando de control.</li>
					</ul>
				</div>
			</div>

		</div>
		<h1></h1>
		<div class="divider-section-small bg-bici"></div>
		</div>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>