<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es"> 
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>

<body>

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp" /> 



		<!-- header-6 -->
		<div class="header-6-sub">
		<div class="background"><h1></h1></div>
		</div>



		<!-- content-26 -->
		<div class="content-26 bg-peter-river">
		<div class="container">
			<div class="row">
				<div class="col-xs-8 col-xs-offset-2">
					<h3>&nbsp;</h3>

				</div>
			</div>
			<div class="row features">
				<div class="col-xs-2 col-height">
					<a href="inscribete.html"><img src="/resources/img/inscribete.png" alt="inscríbite" longdesc="inscribete.html" 
						width="100" height="100" /> 
							<h4>Inscríbete</h4></a>
					<h7>Date de alta en la web y recoge tu tarjeta en el tótem de
					las estaciones</h7>
				</div>
				<div class="col-xs-2 col-height">
					<a href="tarifas.html"><img src="/resources/img/tarifas.png" alt="abonos" longdesc="abonos.html" 
						width="100" height="100" />
							<h4>Abonos &amp; Tarifas</h4></a>
					<h7>Consulta nuestros precios</h7>
				</div>
				<div class="col-xs-2 col-height">
					<a href="mapa.html"><img src="/resources/img/mapa.png" alt="mapa" longdesc="mapa.html" width="100"
						height="100" />
							<h4>Estaciones</h4></a>
					<h7>Hay una estación cada 300 metros, escoge la más cercana</h7>
				</div>
				<div class="col-xs-2 col-height">
					<a href="app.html"> <img src="/resources/img/App.png" alt="app" longdesc="app.html"  width="100"
						height="100" />
							<h4>Descarga nuestra APP</h4></a>
					<h7>Compatible con iOS y Android</h7>
				</div>
				<div class="col-xs-2 col-height">
					<a href="club.html"><img src="/resources/img/club-01.png" alt="club" longdesc="club.html" 
						width="100" height="100" />
							<h4>Club BiciMAD</h4></a>
					<h7>Forma parte de nuestro club<br/></h7>
				</div>
				<div class="col-xs-2 col-height">
					<a href="preguntas.html"><img src="/resources/img/info.png" alt="preguntas" longdesc="preguntas.html" 
						width="100" height="100" />
							<h4>Preguntas Frecuentes</h4></a>
					<h7>Todo lo que debes saber de nuestros servicios</h7>
				</div>
			</div>

		</div>
		</div>
		<!--<div class="delimiter"></div>-->

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />
		<!-- Placed at the end of the document so the pages load faster -->
		<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />

</body>
</html>