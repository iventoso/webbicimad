<!-- footer-6 -->
<div class="footer-6 footer">
	<div class="container">
		<div>
			<div class="row">
				<div class="col-xs-12">

					<div class="col-xs-2">
						<h6>COMO FUNCIONA</h6>
						<ul>
							<li><a href="/que.html">Qu� es</a></li>
							<li><a href="/como.html">C�mo se Utiliza</a></li>
							<li><a href="/bicicleta.html">La Bicicleta</a></li>
							<li><a href="/estacion.html">La Estaci�n</a></li>
							<li><a href="/preguntas.html">Preguntas Frecuentes</a></li>
							<li><a href="/app.html">Descarga la App</a></li>
						</ul>
					</div>

					<div class="col-xs-2">
						<h6>PLANO & ESTACIONES</h6>
						<ul>
							<li><a href="/mapa.html">Mapa</a></li>
						</ul>
					</div>

					<div class="col-xs-2">
						<h6>ABONOS & TARIFAS</h6>
						<ul>
							<li><a href="/abonos.html">Abonos</a></li>
							<li><a href="/tarifas.html">Tarifas</a></li>
						</ul>
					</div>

					<div class="col-xs-2">
						<h6>SEGURIDAD</h6>
						<ul>
							<li><a href="/seguro.html">Seguro</a></li>
							<li><a href="/normativa.html">Normativa</a></li>
							<li><a href="/conducta.html">Consejos y Recomendaciones</a></li>
						</ul>
					</div>

					<div class="col-xs-2">
						<h6>INSCR�BETE</h6>
						<ul>
							<li><a href="/inscribete.html">Rellene sus datos</a></li>
						</ul>
					</div>

					<div class="col-xs-2">
						<h6>�REA USUARIO</h6>
						<ul>
							<li><a href="/login">Tu informaci�n</a></li>
						</ul>
					</div>

					<div class="col-xs-2">
						<h6>CONTACTO</h6>
						<ul>
							<li><a href="/contacto.html">Contacto</a></li>
						</ul>
					</div>

					<div class="col-xs-2">
						<h6>CLUB</h6>
						<ul>
							<li><a href="/club.html">Club BiciMAD</a></li>
						</ul>
					</div>

				</div>
			</div>

		</div>


	</div>
</div>