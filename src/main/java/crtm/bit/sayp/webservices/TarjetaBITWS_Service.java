/**
 * TarjetaBITWS_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package crtm.bit.sayp.webservices;

public interface TarjetaBITWS_Service extends javax.xml.rpc.Service {
    public java.lang.String getTarjetaBITWSSoapHttpAddress();

    public crtm.bit.sayp.webservices.TarjetaBITWS_PortType getTarjetaBITWSSoapHttp() throws javax.xml.rpc.ServiceException;

    public crtm.bit.sayp.webservices.TarjetaBITWS_PortType getTarjetaBITWSSoapHttp(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
