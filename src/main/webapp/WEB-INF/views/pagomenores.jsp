<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%> --%>
		<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<%@ page import="java.security.*"%>

<jsp:include page="/WEB-INF/common/head.jsp" />
<head>

<title>BiciMAD :: Pago</title>


<script language="JavaScript">
	function estado() {
		if (this.pago.checkcondiciones.checked) {

			return true;

		} else {
			alert("No has aceptado las Condiciones de Uso");
			return false;

		}

	}
</script>

<script
	src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha1.js"></script>
<script language="javascript">
	function atras() {
		history.back();
	}
</script>
<style>
#btnCancelar,#btnContinuar {
	width: 100% !important;
	float: left;
}

@media only screen and (max-width: 760px) {
	#btnCancelar,#btnContinuar {
		width: 100% !important;
	}
	#btnCancelar {
		margin-left: 0% !important;
	}
}
</style>

</head>
<body>
<div id="main">
		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp" />

		<div class="contacts-3">
			<div class="container">
				<div class="row">
					<div class="col-xs-10" style="margin-bottom: 40px;">

						<h1></h1>
						<h5>
							<fmt:message key="pago.titulo" />
						</h5>
						<div class="mensaje">
							<ul class="texto_lista">
								<li><p>Número de Documento: ${model.dni}</p></li>
								<li><p>Nombre: ${model.nombre}</p></li>
								<li><p>Apellido1: ${model.apellido1}</p></li>
								<li><p>Apellido2: ${model.apellido2}</p></li>
								<li><p>Móvil: ${model.movil}</p></li>
								<li><p>Email: ${model.email}</p></li>
								<li><p>Nombre tutor: ${model.tutor.nombre}</p></li>
								<li><p>Apellido 1 tutor: ${model.tutor.apellido1}</p></li>
								<li><p>Apellido 2 tutor: ${model.tutor.apellido2}</p></li>
								<li><p>Importe Abono: ${model.importeAbono} &#8364</p></li>
								<li><p>Importe Saldo Inicial: ${model.saldoinicial} &#8364</p></li>
								<li><p>Importe Total: ${model.importeTotal} &#8364</p></li>
							</ul>
						</div>
						<form name=pago
							action='https://sis.redsys.es/sis/realizarPago'
							method="post" onsubmit="return estado()">
							<div class="form_settings">
								<input type="hidden" name="Ds_Merchant_MerchantName"
									value="BiciMad - Bonopark"> </input> <input type="hidden"
									name=Ds_Merchant_MerchantCode value="285970679"> </input> <input
									type="hidden" name=Ds_Merchant_Terminal value="1"> </input> <input
									type="hidden" name=Ds_Merchant_Order
									value="${model.merchantOrder}"> </input> <input type="hidden"
									name=Ds_Merchant_Amount value="${model.importeTPV}"></input> <input
									type="hidden" name=Ds_Merchant_Currency value="978"></input> <input
									type="hidden" name=Ds_Merchant_TransactionType value="0"></input>
								<input type="hidden" name=Ds_Merchant_MerchantURL
									value="${model.notificacionTPV}"></input> <input type="hidden"
									name=Ds_Merchant_MerchantSignature value="${model.firmaTPV}"></input>
								<input type="hidden" name=Ds_Merchant_Identifier
									value="REQUIRED"></input> <input type="hidden"
									name=Ds_Merchant_MerchantData value="${model.datos}"></input> <input
									type="hidden" name=Ds_Merchant_UrlOK
									value="http://www.bicimad.com/pagook.html"></input>
								<input type="hidden" name=Ds_Merchant_UrlKO
									value="http://www.bicimad.com/pagoko.html"></input>
								<input type="checkbox" id="checkcondiciones" value="check"
									name="checkcondiciones" /> He leído y aceptado las 
									<a <fmt:message key="normativa.pdfnormativa" /> target="_blank">
										<fmt:message
										key="normativa.txt3" /></a> 
										<input type="submit" class="submit"
									value="<fmt:message
						key="inscripcion.continuar" />" />
								<%-- <button type="submit" class="submit"
								value="<fmt:message
						key="inscripcion.continuar" />" /> --%>
								<input type='button' name='btnCancelar' id='btnCancelar'
									value='Cancelar' onclick="atras();" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>


			<!-- footer-6 -->
			<jsp:include page="/WEB-INF/common/footer-6.jsp" />

			<!-- footer-2 -->
			<jsp:include page="/WEB-INF/common/footer-2.jsp" />

		</div>

		<!-- Placed at the end of the document so the pages load faster -->
		<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>