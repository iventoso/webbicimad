package com.bonopark.service;

import java.sql.*;

import javax.swing.JOptionPane;

public class ConexionBBDD {
	@SuppressWarnings("finally")
	public static Connection GetConnection(String ip) {
		Connection conexion = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");
			String servidor = "jdbc:mysql://" + ip + ":3306/neo_bonopark_madrid";
			String usuarioDB = "root";
			String passwordDB = "g077um";
			conexion = DriverManager.getConnection(servidor, usuarioDB,
					passwordDB);
		} catch (ClassNotFoundException ex) {
			System.out.println("Error conexion BBDD: " + ex);
			conexion = null;
		} catch (SQLException ex) {
			System.out.println("Error conexion BBDD: " + ex);
			conexion = null;
		} catch (Exception ex) {
			System.out.println("Error conexion BBDD: " + ex);
			conexion = null;
		} finally {
			System.out.println("Conexion : " + conexion);
			return conexion;
		}
	}
}
