<%-- <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%> --%>
<%@page contentType="text/html; charset=ISO-8859-1" pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BiciMAD :: Inscribete</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>

<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp" />

		<div class="divider-section"></div>


		<!-- --------------- MODAL 1 Condiciones de uso-------------->

		<div class="modal fade" id="Condiciones_modal" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Condiciones de Uso</h4>
					</div>
					<div class="modal-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Mauris magna orci, posuere eu tincidunt sit amet, rhoncus nec
							erat. Maecenas eu convallis orci, eget eleifend ante. Vivamus leo
							ligula, auctor vitae erat euismod, sodales facilisis nunc. Mauris
							iaculis bibendum libero vel sagittis. Proin ac justo in libero
							laoreet fermentum. Nulla ut euismod ligula. In a purus ipsum.
							Phasellus odio urna, sagittis et risus sit amet, dapibus molestie
							nunc.</p>

						<p>Nam a condimentum neque. Quisque euismod imperdiet
							pharetra. Proin rhoncus mauris a leo sodales viverra. Praesent
							eget magna auctor, rutrum mi ac, faucibus quam. Curabitur a elit
							suscipit, volutpat lectus eget, facilisis nisi. Nullam est
							mauris, suscipit sit amet risus nec, tincidunt porta risus.
							Vivamus semper nibh sed iaculis consectetur.</p>

						<p>Aenean urna nisl, dignissim nec tortor a, convallis tempor
							est. Vivamus placerat tellus a leo facilisis auctor. Suspendisse
							semper massa sit amet metus tristique porttitor. Nullam eleifend
							ullamcorper enim. Nunc eu ante vehicula purus condimentum
							hendrerit. Mauris tempus sem a leo tincidunt, nec posuere augue
							lacinia. Donec elementum, nulla in tristique sollicitudin, dui
							ipsum blandit risus, nec rutrum eros purus in enim. Maecenas ac
							elementum lacus, pellentesque auctor nunc. Lorem ipsum dolor sit
							amet, consectetur adipiscing elit. Morbi sit amet scelerisque
							purus, et dictum justo.</p>

						<p>Nam a condimentum neque. Quisque euismod imperdiet
							pharetra. Proin rhoncus mauris a leo sodales viverra. Praesent
							eget magna auctor, rutrum mi ac, faucibus quam. Curabitur a elit
							suscipit, volutpat lectus eget, facilisis nisi. Nullam est
							mauris, suscipit sit amet risus nec, tincidunt porta risus.
							Vivamus semper nibh sed iaculis consectetur.</p>

						<p>Aenean urna nisl, dignissim nec tortor a, convallis tempor
							est. Vivamus placerat tellus a leo facilisis auctor. Suspendisse
							semper massa sit amet metus tristique porttitor. Nullam eleifend
							ullamcorper enim. Nunc eu ante vehicula purus condimentum
							hendrerit. Mauris tempus sem a leo tincidunt, nec posuere augue
							lacinia. Donec elementum, nulla in tristique sollicitudin, dui
							ipsum blandit risus, nec rutrum eros purus in enim. Maecenas ac
							elementum lacus, pellentesque auctor nunc. Lorem ipsum dolor sit
							amet, consectetur adipiscing elit. Morbi sit amet scelerisque
							purus, et dictum justo.</p>


					</div>
					<div class="modal-footer">
						<input type="checkbox" name="" value="Acepto las concidiones">Acepto
						las condiciones
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<!-- --------------- MODAL 1 -------------->


		<!-- --------------- MODAL 2 Datos-------------->

		<div class="modal fade" id="Datos_modal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Tratamiento de
							Datos</h4>
					</div>
					<div class="modal-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Mauris magna orci, posuere eu tincidunt sit amet, rhoncus nec
							erat. Maecenas eu convallis orci, eget eleifend ante. Vivamus leo
							ligula, auctor vitae erat euismod, sodales facilisis nunc. Mauris
							iaculis bibendum libero vel sagittis. Proin ac justo in libero
							laoreet fermentum. Nulla ut euismod ligula. In a purus ipsum.
							Phasellus odio urna, sagittis et risus sit amet, dapibus molestie
							nunc.</p>
					</div>
					<div class="modal-footer">
						<input type="checkbox" name="" value="Acepto las concidiones">Acepto
						el tratamiento de datos
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<!-- --------------- MODAL 2 -------------->


		<!-- --------------- MODAL 3 18 años-------------->

		<div class="modal fade" id="18_modal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Mayoría de edad</h4>
					</div>
					<div class="modal-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Mauris magna orci, posuere eu tincidunt sit amet, rhoncus nec
							erat. Maecenas eu convallis orci, eget eleifend ante. Vivamus leo
							ligula, auctor vitae erat euismod, sodales facilisis nunc. Mauris
							iaculis bibendum libero vel sagittis. Proin ac justo in libero
							laoreet fermentum. Nulla ut euismod ligula. In a purus ipsum.
							Phasellus odio urna, sagittis et risus sit amet, dapibus molestie
							nunc.</p>
					</div>
					<div class="modal-footer">
						<input type="checkbox" name="" value="Acepto las concidiones">Soy
						mayor de 18 años
						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<!-- --------------- MODAL 3 -------------->



		<!-- contacts-3 -->
		<section class="contacts-3">
			<div class="container">
				<div class="row">
					<div class="col-sm-5">
						<h5>INSCRIPCIÓN</h5>
						<h6>Rellene este formulario de incripción para darse de alta
							en nuestros servicios online y formar parte de nuestro Club Bike
							Boosters.</h6>

						<h5>Al rellenar este formulario acepto que:</h5>

						<ul>
							<li><a href="#Condiciones_modal" data-toggle="modal">He
									leido y aceptado las Condiciones de Uso</a></li>
							<li><a href="#Datos_modal" data-toggle="modal">Doy mi
									consentimiento al tratamiento de mis datos</a></li>
							<li><a href="#18_modal" data-toggle="modal">Soy mayor de
									18 años</a></li>
						</ul>
					</div>
					<div class="col-sm-5 col-sm-offset-1">
						<form:form commandName="residente" method="post"
							action="validarinscripcion.html">
							<select name="tipodocumento">
								<option value="1">DNI</option>
								<option value="2">Tarjeta Residente</option>
								<option value="3">Pasaporte</option>
							</select>
							<form:errors path="*" cssClass="errorblock" element="div" />

							<form:label path="dni"></form:label>
<							<form:errors path="dni" cssClass="error" />
							<form:input path="dni" type="text" required="required"
								maxlength="9" placeholder="Nº Documento" class="form-control"></form:input>



							<form:label path="nombre"></form:label>
							<form:errors path="nombre" cssClass="error" />
							<form:input path="nombre" type="text" pattern="[A-Za-Z]"
								title="Nombre Incorrecto" placeholder="Nombre"
								class="form-control" />



							<form:label path="apellido1"></form:label>
							<form:input path="apellido1" type="text" required="required"
								placeholder="Primer Apellido" class="form-control" />
							<form:errors path="apellido1" cssClass="error" />


							<form:label path="apellido2"></form:label>
							<form:input path="apellido2" type="text"
								placeholder="Segundo Apellido" class="form-control" />

							<form:label path="fecha_nacimiento"></form:label>
							<fmt:formatDate value='${model.usuario.fecha_nacimiento}'
								type="date" pattern="yyyy/MM/dd" var="f" />
							<form:input path="fecha_nacimiento" type="text"
								value='${model.fecha}' required="required"
								placeholder="Fecha de Nacimiento" class="form-control" />
							<form:errors path="fecha_nacimiento" cssClass="error" />


							<form:label path="movil"></form:label>
							<form:input path="movil" type="text" required="required"
								pattern="[0-9]{9}" title="Movil Incorrecto" maxlength="9"
								placeholder="Móvil" class="form-control" />
							<form:errors path="movil" cssClass="error" />


							<form:label path="email"></form:label>
							<form:input path="email" type="email" required="required"
								placeholder="Email" class="form-control" />
							<form:errors path="email" cssClass="error" />


							<form:label path="direccion"></form:label>
							<form:input path="direccion" type="text" required="required"
								placeholder="Direccion" class="form-control" />
							<form:errors path="direccion" cssClass="error" />

							<form:label path="municipio"></form:label>
							<form:input path="municipio" type="text" required="required"
								pattern="[A-Za-Z]" title="Municipio Incorrecto"
								placeholder="Municipio" class="form-control" />
							<form:errors path="municipio" cssClass="error" />
							
							<form:label path="provincia"></form:label>
							<form:input path="provincia" type="text" required="required"
								pattern="[A-Za-Z]" title="Municipio Incorrecto"
								placeholder="Provincia" class="form-control" />
							<form:errors path="provincia" cssClass="error" />

							<form:label path="codigo_postal"></form:label>
							<form:input path="codigo_postal" type="text" required="required"
								pattern="[0-9]{5}" title="Codigo Postal Incorrecto"
								maxlength="5" placeholder="Código Postal" class="form-control" />
							<form:errors path="codigo_postal" cssClass="error" />

							<div class="panel-group" id="accordion">

								<!-- 								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion"
												href="#collapseOne"> Compra de ABONO 25€ </a>
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse">
										<div class="panel-body">


											<button type="submit" class="btn btn-primary">COMPRAR</button>


										</div>
									</div>
								</div> -->
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
<!-- 											<a data-toggle="collapse" data-parent="#accordion"
												href="#collapseTwo"> - Con Abono tranporte de Madrid -<br>
												10 € de Descuento
											</a> -->
											<input data-toggle="collapse" data-parent="#accordion"
												href="#collapseTwo" type="checkbox" name="consorcio" value="consorcio"> Con Abono tranporte de Madrid -<br>
												10 € de Descuento<br>
										</h4>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse">
										<div class="panel-body">

											<div class="madrid">
												<img src="/resources/img/abono_madrid.jpg"
													width="240" height="153" alt="">
											</div>
											<input placeholder="Número de tarjeta" type="text"
												class="form-control" name="consorcio" id="consorcio"/>
										</div>
									</div>
								</div>
							</div>
 							<select name="saldoinicial">								
								<option value="5">5</option>
								<option value="10">10</option>
								<option value="20">20</option>
							</select> 

							<input type="submit" class="btn btn-primary" value="COMPRAR" />
							<!-- 							<button type="submit" class="btn btn-primary">COMPRAR</button> -->
						</form:form>

					</div>
				</div>
			</div>
	</div>


	</section>



	<!-- footer-6 -->
	<jsp:include page="/WEB-INF/common/footer-6.jsp" />

	<!-- footer-2 -->
	<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>