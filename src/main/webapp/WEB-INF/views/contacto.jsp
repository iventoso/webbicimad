<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="es">
<head>
<title>BiciMAD :: Contacto</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>

<body>

	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="contacto" value="active" />
		</jsp:include>


		<!-- content-15 -->
		<div class="content-15 v-center bg-bici">
		<div>
			<div class="container">
				<div class="row">
                	<div class="row">
                        <h3 style="margin-left:30px;">
                            Contacto
                        </h3>
                    </div>
                	<div class="col-xs-12">
                        <p>- En el teléfono 010 (915 298 210 si llama desde fuera de la ciudad de Madrid).</p>
                        <p>- Sin cita previa en las Oficinas de Atención al Ciudadano Línea Madrid.</p>
                        <p>- En el tótem de cualquier estación.</p>
                    </div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<p>El usuario podrá formular sugerencias y reclamaciones relativas al servicio por teléfono, llamando al 010 (915 298 210 si llama desde fuera de la ciudad de Madrid), presencialmente sin cita previa en las Oficinas de Atención al Ciudadano Líneamadrid (OAC), cumplimentando el formulario de <a target="_blank" href="http://www.madrid.es/portales/munimadrid/es/Inicio/Ayuntamiento/Contactar/Sugerencias-y-reclamaciones?vgnextfmt=default&vgnextchannel=5eadc1ab4fd86210VgnVCM2000000c205a0aRCRD">Sugerencias y Reclamaciones</a> en la página web del Ayuntamiento de Madrid, <a href="http://www.madrid.es">www.madrid.es</a>, o mediante su presentación en los registros de los Distritos, en los restantes registros del Ayuntamiento de Madrid, en los registros de la Administración General del Estado, en los de las Comunidades Autónomas, o mediante las demás formas previstas en el art. 38.4 de la Ley 30/1992, de 26 de noviembre.</p>
					</div>
				</div>

			</div>
		</div>
		</div>



		<!-- price-2 -->
		<div class="price-2 bg-bici">
		<div class="container">
			<div class="plans">
				<div class="plan">
					<div class="title2">
						<span class="fui-phone"></span> 010 - Línea Madrid
					</div>
					<div class="description">
						<div class="description-box2">Atención al ciudadano del Ayuntamiento de Madrid</div>
					</div>
				</div>

			</div>
		</div>


		</div>


		<!-- contacts-5 -->
		<div class="contacts-5">

		<div class="container">
			<div class="row">
				<div class="col-xs-4">
					<div class="box box-first">
						<h5>Bonopark</h5>
						<p>
							Calle Serrano, 85<br>28006 Madrid, España
						</p>
					</div>
					<br class="hidden-xs">

				</div>
				<div class="col-xs-4  box">
					<h5>Ayuda</h5>
					<p>
						Si necesita ayuda urgente puede contactar con el teléfono de
						atención ciudadana 010 y exclusivamente para el caso de
						incidencias al 900505463
					</p>
				</div>
				<div class="col-xs-4 box">
					<h5>Preguntas frecuentes</h5>
					<p>
						Recuerda que hemos hecho una selección de las dudas más frecuentes
						acerca de nuestro servicio y las puedes consultar en <a
							href="preguntas.html">Preguntas frecuentes</a>
					</p>
				</div>
			</div>
		</div>
		</div>
		<h1></h1>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>