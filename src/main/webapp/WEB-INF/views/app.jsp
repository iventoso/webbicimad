<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD :: Servicios</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="comofunciona" value="active" />
		</jsp:include>



		<div class="divider-section"></div>


		<!-- content-15b -->
		<div class="content-15 v-center">

		<div class="container">
			<div class="col-xs-12">

				<div class="row col-xs-offset-2">
					<h3>Descarga la App</h3>
				</div>

				<div class="row">
					<div class="col-xs-4 col-xs-offset-2">
						<img src="/resources/img/apple.jpg"  alt="appStore" />
							<p>
								<a href="http://itunes.com/app/bicimad">Ir al Apple Appstore</a>
							</p>
					</div>
					<div class="col-xs-4">
						<img src="/resources/img/android.jpg" alt="android" />
							<p>
								<a href="http://market.android.com/details?id=com.bonopark.bicimad">Ir al Google Playstore</a>
							</p>
					</div>
				</div>
				
				<div class="row" style="text-align:center;">
                	<p style="max-width:200px;">
						<img src="/resources/img/qr_code_bicimad.png"  alt="código" />
                    </p>
				</div>
			</div>
		</div>
		<h1></h1>
		<div class="divider-section-big"></div>

		</div>
		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>