<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>jQuery UI Datepicker - Default functionality</title>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
</head>
<body>

	<p>
		Date: <input type="text" id="datepicker">
	</p>



	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="comofunciona" value="active" />
		</jsp:include>

		<!-- content-15b -->
		<section class="content-15 v-center bg-bici">
		<div class="container">
			<div class="row">
				<h3>La Bicicleta</h3>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<h6>Nuestra Booster Bike es una bicicleta a la que se a�ade
						tecnolog�a el�ctrica que proporciona al usuario asistencia en el
						pedaleo. Al pedalear, el motor el�ctrico se activa asumiendo gran
						parte del esfuerzo que, de manera convencional, asumir�a el
						usuario.</h6>
				</div>
			</div>
			<div class="row features">
				<div class="col-xs-6">
					<img src="/resources/img/Labici.jpg" width="540"
						height="441" alt="">
				</div>
				<div class="col-xs-5">
					<ul>
						<li>El bot�n rojo ON/OFF situado en el manillar le permite
							encender y apagar la bicicleta.</li>
						<li>El bot�n azul LIGHT enciende y apaga las luces.</li>
						<li>El bot�n verde MODE regula el nivel de asistencia
							el�ctrica. Las posiciones LOW, MED y HIGH significan
							respectivamente que la ayuda seleccionada es baja, media o alta.</li>
						<li>El nivel de bater�a de la bicicleta se indica en la parte
							derecha del mando de control.</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="divider-section-small bg-bici"></div>
		</section>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>