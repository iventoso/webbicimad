package com.bonopark.web;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.AxisFault;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.bonopark.domain.Base;
import com.bonopark.domain.Estacion;
import com.bonopark.service.EstacionManager;
import com.bonopark.service.SimpleServicioCorreo;

import crtm.bit.sayp.webservices.TarjetaBITWSProxy;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private EstacionManager estacionManager;

	// @Resource
	// private ServicioCorreo servicioCorreo;
	private static final Logger logger = LoggerFactory
			.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 */

	public static void detectarIdioma(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			RequestContextUtils.getLocaleResolver(request).setLocale(request,
					response, new Locale(request.getParameter("siteLanguage")));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@RequestMapping("/que.html")
	public ModelAndView que(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));

		return new ModelAndView("que", "model", myModel);
	}

	@RequestMapping("/index.html")
	public ModelAndView index(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));

		return new ModelAndView("index", "model", myModel);
	}
	
	@RequestMapping("/copymapa.html")
	public ModelAndView copymapa(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));

		return new ModelAndView("Copymapa", "model", myModel);
	}

	@RequestMapping("/app.html")
	public ModelAndView app(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));

		return new ModelAndView("app", "model", myModel);
	}

	@RequestMapping("/como.html")
	public ModelAndView como(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));

		return new ModelAndView("como", "model", myModel);
	}

	@RequestMapping("/bicicleta.html")
	public ModelAndView bicicleta(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));

		return new ModelAndView("bicicleta", "model", myModel);
	}

	@RequestMapping("/estacion.html")
	public ModelAndView estacion(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));

		return new ModelAndView("estacion", "model", myModel);
	}

	@RequestMapping("/")
	public ModelAndView home(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("index", "model", myModel);
	}

	@RequestMapping("/contacto.html")
	public ModelAndView contacto(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			request.setCharacterEncoding("UTF-8");
			response.setContentType("text/html;charset=UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		return new ModelAndView("contacto", "model", myModel);
	}

	@RequestMapping("/componentes.html")
	public ModelAndView componentes(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("componentes", "model", myModel);
	}

	@RequestMapping("/compra.html")
	public ModelAndView compra(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("compra", "model", myModel);
	}

	@RequestMapping("/club.html")
	public ModelAndView club(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("club", "model", myModel);
	}

	// @RequestMapping("/datos.html")
	// public ModelAndView datos(HttpServletRequest request,
	// HttpServletResponse response) {
	// detectarIdioma(request, response);
	// Map<String, Object> myModel = new HashMap<String, Object>();
	// //myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
	// return new ModelAndView("datos", "model", myModel);
	// }

	@RequestMapping("/horario.html")
	public ModelAndView horario(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("horario", "model", myModel);
	}

	@RequestMapping("/info.html")
	public ModelAndView info(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("info", "model", myModel);
	}

	@RequestMapping("/abonos.html")
	public ModelAndView abonos(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("abonos", "model", myModel);
	}

	@RequestMapping("/mapa.html")
	public ModelAndView mapa(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
//		List<Estacion> est = estacionManager.getEstaciones();
//		myModel.put("estaciones", est);
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("mapa", "model", myModel);
	}

	@RequestMapping("/normativa.html")
	public ModelAndView normativa(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("normativa", "model", myModel);
	}

	@RequestMapping("/conducta.html")
	public ModelAndView conducta(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("conducta", "model", myModel);
	}

	@RequestMapping("/preguntas.html")
	public ModelAndView preguntas(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("preguntas", "model", myModel);
	}

	@RequestMapping("/recarga.html")
	public ModelAndView recarga(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("recarga", "model", myModel);
	}

	@RequestMapping("/seguro.html")
	public ModelAndView seguro(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("seguro", "model", myModel);
	}

	@RequestMapping("/tarifas.html")
	public ModelAndView tarifas(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("tarifas", "model", myModel);
	}

	@RequestMapping("/telefono.html")
	public ModelAndView telefono(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("telefono", "model", myModel);
	}

	@RequestMapping("/sistema.html")
	public ModelAndView sistema(HttpServletRequest request,
			HttpServletResponse response) {
		Map<String, Object> myModel = new HashMap<String, Object>();
		detectarIdioma(request, response);
		List<Estacion> est = estacionManager.getEstaciones();
		myModel.put("estaciones", est);
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("sistema", "model", myModel);
	}

	@RequestMapping(value = "/enviarmensaje.html", method = RequestMethod.POST)
	public ModelAndView enviarMensaje(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		String nombre = (String) request.getParameter("nombre");
		String apellidos = (String) request.getParameter("apellidos");
		String movil = (String) request.getParameter("movil");
		String email = (String) request.getParameter("email");
		String mensaje = (String) request.getParameter("mensaje");
		SimpleServicioCorreo servicioCorreo = new SimpleServicioCorreo();
		servicioCorreo.sendContacto("i.ventoso@bonopark.es",
				"Mensaje Contacto Web", nombre, apellidos, movil, email,
				mensaje);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mensaje", "contactomensaje.txt");
		// myModel.put("noticias", noticiaManager.getUltimasNoticias(3));
		return new ModelAndView("mensaje", "model", myModel);
	}

	@RequestMapping("/mensajeTPV.html")
	public ModelAndView mensajeTPV(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mensaje", "Transaccion con TPV Virtual");
		return new ModelAndView("mensaje", "model", myModel);

	}

	@RequestMapping("/factura.html")
	public ModelAndView factura(HttpServletRequest request,
			HttpServletResponse response) {
		detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mensaje", "Transaccion con TPV Virtual");
		return new ModelAndView("factura");

	}

	@RequestMapping(value = "/pago.html", method = RequestMethod.GET)
	public ModelAndView pago(HttpServletRequest request,
			HttpServletResponse response) {
		HomeController.detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// return new ModelAndView("inscripcion", "command", new Residente());
		return new ModelAndView("pago", "model", myModel);
	}

}
