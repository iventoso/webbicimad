<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
<style>
	.divider-section-small{ min-height:400px !important;}
</style>
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/usuarioheader.jsp">
			<jsp:param name="saldo" value="active" />
		</jsp:include>
		<!-- content-15b -->

		<div class="content-15 v-center bg-bici">

		<div class="container">
			<div class="plans">
				<div class="plan">
					<div class="row">
						<h3>Saldo</h3>
					</div>
					<div class="title">SALDO ACTUAL</div>
					<div class="description">
						<div class="description-box">
							Saldo Actual: <b>${model.abono.saldo}</b>
						</div>
						<div class="description-box">
							Fecha de Creación: <b>
							<fmt:formatDate value='${model.abono.fecha}'
							type="DATE" pattern="dd/MM/yyyy" var="f" />
							${f}</b>
						</div>
						<div class="description-box">
							Duración (días): <b>${model.abono.duracion}</b>
						</div>
					</div>
				</div>
				<br></br> Recarga de Saldo
				<form:form method="post" action="recargasaldo.html" name="form">
					<input type="submit" title="10" value="10" id="evento_nuevo" name="10" />
					<input type="submit" title="15" value="15" id="evento_eliminar" name="15" />
					<input type="submit" title="25" value="25" id="evento_cancelar" name="25" />
					<input title="saldo actual" type="hidden" value="${model.abono.saldo}" name="saldoactual"/>
				</form:form>
			</div>
		</div>
		<div class="divider-section-small bg-bici"></div>
		</div>

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>
	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>