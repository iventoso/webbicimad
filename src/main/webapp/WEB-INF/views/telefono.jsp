<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD ::</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>

<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp" />
		<div class="divider-section"></div>



		<!-- content-15 -->
		<section class="content-15 v-center bg-bici">
		<div>
			<div class="container">
				<div class="row">
					<h3>Teléfonos</h3>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<p>Contáctenos por los siguientes teléfonos si necesita
							infirmación, tiene alguna duda o un incidente .</p>


					</div>
				</div>

			</div>
		</div>
		</section>



		<!-- price-2 -->
		<section class="price-2 bg-bici">
		<div class="container">
			<div class="plans">
				<div class="plan">
					<div class="title2">
						<span class="fui-phone"></span> 010
					</div>
					<div class="description">
						<div class="description-box2">Atención al ciudadano de la
							Comunidad de Madrid</div>
					</div>
				</div>

				<div class="plan plan-2">
					<div class="title2">
						<span class="fui-phone"></span> 800 100 100

					</div>
					<div class="description">
						<div class="description-box2">Atención al público de BiciMAD
						</div>
					</div>
				</div>



			</div>
		</div>


		<div class="divider-section-small bg-bici"></div>

		</section>


		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>