<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>
<body>
	<div class="page-wrapper">


		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/usuarioheader.jsp">
			<jsp:param name="cambiopwd" value="active" />
		</jsp:include>

		<!-- content-15b -->
		<section class="content-15 v-center bg-bici">
		<div class="container">
			<div class="row">
				<h3>Cambio Contraseña</h3>
			</div>
			<form:form method="post" action="cambiopwdvalidate"
				commandName="cambioContrasena">
				<div class="form_settings">
					<form:errors path="*" cssClass="errorblock" element="div" />
					<p>
						<span><form:label path="pwdVieja">
								<fmt:message key="cambiopwd.pwdactual" />
							</form:label></span>
						<form:input path="pwdVieja" type='password' required="required" />
					</p>
					<p>
						<span><form:label path="pwdNueva">
								<fmt:message key="cambiopwd.pwdnueva" />
							</form:label></span>
						<form:input path="pwdNueva" type='password' required="required" />
					</p>
					<p>
						<span><form:label path="pwdRep">
								<fmt:message key="cambiopwd.pwdnuevarep" />
							</form:label></span>
						<form:input path="pwdRep" type='password' required="required" />
					</p>
										<p>
						<span>
						<form:input hidden="true" path="dni" value="${param.dni}"/>
					</p>
					<input type="submit" class="submit"
						value="<fmt:message
						key="inscripcion.env" />" />
				</div>

			</form:form>
		</div>
		<div class="divider-section-small bg-bici"></div>
		</section>

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>