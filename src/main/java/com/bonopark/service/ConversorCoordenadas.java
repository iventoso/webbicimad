package com.bonopark.service;

public class ConversorCoordenadas {

	public float conversorLatitudes(String latitud) {
		String signo = latitud.substring(9);
		float grados = Float.parseFloat(latitud.substring(0, 2))
				+ (Float.parseFloat(latitud.substring(2, 9)) / 60);
		if (signo == "N") {
			return grados;
		} else {
			return -grados;
		}

	}
	public float conversorLongitudes(String longitud) {
		String signo = longitud.substring(10);
		float grados = Float.parseFloat(longitud.substring(0, 3))
				+ (Float.parseFloat(longitud.substring(3, 10)) / 60);
		if (signo == "E") {
			return grados;
		} else {
			return -grados;
		}

	}

}
