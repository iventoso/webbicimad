package com.bonopark.repository;

import java.util.List;

import javax.persistence.EntityManager;

import com.bonopark.domain.Estacion;



public interface EstacionDao {
	List<Estacion> getEstacionList();

}
