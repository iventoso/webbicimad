/**
 * TarjetaBITWS_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package crtm.bit.sayp.webservices;

public interface TarjetaBITWS_PortType extends java.rmi.Remote {
    public crtm.bit.sayp.webservices.RespuestaInformacionGeneralTarjeta1 informacionGeneralTarjeta1(java.lang.String sNumeroTTP) throws java.rmi.RemoteException;
    public java.lang.Short bloquearOperacionSobreTitulosPorIrregularidadInfraccion(java.lang.String cCasoDeUso, java.lang.String sNumeroTTP) throws java.rmi.RemoteException;
}
