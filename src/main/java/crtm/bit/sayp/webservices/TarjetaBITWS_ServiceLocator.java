/**
 * TarjetaBITWS_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package crtm.bit.sayp.webservices;

public class TarjetaBITWS_ServiceLocator extends org.apache.axis.client.Service implements crtm.bit.sayp.webservices.TarjetaBITWS_Service {

    public TarjetaBITWS_ServiceLocator() {
    }


    public TarjetaBITWS_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TarjetaBITWS_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for TarjetaBITWSSoapHttp
    private java.lang.String TarjetaBITWSSoapHttp_address = "http://sbit1.crtm.es:50081/spai-crtm/srv/general/TarjetaBITWS";

    public java.lang.String getTarjetaBITWSSoapHttpAddress() {
        return TarjetaBITWSSoapHttp_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String TarjetaBITWSSoapHttpWSDDServiceName = "TarjetaBITWSSoapHttp";

    public java.lang.String getTarjetaBITWSSoapHttpWSDDServiceName() {
        return TarjetaBITWSSoapHttpWSDDServiceName;
    }

    public void setTarjetaBITWSSoapHttpWSDDServiceName(java.lang.String name) {
        TarjetaBITWSSoapHttpWSDDServiceName = name;
    }

    public crtm.bit.sayp.webservices.TarjetaBITWS_PortType getTarjetaBITWSSoapHttp() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(TarjetaBITWSSoapHttp_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getTarjetaBITWSSoapHttp(endpoint);
    }

    public crtm.bit.sayp.webservices.TarjetaBITWS_PortType getTarjetaBITWSSoapHttp(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            crtm.bit.sayp.webservices.TarjetaBITWSSoapHttpBindingStub _stub = new crtm.bit.sayp.webservices.TarjetaBITWSSoapHttpBindingStub(portAddress, this);
            _stub.setPortName(getTarjetaBITWSSoapHttpWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setTarjetaBITWSSoapHttpEndpointAddress(java.lang.String address) {
        TarjetaBITWSSoapHttp_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (crtm.bit.sayp.webservices.TarjetaBITWS_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                crtm.bit.sayp.webservices.TarjetaBITWSSoapHttpBindingStub _stub = new crtm.bit.sayp.webservices.TarjetaBITWSSoapHttpBindingStub(new java.net.URL(TarjetaBITWSSoapHttp_address), this);
                _stub.setPortName(getTarjetaBITWSSoapHttpWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("TarjetaBITWSSoapHttp".equals(inputPortName)) {
            return getTarjetaBITWSSoapHttp();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservices.sayp.bit.crtm/", "TarjetaBITWS");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservices.sayp.bit.crtm/", "TarjetaBITWSSoapHttp"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("TarjetaBITWSSoapHttp".equals(portName)) {
            setTarjetaBITWSSoapHttpEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
