package com.bonopark.domain;

import java.io.Serializable;
import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="base") 
public class Base implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int idbase;
//	private int activada;
//	private int ignorada;
//	private String buzpil;
//	private String inputs;
//	private String outputs;
//	private String rfid;
//	private String alarmas;
//	private String imaxc;
//	private String seglib;
//	private String maxmin;
//	private String tipeoutp;
//	private String rfid_device1;
//	private String rfid_device2;
//	private String rfid_device3;
//	private String rfid_device4;
//	private String rfid_device5;
//	private String rfid_device6;
//	private String rfid_device7;
//	private String rfid_device8;
//	private String rfid_device9;
//	private String rfid_device10;
//	private String rfid_device11;
//	private String rfid_device12;
//	private String rfid_device13;
//	private String rfid_device14;
//	private String rfid_device15;
//	private String rfid_device16;
//	private Date fecha_cambio_estado;
	private int estado;
//	private int tiempo_todeath;
//	private int tiempo_bateria;
//	private int tiempo_tarjeta_aceptada;
//	private int tiempo_tarjeta_rechazada;
//	private int tiempo_error_desenganche;
//	private int tiempo_interruptor_on;
//	private boolean reservable;
//	private String ip;
    @ManyToOne
    @JoinColumn(name="idestacion")
    @Id
    private Estacion estacion;
	
	public Base() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getIdbase() {
		return idbase;
	}

	public void setIdbase(int idbase) {
		this.idbase = idbase;
	}
//
//	public int getActivada() {
//		return activada;
//	}
//
//	public void setActivada(int activada) {
//		this.activada = activada;
//	}
//
//	public int getIgnorada() {
//		return ignorada;
//	}
//
//	public void setIgnorada(int ignorada) {
//		this.ignorada = ignorada;
//	}
//
//	public String getBuzpil() {
//		return buzpil;
//	}
//
//	public void setBuzpil(String buzpil) {
//		this.buzpil = buzpil;
//	}
//
//	public String getInputs() {
//		return inputs;
//	}
//
//	public void setInputs(String inputs) {
//		this.inputs = inputs;
//	}
//
//	public String getOutputs() {
//		return outputs;
//	}
//
//	public void setOutputs(String outputs) {
//		this.outputs = outputs;
//	}
//
//	public String getRfid() {
//		return rfid;
//	}
//
//	public void setRfid(String rfid) {
//		this.rfid = rfid;
//	}
//
//	public String getAlarmas() {
//		return alarmas;
//	}
//
//	public void setAlarmas(String alarmas) {
//		this.alarmas = alarmas;
//	}
//
//	public String getImaxc() {
//		return imaxc;
//	}
//
//	public void setImaxc(String imaxc) {
//		this.imaxc = imaxc;
//	}
//
//	public String getSeglib() {
//		return seglib;
//	}
//
//	public void setSeglib(String seglib) {
//		this.seglib = seglib;
//	}
//
//	public String getMaxmin() {
//		return maxmin;
//	}
//
//	public void setMaxmin(String maxmin) {
//		this.maxmin = maxmin;
//	}
//
//	public String getTipeoutp() {
//		return tipeoutp;
//	}
//
//	public void setTipeoutp(String tipeoutp) {
//		this.tipeoutp = tipeoutp;
//	}
//
//	public String getRfid_device1() {
//		return rfid_device1;
//	}
//
//	public void setRfid_device1(String rfid_device1) {
//		this.rfid_device1 = rfid_device1;
//	}
//
//	public String getRfid_device2() {
//		return rfid_device2;
//	}
//
//	public void setRfid_device2(String rfid_device2) {
//		this.rfid_device2 = rfid_device2;
//	}
//
//	public String getRfid_device3() {
//		return rfid_device3;
//	}
//
//	public void setRfid_device3(String rfid_device3) {
//		this.rfid_device3 = rfid_device3;
//	}
//
//	public String getRfid_device4() {
//		return rfid_device4;
//	}
//
//	public void setRfid_device4(String rfid_device4) {
//		this.rfid_device4 = rfid_device4;
//	}
//
//	public String getRfid_device5() {
//		return rfid_device5;
//	}
//
//	public void setRfid_device5(String rfid_device5) {
//		this.rfid_device5 = rfid_device5;
//	}
//
//	public String getRfid_device6() {
//		return rfid_device6;
//	}
//
//	public void setRfid_device6(String rfid_device6) {
//		this.rfid_device6 = rfid_device6;
//	}
//
//	public String getRfid_device7() {
//		return rfid_device7;
//	}
//
//	public void setRfid_device7(String rfid_device7) {
//		this.rfid_device7 = rfid_device7;
//	}
//
//	public String getRfid_device8() {
//		return rfid_device8;
//	}
//
//	public void setRfid_device8(String rfid_device8) {
//		this.rfid_device8 = rfid_device8;
//	}
//
//	public String getRfid_device9() {
//		return rfid_device9;
//	}
//
//	public void setRfid_device9(String rfid_device9) {
//		this.rfid_device9 = rfid_device9;
//	}
//
//	public String getRfid_device10() {
//		return rfid_device10;
//	}
//
//	public void setRfid_device10(String rfid_device10) {
//		this.rfid_device10 = rfid_device10;
//	}
//
//	public String getRfid_device11() {
//		return rfid_device11;
//	}
//
//	public void setRfid_device11(String rfid_device11) {
//		this.rfid_device11 = rfid_device11;
//	}
//
//	public String getRfid_device12() {
//		return rfid_device12;
//	}
//
//	public void setRfid_device12(String rfid_device12) {
//		this.rfid_device12 = rfid_device12;
//	}
//
//	public String getRfid_device13() {
//		return rfid_device13;
//	}
//
//	public void setRfid_device13(String rfid_device13) {
//		this.rfid_device13 = rfid_device13;
//	}
//
//	public String getRfid_device14() {
//		return rfid_device14;
//	}
//
//	public void setRfid_device14(String rfid_device14) {
//		this.rfid_device14 = rfid_device14;
//	}
//
//	public String getRfid_device15() {
//		return rfid_device15;
//	}
//
//	public void setRfid_device15(String rfid_device15) {
//		this.rfid_device15 = rfid_device15;
//	}
//
//	public String getRfid_device16() {
//		return rfid_device16;
//	}
//
//	public void setRfid_device16(String rfid_device16) {
//		this.rfid_device16 = rfid_device16;
//	}
//
//	public Date getFecha_cambio_estado() {
//		return fecha_cambio_estado;
//	}
//
//	public void setFecha_cambio_estado(Date fecha_cambio_estado) {
//		this.fecha_cambio_estado = fecha_cambio_estado;
//	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

//	public int getTiempo_todeath() {
//		return tiempo_todeath;
//	}
//
//	public void setTiempo_todeath(int tiempo_todeath) {
//		this.tiempo_todeath = tiempo_todeath;
//	}
//
//	public int getTiempo_bateria() {
//		return tiempo_bateria;
//	}
//
//	public void setTiempo_bateria(int tiempo_bateria) {
//		this.tiempo_bateria = tiempo_bateria;
//	}
//
//	public int getTiempo_tarjeta_aceptada() {
//		return tiempo_tarjeta_aceptada;
//	}
//
//	public void setTiempo_tarjeta_aceptada(int tiempo_tarjeta_aceptada) {
//		this.tiempo_tarjeta_aceptada = tiempo_tarjeta_aceptada;
//	}
//
//	public int getTiempo_tarjeta_rechazada() {
//		return tiempo_tarjeta_rechazada;
//	}
//
//	public void setTiempo_tarjeta_rechazada(int tiempo_tarjeta_rechazada) {
//		this.tiempo_tarjeta_rechazada = tiempo_tarjeta_rechazada;
//	}
//
//	public int getTiempo_error_desenganche() {
//		return tiempo_error_desenganche;
//	}
//
//	public void setTiempo_error_desenganche(int tiempo_error_desenganche) {
//		this.tiempo_error_desenganche = tiempo_error_desenganche;
//	}
//
//	public int getTiempo_interruptor_on() {
//		return tiempo_interruptor_on;
//	}
//
//	public void setTiempo_interruptor_on(int tiempo_interruptor_on) {
//		this.tiempo_interruptor_on = tiempo_interruptor_on;
//	}
//
//	public boolean isReservable() {
//		return reservable;
//	}
//
//	public void setReservable(boolean reservable) {
//		this.reservable = reservable;
//	}
//
//	public String getIp() {
//		return ip;
//	}
//
//	public void setIp(String ip) {
//		this.ip = ip;
//	}

	public Estacion getEstacion() {
		return estacion;
	}

	public void setEstacion(Estacion estacion) {
		this.estacion = estacion;
	}

}
