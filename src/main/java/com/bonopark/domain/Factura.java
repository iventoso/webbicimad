package com.bonopark.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name="facturas") 
public class Factura implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="nombre_factura")
	private String nombreFactura;
	private Date fecha;
	private double importe;
	private int id_factura;
//	@ManyToOne
//    @JoinColumn(name="rfid")
//    private Rfid rfid;
	@ManyToOne
	@JoinColumn(name="dni")
	private Residente residente;
	
	private String medios_pago;
	
	private String rfid;
	
	private double importe_inicial;
	
	public Residente getResidente() {
		return residente;
	}
	public void setResidente(Residente residente) {
		this.residente = residente;
	}
	public String getNombreFactura() {
		return nombreFactura;
	}
	public void setNombreFactura(String nombreFactura) {
		this.nombreFactura = nombreFactura;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getId_factura() {
		return id_factura;
	}
	public void setId_factura(int id_factura) {
		this.id_factura = id_factura;
	}
//	public Rfid getRfid() {
//		return rfid;
//	}
//	public void setRfid(Rfid rfid) {
//		this.rfid = rfid;
//	}
	public double getImporte() {
		return importe;
	}
	public void setImporte(double importe) {
		this.importe = importe;
	}
	@Override
	public String toString() {
		return "Factura [nombreFactura=" + nombreFactura + ", fecha=" + fecha
				+ ", importe=" + importe + ", id_factura=" + id_factura
//				+ ", rfid=" + rfid 
				+ "]";
	}
	public String getMedios_pago() {
		return medios_pago;
	}
	public void setMedios_pago(String medios_pago) {
		this.medios_pago = medios_pago;
	}
	public String getRfid() {
		return rfid;
	}
	public void setRfid(String rfid) {
		this.rfid = rfid;
	}
	public double getImporte_inicial() {
		return importe_inicial;
	}
	public void setImporte_inicial(double importe_inicial) {
		this.importe_inicial = importe_inicial;
	}
}