<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es">
<head>
<title>BiciMAD</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
</head>
<body>

	<!-- header-1 -->
	<jsp:include page="/WEB-INF/common/header-1.jsp">
		<jsp:param name="abonostarifas" value="active" />
	</jsp:include>

	<!-- content-15 -->
	<section class="content-15 v-center bg-bici">
	<div>
		<div class="container">
			<div class="row">
				<h3>Abonos</h3>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-6">
						<h6>Abono Anual</h6>
						<p>El abono anual se adquiere mediante tarjeta bancaria a
							través de cualquiera de los siguientes canales:</p>
						<ul>
							<li>En la página web de BiciMAD: www.bicimad.com.</li>
							<li>En la aplicación para móvil "BiciMAD".</li>
							<!--  				<li>Por teléfono, llamando al teléfono 010 (915 298 210 si llama desde fuera de la ciudad de Madrid).</li>
						<li>Sin cita previa en las Oficinas de Atención al Ciudadano Línea Madrid.</li>-->
						</ul>
						<p>
							Una vez adquirido el abono anual, el usuario recibe un mensaje de
							correo electrónico o de móvil en el que se indica un código de
							abonado con el que puede recoger la tarjeta de abono anual en el
							tótem de cualquier estación. </br>En el momento de la adquisición, el
							usuario también puede cargar con saldo la tarjeta de abono anual.
							Las posteriores recargas de saldo, se realizan a través de la
							página web de BiciMAD (www.bicimad.com) o la aplicación para móvil
							BiciMAD. La consulta de saldo
							está disponible en todos estos canales.</br>La tarjeta de abono anual
							es personal e intransferible, no pudiendo ser cedida a un
							tercero. </br>En caso de pérdida de la tarjeta de abono anual, el
							abonado tendrá que llamar al 010 o al teléfono de incidencias 900 50 54 63.</br>En el
							supuesto de robo o hurto de la tarjeta de abono anual, el abonado
							tendrá que denunciarlo ante las Fuerzas y Cuerpos de Seguridad y
							aportar copia de la denuncia para obtener de manera gratuita un
							duplicado de la misma en las Oficinas de Atención al Ciudadano
							Línea Madrid.</br>El duplicado de la tarjeta de abono anual se puede
							solicitar a través del 010 o el teléfono de incidencias 900 50 54 63.
							</br>La baja como abonado anual se realiza a través del
							teléfono 010 (915 298 210 si llama desde fuera de la ciudad de
							Madrid) o sin cita previa en las Oficinas de Atención al Ciudadano
							Línea Madrid.
						</p>
					</div>
					<div class="col-xs-6">
						<h6>Tarjetas de uso ocasional</h6>
						<p style="color: #a6555a;">Actualmente no disponible</p>
						<p>
							El uso ocasional del servicio se contrata mediante tarjeta
							bancaria en el tótem de la estación.</br>El usuario ocasional tiene
							que seleccionar en el tótem una tarjeta de un día, tres días o
							cinco días. Esta tarjeta es gratuita y le permite retirar la
							bicicleta las veces que quiera durante el periodo de validez de
							la misma, realizándose el cobro que corresponda por los tiempos
							de uso efectuados al final de dicho periodo de validez.</br>Como
							garantía del cobro, al expedir el tótem la tarjeta, queda
							bloqueado un importe de 150€ en la disposición de saldo de la
							tarjeta bancaria ("pre-autorización") que se desbloquea cuando se
							ha realizado el cargo en la tarjeta bancaria del importe que
							corresponde por los tiempos de uso efectuados.
						</p>
					</div>
				</div>
			</div>

		</div>
	</div>
	<h1></h1>
	<div class="divider-section-small bg-bici"></div>
	<div class="divider-section-big"></div>
	</section>

	<!-- footer-6 -->
	<jsp:include page="/WEB-INF/common/footer-6.jsp" />

	<!-- footer-2 -->
	<jsp:include page="/WEB-INF/common/footer-2.jsp" />


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>