<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>BiciMAD :: Servicios</title>
<jsp:include page="/WEB-INF/common/head.jsp" />

</head>
<title>DBIZI</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
<style>
html,body,#map_canvas {
	height: 100%;
	margin: 0px;
	padding: 0px
}

.controls {
	margin-top: 16px;
	border: 1px solid transparent;
	border-radius: 2px 0 0 2px;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	height: 32px;
	outline: none;
	box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
}

#pac-input {
	background-color: #fff;
	padding: 0 11px 0 13px;
	width: 400px;
	font-family: Roboto;
	font-size: 15px;
	font-weight: 300;
	text-overflow: ellipsis;
}

#pac-input:focus {
	border-color: #4d90fe;
	margin-left: -1px;
	padding-left: 14px; /* Regular padding-left + 1. */
	width: 401px;
}

.pac-container {
	font-family: Roboto;
}

#type-selector {
	color: #fff;
	background-color: #4d90fe;
	padding: 5px 11px 0px 11px;
}

#type-selector label {
	font-family: Roboto;
	font-size: 13px;
	font-weight: 300;
}
}
</style>
<script
	src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script src="https://www.google.com/jsapi"></script>

<script type="text/javascript">

function initialize() {
	var mapOptions = {
	 			center : new google.maps.LatLng(40.422767,-3.688329),
 			zoom : 13,
 			maxZoom : 18,
			mapTypeId : google.maps.MapTypeId.ROADMAP,
			streetViewControl: false,
			
		};

		var map = new google.maps.Map(document.getElementById("map_canvas"),
				mapOptions);

		setMarkers(map, estaciones);

	  var input = /** @type {HTMLInputElement} */(
	      document.getElementById('pac-input'));

	  var types = document.getElementById('type-selector');
	  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

	  var autocomplete = new google.maps.places.Autocomplete(input);
	  autocomplete.bindTo('bounds', map);

	  var infowindow = new google.maps.InfoWindow();
	  var marker = new google.maps.Marker({
	    map: map,
	    anchorPoint: new google.maps.Point(0, -29)
	  });

	  google.maps.event.addListener(autocomplete, 'place_changed', function() {
	    infowindow.close();
	    marker.setVisible(false);
	    var place = autocomplete.getPlace();
	    if (!place.geometry) {
	      return;
	    }

	    // If the place has a geometry, then present it on a map.
	    if (place.geometry.viewport) {
	      map.fitBounds(place.geometry.viewport);
	    } else {
	      map.setCenter(place.geometry.location);
	      map.setZoom(17);  // Why 17? Because it looks good.
	    }
	    marker.setIcon(/** @type {google.maps.Icon} */({
	      url: place.icon,
	      size: new google.maps.Size(71, 71),
	      origin: new google.maps.Point(0, 0),
	      anchor: new google.maps.Point(17, 34),
	      scaledSize: new google.maps.Size(35, 35)
	    }));
	    marker.setPosition(place.geometry.location);
	    marker.setVisible(true);

	    var address = '';
	    if (place.address_components) {
	      address = [
	        (place.address_components[0] && place.address_components[0].short_name || ''),
	        (place.address_components[1] && place.address_components[1].short_name || ''),
	        (place.address_components[2] && place.address_components[2].short_name || '')
	      ].join(' ');
	    }

	    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
	    infowindow.open(map, marker);
	  });

	  // Sets a listener on a radio button to change the filter type on Places
	  // Autocomplete.
	  function setupClickListener(id, types) {
	    var radioButton = document.getElementById(id);
	    google.maps.event.addDomListener(radioButton, 'click', function() {
	      autocomplete.setTypes(types);
	    });
	  }

	  setupClickListener('changetype-all', []);
	  setupClickListener('changetype-establishment', ['establishment']);
	  setupClickListener('changetype-geocode', ['geocode']);
	}



var estaciones = [
					<c:forEach items="${model.estaciones}" var="est" varStatus="loop">
					
 					<c:set var="numbaseslibres" value='0' />
					<c:set var="numbicis" value='0' />
				    
					    <c:forEach items="${est.bases}" var="base" varStatus="i">
					    <c:choose>
							<c:when test="${base.estado == '4'}">
								<c:set var="numbaseslibres" value="${numbaseslibres + 1}" />
							</c:when>
							<c:when test="${base.estado == '3'}">
								<c:set var="numbicis" value="${numbicis + 1}" />
							</c:when>
						</c:choose>
						</c:forEach> 				 
						
					
					[${est.latitud}, ${est.longitud}, "${est.nombre}", "${numbaseslibres}", "${numbicis}", "${est.luz}", "${est.numero_estacion}"]${!loop.last ? ',' : ''}
					
					</c:forEach>
	                  ]; 
	
	function setMarkers(map, locations) {

		var luz = null;


		//you only need 1 infoWindow
		  var infowindow = new google.maps.InfoWindow({
		       content: "holding",
		       //maxWidth: 200,//content will be set later

		     });
		  for (var i = 0; i < locations.length; i++) {
		    var estacion = locations[i];
		    var myLatLng = new google.maps.LatLng(estacion[0], estacion[1]);	
		    luz = estacion[5];
		   		    	
		    var marker = new google.maps.Marker({
		        position: myLatLng,
		        map: map,
		        icon: 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=1.1|0|48b85c|13|b|' + estacion[6],
/* 		        icon: '/resources/img/icono_bici_verde1.png', */
		        html: "<div id=\"maps_window_info\"><FONT SIZE=5 COLOR=blue><b>" + estacion[2] + "</b></FONT><br><FONT SIZE=4>Bicis: " + estacion[4] + "<br> Libres: " + estacion[3] + "</FONT></div>",
		    });
		    google.maps.event.addListener(marker, 'click', function() {
		    	infowindow.setContent(this.html);
			    infowindow.open(map,this);
			  });
		    if(luz == "true")
		    	{
		    	marker.icon = 'http://chart.apis.google.com/chart?chst=d_map_spin&chld=1.1|0|e64a3e|13|b|' + estacion[6];
		    	/* marker.icon = '/resources/img/icono_bici_rojo1.png'; */
		    	}
		  }
		  
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	
	


</script>

</head>
<body>

	<div class="page-wrapper">

		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/header-1.jsp">
			<jsp:param name="plano" value="active" />
		</jsp:include>

		<div class="divider-section"></div>

		<!-- contacts-5 -->
		<section class="contacts-5">
		<div class="map">
			map
			<div id="map_canvas"></div>
			<!-- 			<iframe
				src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d24299.65866840988!2d-3.6935817587829596!3d40.420871071367365!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4228eb98fe7375%3A0x1e29a0120f55f0a2!2sCalle+Serrano%2C+85!5e0!3m2!1ses!2ses!4v1397504567441"
				width="100%" height="450" frameborder="0" style="border: 0">
			</iframe> -->
		</div>
		<!-- 		 -->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 box-gray">
					<span class="fui-chat box-red-icon"></span> <br>(Nº) Número e
					información de cada estación
				</div>

				<div class="col-sm-4  box-green">
					<span class="fui-location box-red-icon"></span> <br>Estaciones
					con Alta Disponibilidad
				</div>
				<div class="col-sm-4 box-red">
					<span class="fui-location box-red-icon"></span> <br>Estaciones
					con Baja Disponibilidad
				</div>
			</div>
		</div>
		</section>

		<div class="divider-section-small"></div>

		<div class="divider-section-small"></div>

		<!-- footer-6 -->
		<jsp:include page="/WEB-INF/common/footer-6.jsp" />

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />

	</div>


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>


