package com.bonopark.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.bonopark.domain.Residente;

@Service("validatorInscripcion")
public class ValidatorInscripcion implements Validator {
	@Autowired
	private ResidenteManager residenteManager;

	private static final int ANNO_ACTUAL = Integer
			.valueOf(new SimpleDateFormat("yyyy").format(new Date()));

	@Override
	public boolean supports(Class<?> clazz) {
		return Residente.class.isAssignableFrom(clazz); // clase del bean al que
														// da soporte este
														// validador
		// return Residente.class.equals(clazz);

	}

	@Override
	public void validate(Object target, Errors errors) {

		// El nombre es obligatorio
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nombre",
				"required.nombre", "El nombre es obligatorio");

		// El apellido es obligatorio
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "apellido1",
				"required.apellido", "El apellido es obligatorio");

		// El dni es obligatorio
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dni",
				"required.dni", "El dni es obligatorio");

		// La fecha es obligatorio
		// ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fecha_nacimiento",
		// "required.fecha_nacimiento",
		// "La fecha de nacimiento es obligatorio");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email",
				"required.email", "El email es obligatorio");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "movil",
				"required.movil", "El movil es obligatorio");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "direccion",
				"required.direccion", "La direccion es obligatorio");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "municipio",
				"required.municipio", "El municipio es obligatorio");

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "codigo_postal",
				"required.codigo_postal", "El Codigo Postal es obligatorio");

		Residente residente = (Residente) target;

		try {
			DateFormat df2 = DateFormat.getDateInstance(DateFormat.SHORT);
			String fecha = df2.format(residente.getFecha_nacimiento());
			Date f = residente.getFecha_nacimiento();
			System.out.println("Fecha validator : " + fecha);
			if (f == null) {
				errors.rejectValue("fecha_nacimiento",
						"inscripcion.errorformatofecha");
				return;
			}

		} catch (Exception e) {
			errors.rejectValue("fecha_nacimiento",
					"inscripcion.errorformatofecha");
			return;
		}
		if (residente.getFecha_nacimiento() == null) {
			errors.rejectValue("fecha_nacimiento",
					"inscripcion.errorformatofecha");
			return;
		}

		// Comprobamos si ha elegido el pasaporte en el checkbox 
		// Si viene del perfil usuario y entonces será null y saltará excepcion
		try {
			if (!residente.getTipoDocumento().equals("3")) {
				if (!isDNI(residente.getDni()) && !isTarjetaDeResidenciaValida(residente.getDni())) {
					errors.rejectValue("dni", "inscripcion.dniincorrecto");
					return;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (!validarEdad(residente)) {

			errors.rejectValue("fecha_nacimiento",
					"inscripcion.fechaincorrecta");
			return;
		}
		if (!emailValido(residente.getEmail())) {
			errors.rejectValue("email", "inscripcion.emailincorrecto");
			return;
		}
		if (!movilValido(residente.getMovil())) {
			errors.rejectValue("movil", "inscripcion.movilincorrecto");
			return;
		}
	}

	private boolean movilValido(String movil) {
		Pattern dniPattern = Pattern.compile("(\\d{1,9})");
		Matcher m = dniPattern.matcher(movil);
		if (m.matches()) {
			return true;
		}
		return false;

	}

	public boolean emailValido(String email) {
		boolean result = true;
		try {
			InternetAddress emailAddr = new InternetAddress(email);
			emailAddr.validate();
		} catch (AddressException ex) {
			result = false;
		}
		return result;
	}

	public static int calcularEdad(Date fechaNacimiento) {
		int diff_year = 0;
		try {
			Calendar fechaNac = Calendar.getInstance();
			System.out.println("Fecha Metido: " + fechaNacimiento);
			fechaNac.setTime(fechaNacimiento);
			Calendar today = Calendar.getInstance();
			diff_year = today.get(Calendar.YEAR) - fechaNac.get(Calendar.YEAR);
			int diff_month = today.get(Calendar.MONTH)
					- fechaNac.get(Calendar.MONTH);
			int diff_day = today.get(Calendar.DAY_OF_MONTH)
					- fechaNac.get(Calendar.DAY_OF_MONTH);

			// Si está en ese año pero todavía no los ha cumplido
			if (diff_month < 0 || (diff_month == 0 && diff_day < 0)) {
				diff_year = diff_year - 1; // no aparecían los dos guiones del
											// postincremento :|

			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Fecha Erronea");
		}

		return diff_year;
	}

	private boolean validarEdad(Residente residente) {
		String fecha = new SimpleDateFormat("dd/MM/yyyy").format(residente
				.getFecha_nacimiento());
		if (residente.getFecha_nacimiento() == null) {
			return false;
		}
		int edad = calcularEdad(residente.getFecha_nacimiento());
		if (edad < 14) {
			return false;
		}
		return true;
	}

	// private int obtenerDigito(String valor) {
	// int[] valores = { 1, 2, 4, 8, 5, 10, 9, 7, 3, 6 };
	// int control = 0;
	// for (int i = 0; i <= 9; i++)
	// control += Integer.parseInt(valor.substring(i, i + 1)) * valores[i];
	// control = 11 - (control % 11);
	// if (control == 11)
	// control = 0;
	// else if (control == 10)
	// control = 1;
	// return control;
	// };

	// private boolean validarCuenta(Residente residente) {
	// //Validar Cuenta Corriente
	// if (!(obtenerDigito("00" + residente.getEntidad() +
	// residente.getOficina()) == Integer
	// .parseInt(residente.getDc()) / 10)
	// || !(obtenerDigito(residente.getNumCuenta()) == Integer
	// .parseInt(residente.getDc()) % 10)) {
	// System.out.println("El número de cuenta no es correcto");
	// return false;
	//
	// } else {
	// System.out.println("CCC correcto");
	// return true;
	// }
	// };

	// private boolean usuarioRepetido(Residente residente) {
	// String dni = residente.getDni();
	// return residenteManager.comprobarUsuarioByDni(dni);
	//
	// }

	public static boolean isDNI(String dni) {

		String primerCaracter = null;
		String segundoCaracter = null;
		String tercerCaracter = null;
		String valoresPrimerCaracter = "KLTXYZ";
		String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String nif = null;
		
		if (dni.length() == 11) {
			primerCaracter = dni.substring(0, 1).toUpperCase();
			segundoCaracter = dni.substring(1, 2).toUpperCase();
			tercerCaracter = dni.substring(2, 4).toUpperCase();
			nif = dni.substring(2, 11).toUpperCase();
			if(valoresPrimerCaracter.indexOf(primerCaracter) == -1)
				return false;
			else if (letras.indexOf(segundoCaracter) == -1)
				return false;
			else if (tercerCaracter.equals("00"))
				return false;
			else
				return isNIFValido(nif);
			
		} else if (dni.toUpperCase().startsWith("X")
				|| dni.toUpperCase().startsWith("Y")
				|| dni.toUpperCase().startsWith("Z"))

			dni = dni.substring(1);

		return isNIFValido(dni);
		
	}

	private static boolean isNIFValido(String dni)
	{
		Pattern dniPattern = Pattern
				.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
		Matcher m = dniPattern.matcher(dni);
		if (m.matches()) {
			String letra = m.group(2);
			// Extraer letra del NIF
			String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
			int numeroDni = Integer.parseInt(m.group(1));
			numeroDni = numeroDni % 23;
			String reference = letras.substring(numeroDni, numeroDni + 1);

			if (reference.equalsIgnoreCase(letra)) {

				return true;
			} else {

				return false;
			}
		} else
			return false;
	}

	// TARJETA DE RESIDENTE
	public static boolean isTarjetaDeResidenciaValida(String cadena) {

		int longitud = 0;
		boolean correcto = true;
		String nif = null;
		String primerCaracter = null;
		String segundoCaracter = null;
		String tercerCaracter = null;
		String valoresPrimerCaracter = "KLTXYZ";
		String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		// Valida la longitud de la cadena.
		longitud = cadena.length();
		if (longitud == 0) {
			return false;
		} else if (!(longitud == 11 || longitud == 9)) {
			return false;
		}

		primerCaracter = cadena.substring(0, 1).toUpperCase();

		if (longitud == 11) {
			segundoCaracter = cadena.substring(1, 2).toUpperCase();
			tercerCaracter = cadena.substring(2, 4).toUpperCase();
			nif = cadena.substring(2, 11).toUpperCase();

			if (valoresPrimerCaracter.indexOf(primerCaracter) == -1)
				correcto = false;
			else if (letras.indexOf(segundoCaracter) == -1)
				correcto = false;
			else if (tercerCaracter.equals("00"))
				correcto = false;
			else
				correcto = isNIFValido(nif);
		}

		if (longitud == 9) {
			if (primerCaracter.equals("X")) {
				nif = "0" + cadena.substring(1, 9).toUpperCase();
				correcto = isNIFValido(nif);
			} else if (primerCaracter.equals("Y")) {
				nif = "1" + cadena.substring(1, 9).toUpperCase();
				correcto = isNIFValido(nif);
			} else if (primerCaracter.equals("Z")) {
				nif = "2" + cadena.substring(1, 9).toUpperCase();
				correcto = isNIFValido(nif);
			} else {
				correcto = false;
			}
		}
		return correcto;
	}
}
