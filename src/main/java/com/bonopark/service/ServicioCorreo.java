package com.bonopark.service;  

import com.bonopark.domain.Residente;
import com.bonopark.domain.Usuario;

public interface ServicioCorreo {
    
    public void sendContacto(String to, String subject, String nombre, String Apellido, String movil, String email, String mensaje);  

	boolean sendCambioPwd(String to, String pwd, String DNI);
	
	boolean sendErrorBBDDPago(String texto);
	
	boolean sendErrorBBDDFactura(String texto);
	boolean sendError(String texto);

	String sendConfirmacion(String to, String pwd,
			Residente residente, Usuario usuario, int codId);
	
	String sendConfirmacionSinClave(String to, String codId, String fechaCaducidad);

	String sendRecargaSaldo(String to, double recarga, String saldoTotal,
			Residente residente);

	String sendModificacionDatos(String to, Residente residente);

	String sendBaja(String to, Residente residente);

	String sendContrasena(String to, String pwd, Residente residente);
	
}
