package crtm.bit.sayp.webservices;

public class TarjetaBITWSProxy implements crtm.bit.sayp.webservices.TarjetaBITWS_PortType {
  private String _endpoint = null;
  private crtm.bit.sayp.webservices.TarjetaBITWS_PortType tarjetaBITWS_PortType = null;
  
  public TarjetaBITWSProxy() {
    _initTarjetaBITWSProxy();
  }
  
  public TarjetaBITWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initTarjetaBITWSProxy();
  }
  
  private void _initTarjetaBITWSProxy() {
    try {
      tarjetaBITWS_PortType = (new crtm.bit.sayp.webservices.TarjetaBITWS_ServiceLocator()).getTarjetaBITWSSoapHttp();
      if (tarjetaBITWS_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)tarjetaBITWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)tarjetaBITWS_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (tarjetaBITWS_PortType != null)
      ((javax.xml.rpc.Stub)tarjetaBITWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public crtm.bit.sayp.webservices.TarjetaBITWS_PortType getTarjetaBITWS_PortType() {
    if (tarjetaBITWS_PortType == null)
      _initTarjetaBITWSProxy();
    return tarjetaBITWS_PortType;
  }
  
  public crtm.bit.sayp.webservices.RespuestaInformacionGeneralTarjeta1 informacionGeneralTarjeta1(java.lang.String sNumeroTTP) throws java.rmi.RemoteException{
    if (tarjetaBITWS_PortType == null)
      _initTarjetaBITWSProxy();
    return tarjetaBITWS_PortType.informacionGeneralTarjeta1(sNumeroTTP);
  }
  
  public java.lang.Short bloquearOperacionSobreTitulosPorIrregularidadInfraccion(java.lang.String cCasoDeUso, java.lang.String sNumeroTTP) throws java.rmi.RemoteException{
    if (tarjetaBITWS_PortType == null)
      _initTarjetaBITWSProxy();
    return tarjetaBITWS_PortType.bloquearOperacionSobreTitulosPorIrregularidadInfraccion(cCasoDeUso, sNumeroTTP);
  }
  
  
}