package com.bonopark.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.*;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bonopark.domain.Noticia;

@Repository(value = "noticiaDao")
public class JPANoticiaDao implements NoticiaDao{

	private static EntityManager em = null;
	/*
	 * Sets the entity manager.
	 */
	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	public void setEntityManager(EntityManager em) {
		JPANoticiaDao.em = em;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Noticia> getNoticias() {
		return em.createQuery("from Noticia n order by n.fecha desc").getResultList();
	}
	public Noticia getNoticiaById(int id) {
		// TODO Auto-generated method stub
		return (Noticia) em.createQuery("from Noticia n where n.id = ?1")
				.setParameter(1, new Integer (id)).getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Noticia> getUltimasNoticias(int numero) {
		return em.createQuery("from Noticia n order by n.fecha desc").setMaxResults(numero).getResultList();
	}

}
