<!-- footer-6 -->
<footer class="footer-6">
	<div class="container">
		<div class="row">
			<div class="col-sm-2">
				<div>
					<a class="brand" href="#">Bici<strong>MAD</strong></a>
				</div>
			</div>
		</div>

		<nav>
			<div class="row">
				<div class="col-sm-12">

					<div class="col-sm-2">
						<h6>QUE ES</h6>
						<ul>
							<li><a href="que.html">Desripci�n<br>del Servicio
							</a></li>
							<li><a href="componentes.html">Componentes</a></li>
							<li><a href="preguntas.html">Preguntas<br>frecuentes
							</a></li>
						</ul>
					</div>

					<div class="col-sm-2">
						<h6>SERVICIOS</h6>
						<ul>
							<li><a href="tarifas.html">Tarifas</a></li>
							<li><a href="seguro.html">Seguros</a></li>
							<li><a href="horario.html">Horarios</a></li>
							<li><a href="normativa.html">Normativa</a></li>
							<li><a href="app.html">Descarga la App</a></li>
						</ul>
					</div>

					<div class="col-sm-2">
						<h6>CONTACTOS</h6>
						<ul>
							<li><a href="mapa.html">Mapas oficinas</a></li>
							<li><a href="telefono.html">Tel�fonos</a></li>
						</ul>
					</div>

					<div class="col-sm-2">
						<h6>USUARIOS</h6>
						<ul>
							<li><a href="info.html">Tu informaci�n</a></li>
							<li><a href="sistema.html">Informaci�n<br>del
									sistema
							</a></li>
							<li><a href="recarga.html">Recarga saldo</a></li>
						</ul>
					</div>

					<div class="col-sm-2">
						<h6>INSCR�BETE</h6>
						<ul>
							<li><a href="datos.html">Rellene<br> sus datos
							</a></li>
							<li><a href="compra.html">Compra</a></li>
						</ul>
					</div>

					<div class="col-sm-2">
						<h6>RECARGA</h6>
						<ul>
							<li><a href="login.html">Login</a></li>
						</ul>
					</div>

				</div>
			</div>

		</nav>


	</div>
</footer>