package com.bonopark.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="rfid") 
public class Rfid implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	private String idrfid;
	private int tipo;
	@Column(columnDefinition = "BIT")
	private boolean activado;
	
	@OneToOne
	@PrimaryKeyJoinColumn
	private Abono abono;
	//private Set<Abono> abono = new HashSet<Abono>();
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="rfid")
	@OrderBy("fecha_desenganche DESC")
	private Set<Enganche> enganche;

	public String getIdrfid() {
		return idrfid;
	}

	public void setIdrfid(String idrfid) {
		this.idrfid = idrfid;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public boolean isActivado() {
		return activado;
	}

	public void setActivado(boolean activado) {
		this.activado = activado;
	}

	public Abono getAbono() {
		return abono;
	}

	public void setAbono(Abono abono) {
		this.abono = abono;
	}

	public Set<Enganche> getEnganche() {
		return enganche;
	}

	public void setEnganche(Set<Enganche> enganche) {
		this.enganche = enganche;
	}

	@Override
	public String toString() {
		return "Rfid [idrfid=" + idrfid + ", tipo=" + tipo + ", activado="
				+ activado + "]";
	}
	
	
}
