package com.bonopark.web;

import java.io.Serializable;
import java.security.Principal;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.bonopark.domain.Residente;
import com.bonopark.service.ResidenteManager;
import com.bonopark.service.ValidatorInscripcion;

@Controller
public class LoginController implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Autowired
	private ResidenteManager residenteManager;

	@ModelAttribute("residente")
	public Residente populateForm() {
		return new Residente(); // creamos el bean para que se pueda popular
	}
	@RequestMapping("/login")
	public ModelAndView login(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {
		HomeController.detectarIdioma(request, response);
		return new ModelAndView("login");

	}

	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public ModelAndView loginerror(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

		HomeController.detectarIdioma(request, response);
		model.addAttribute("error", "true");
		return new ModelAndView("login");

	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

		HomeController.detectarIdioma(request, response);

		return new ModelAndView("login");

	}

	@RequestMapping(value = "/validarolvidopwd", method = RequestMethod.POST)
	public ModelAndView validarolvidopwd(Residente residente, ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

		HomeController.detectarIdioma(request, response);
		Map<String, Object> myModel = new HashMap<String, Object>();
		// if (result.hasErrors()) {
		// return new ModelAndView("olvidecontrasena");
		// }
		// Comprobamos que el Usuario ha introducido el DNI y email
		// correctamente
		residente = residenteManager.findUsuarioByDniandEmail(residente.getDni(),
				residente.getEmail());
		if (residente == null) {
			// Los datos de DNI y Email no son correctos
			myModel.put("mensaje", "olvidepwd.datoserroneos");

		} else {
			if (residenteManager.generarPwd(residente) == false) {
				myModel.put("mensaje", "mensaje.operacionnorealizada");
			}
			else{
				myModel.put("mensaje", "olvidepwd.operacionok");
			}

		}
		return new ModelAndView("mensaje", "model", myModel);
	}

	@RequestMapping(value = "/olvidepwd", method = RequestMethod.GET)
	public ModelAndView olvidecontrasena(ModelMap model,
			HttpServletRequest request, HttpServletResponse response) {

		HomeController.detectarIdioma(request, response);
		return new ModelAndView("olvidecontrasena");

	}

}
