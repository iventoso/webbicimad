<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<jsp:directive.include file="/WEB-INF/common/include.jsp" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<%@ page import="java.security.*"%>
<title>Pago</title>
<jsp:include page="/WEB-INF/common/head.jsp" />
<head>


<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/sha1.js"></script>
<script language="javascript">
	function atras() {
		history.back();
	}
</script>
<style>
	.divider-section-small{ min-height:300px !important;}

	#btnCancelar, #btnContinuar{ width:48% !important; float:left;}
	#btnCancelar{ margin-left:1%;}
	@media only screen and (max-width: 760px){
		#btnCancelar, #btnContinuar{ width:100% !important;}
		#btnCancelar{ margin-left:0% !important; margin-top:20px !important;}
	}
	
</style>

</head>
<body>
	<div class="page-wrapper">
		<!-- header-1 -->
		<jsp:include page="/WEB-INF/common/usuarioheader.jsp">
			<jsp:param name="saldo" value="active" />
		</jsp:include>
		<!-- content-15b -->

		<!-- contacts-3 -->
		<div class="contacts-3">
			<div class="container">
                <div class="row">
                    <div class="col-xs-10" style="margin-bottom:40px;">
                        <h1></h1>
                        <h3>
                            <fmt:message key="pago.titulo" />
                        </h3>
                        <div class="mensaje">
                            <ul class="texto_lista">
                                <li><p>Saldo Actual: ${model.saldoactual}</p></li>
                                <li><p>Recarga de Saldo : ${model.recarga}</p></li>
                            </ul>
                        </div>
                        <form name=pago
                            action='https://sis.redsys.es/sis/realizarPago'
                            method="post">
                            <div class="form_settings">
                                <input type="hidden" name="Ds_Merchant_MerchantName"
                                    value="BiciMad - Bonopark">
                                    </input> 
                                    <input type="hidden"
                                    name=Ds_Merchant_MerchantCode value="285970679">
                                    </input> 
                                    <input
                                    type="hidden" name=Ds_Merchant_Terminal value="1">
                                    </input> 
                                    <input
                                    type="hidden" name=Ds_Merchant_Order value="${model.merchantOrder}">
                                    </input>
                                <input type="hidden" name=Ds_Merchant_Amount
                                    value="${model.recargaTPV}"></input> <input type="hidden"
                                    name=Ds_Merchant_Currency value="978"></input> <input
                                    type="hidden" name=Ds_Merchant_TransactionType value="0"></input>
                                <input type="hidden" name=Ds_Merchant_MerchantURL
                                    value="${model.notificacionTPV}"></input>
                                <input type="hidden" name=Ds_Merchant_MerchantSignature
                                    value="${model.firmaTPV}"></input> <input type="hidden"
                                    name=Ds_Merchant_Identifier value="REQUIRED"></input> <input
                                    type="hidden" name=Ds_Merchant_MerchantData
                                    value="${model.dni}"></input>
                                    <input
                                    type="hidden" name=Ds_Merchant_UrlOK
                                    value="http://www.bicimad.com/pagook.html"></input>
                                    <input
                                    type="hidden" name=Ds_Merchant_UrlKO
                                    value="http://www.bicimad.com/pagoko.html"></input>
    
                                <!-- 						<center>
                            
                                <a href="javascript:calc()">Pagar</a>
                            </center> -->
                                <input id="btnContinuar" title="continuar" type="submit" class="submit" value="<fmt:message key="inscripcion.continuar" />" /> 
                                <%-- <button type="submit" class="submit"
                                    value="<fmt:message
                            key="inscripcion.continuar" />" /> --%>
                                <input title="cancelar" type='button' name='btnCancelar' id='btnCancelar' value='Cancelar' onClick="atras();" />
                            </div>
                        </form>
                	</div>
				</div>
			</div>
		</div>
		
        
	</div>

		<!-- footer-2 -->
		<jsp:include page="/WEB-INF/common/footer-2.jsp" />


	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/WEB-INF/common/scriptsfinal.jsp" />
</body>
</html>