package com.bonopark.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.*;

@Entity
@Table(name = "enganche")
public class Enganche implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	private int idestacion_desenganche;
	private int idestacion_enganche;
	@Id
	private Date fecha_enganche;
	private Date fecha_desenganche;
	private double saldo_anterior;
	private double saldo_nuevo;
	@Column(columnDefinition = "BIT")
	private boolean con_reserva;
	@Column(columnDefinition = "BIT")
	private boolean luz_enganche;
	@Column(columnDefinition = "BIT")
	private boolean luz_desenganche;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "idrfid")
	private Rfid rfid;



	public int getIdestacion_desenganche() {
		return idestacion_desenganche;
	}
	public void setIdestacion_desenganche(int idestacion_desenganche) {
		this.idestacion_desenganche = idestacion_desenganche;
	}
	public int getIdestacion_enganche() {
		return idestacion_enganche;
	}
	public void setIdestacion_enganche(int idestacion_enganche) {
		this.idestacion_enganche = idestacion_enganche;
	}
	public Date getFecha_enganche() {
		return fecha_enganche;
	}
	public void setFecha_enganche(Date fecha_enganche) {
		this.fecha_enganche = fecha_enganche;
	}

	public Date getFecha_desenganche() {
		return fecha_desenganche;
	}

	public void setFecha_desenganche(Date fecha_desenganche) {
		this.fecha_desenganche = fecha_desenganche;
	}

	public double getSaldo_anterior() {
		return saldo_anterior;
	}

	public void setSaldo_anterior(double saldo_anterior) {
		this.saldo_anterior = saldo_anterior;
	}

	public double getSaldo_nuevo() {
		return saldo_nuevo;
	}

	public void setSaldo_nuevo(double saldo_nuevo) {
		this.saldo_nuevo = saldo_nuevo;
	}

	public boolean isCon_reserva() {
		return con_reserva;
	}

	public void setCon_reserva(boolean con_reserva) {
		this.con_reserva = con_reserva;
	}

	public boolean isLuz_enganche() {
		return luz_enganche;
	}

	public void setLuz_enganche(boolean luz_enganche) {
		this.luz_enganche = luz_enganche;
	}

	public boolean isLuz_desenganche() {
		return luz_desenganche;
	}

	public void setLuz_desenganche(boolean luz_desenganche) {
		this.luz_desenganche = luz_desenganche;
	}

	public Rfid getRfid() {
		return rfid;
	}

	public void setRfid(Rfid rfid) {
		this.rfid = rfid;
	}

	@Override
	public String toString() {
		return "Enganche [fecha_enganche=" + fecha_enganche + ", fecha_desenganche="
				+ fecha_desenganche + ", saldo_anterior=" + saldo_anterior
				+ ", saldo_nuevo=" + saldo_nuevo + ", con_reserva="
				+ con_reserva + ", luz_enganche=" + luz_enganche
				+ ", luz_desenganche=" + luz_desenganche + " rfid=" + rfid + "]";
	}

}
