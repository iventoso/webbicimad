package com.bonopark.service;

import java.io.Serializable;
import java.util.List;
import com.bonopark.domain.*;
public interface EstacionManager extends Serializable{
	
	public List<Estacion> getEstaciones();

}
